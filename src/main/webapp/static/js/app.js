$(document).ready(function() {
    $("#orderBy").change(function() {
        var url = location.href;
        var regEx = /([?&]orderBy)=([^#&]*)/g;
        var regEx2 = /[?]/g;
        if(regEx.test(url)) {
            var newurl = url.replace(regEx, '$1='+$("#orderBy option:selected").val());
        } else if(regEx2.test(url)) {
            newurl = url+"&orderBy="+$("#orderBy option:selected").val();
        } else {
            newurl = url+"?orderBy="+$("#orderBy option:selected").val();
        }
        window.location.replace(newurl);
    });
});