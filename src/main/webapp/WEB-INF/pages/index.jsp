<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/narrowH1.jsp" %>
<%@ include file = "helpers/filterHeader.jsp" %>

<c:url value="${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string']}" var="currentPage"/>
    <div class="row">
        <div class="shop__grid__view__wrap">
            <div role="tabpanel" id="grid-view" class="single-grid-view tab-pane fade in active clearfix">
            <c:forEach var="modification" items="${modificationsList}">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="category">
                        <div class="ht__cat__thumb">
                            <a href="/shop/${modification.id}">
                                <img src=${modification.product.photo} width="290px" height = "385px" alt="product images">
                            </a>
                        </div>
                        <div class="fr__hover__info">
                            <ul class="product__action">
                                <li><a href="/cart/${modification.id}/1" title = "Купить"><i class="icon-basket icons"></i></a></li>
                            </ul>
                        </div>
                        <div class="fr__product__inner">
                            <h4><a href="/shop/${modification.id}">
                                <div>
                                    <c:forEach var="productProperty" items="${modification.product.productProperties}" varStatus="loop">
                                        <c:if test="${productProperty.property.id == Constants.BRAND_PROPERTY_ID}">
                                            <c:forEach var="peValue" items="${productProperty.property.propertyEnumValues}">
                                                <c:if test = "${(peValue.id == productProperty.value)}">${peValue.value}
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </c:forEach>
                                </div>
                                ${modification.product.name}
                                <c:forEach var="modificationProperty" items="${modification.modificationProperties}">
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                                    </c:if>
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                                    </c:if>
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                                    </c:if>
                                </c:forEach>
                            </a></h4>
                            <ul class="fr__pro__prize">
                                <li>${modification.price} &#8381;</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </c:forEach>
            </div>
        </div>
    </div>
</div>
<%@ include file = "helpers/pagination.jsp" %>
<%@ include file = "helpers/filterFooter.jsp" %>
<%@ include file = "layouts/footer.jsp" %>

