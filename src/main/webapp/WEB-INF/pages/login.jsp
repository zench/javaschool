<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel-body">
        <c:if test="${empty userId}">
        <div class="checkout-method__login col-md-5">
            <form action="login" method="post">
                <h2 class="checkout-method__title mb--10">Авторизация</h2>
                <c:if test="${not empty error}">
                    <div class="alert alert-danger">
                        Ошибка аутентификации
                        <br/>
                    </div>
                </c:if>
                <div class="accordion__body">
                    <div class="checkout-method__login">
                        <div class="single-input">
                            <label for="user-email">Email</label>
                            <input name = "username" type="email" id="user-email">
                        </div>
                        <div class="single-input mb--10">
                            <label for="user-pass">Пароль</label>
                            <input name='password' type="password" id="user-pass">
                        </div>
                        <p class="require">* Обязательные поля</p>
                        <!--<a href="/login/restore">Забыли пароль?</a>-->
                        <div class="dark-btn mt--10">
                            <input type="submit" value="Войти">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </c:if>
        <c:if test="${!empty userId}">
            <p>Вы успешно авторизованы!</p>
            <div class="mt--10 buttons-cart">
                <a href = "/">Перейти к покупкам</a>
            </div>
        </c:if>
    </div>
</div>
<%@ include file = "layouts/footer.jsp" %>