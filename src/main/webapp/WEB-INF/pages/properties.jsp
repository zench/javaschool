<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <h2>${category.name}</h2>
    <table class="table table-striped">
        <tr>
            <th>Название</th>
            <th>Тип</th>
            <th>Ед. измерения</th>
            <th>Продукт/модификация</th>
            <th>Действие</th>
        </tr>
        <c:forEach var="property" items="${propertiesList}">
            <tr>
                <td>${property.name}</td>
                <td>
                        ${property.type.name}
                </td>
                <td>
                        ${property.unit.name}
                </td>
                <td>
                        ${property.entityType.value}
                </td>
                <td>
                    <a href="/admin/properties/edit/${property.id}" title="Редактировать"><i class="icon-pencil icons"></i></a>
                    <c:if test="${property.id > 15}" >
                        <a href="/admin/properties/delete/${property.id}" title="Удалить"><i class="icon-close icons"></i></a>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a class="btn btn-success" href="/admin/categories/${category.id}/properties/add" role="button">Добавить</a>
</div>
<%@ include file = "helpers/adminMenu.jsp" %>
<%@ include file = "layouts/footer.jsp" %>
