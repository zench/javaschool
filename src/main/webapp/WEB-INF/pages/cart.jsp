<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>
<c:if test="${!empty cartItems}">
<div class = "row">
<form action="/cart/update" method="post">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="table-content table-responsive">
            <table>
                <thead>
                <tr>
                    <th class="product-thumbnail"></th>
                    <th class="product-name">Название</th>
                    <th class="product-price">Цена</th>
                    <th class="product-quantity">Количество</th>
                    <th class="product-subtotal">Всего</th>
                    <th class="product-remove">Удалить</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="cartItem" items="${cartItems}" varStatus="loop">
                    <tr>
                        <td class="product-thumbnail"><a href="/shop/${cartItem.modification.id}"><img src="${cartItem.modification.product.photo}" alt="product img" /></a></td>
                        <td class="product-name"><a href="/shop/${cartItem.modification.id}">${cartItem.modification.product.name}
                            <c:forEach var="modificationProperty" items="${cartItem.modification.modificationProperties}">
                                <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                                </c:if>
                                <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                                </c:if>
                                <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                                </c:if>
                            </c:forEach>
                            </a>
                            <input type="hidden" name="cartItems[${loop.index}].id" value="${cartItem.id}">
                        </td>
                        <td class="product-price"><span class="amount">${cartItem.modification.price} &#8381;</span></td>
                        <td class="product-quantity"><input type="number" min="1" name="cartItems[${loop.index}].count" value="${cartItem.count}" /></td>
                        <td class="product-subtotal">${cartItem.modification.price*cartItem.count} &#8381;</td>
                        <td class="product-remove"><a href="/cart/delete/${cartItem.id}"><i class="icon-trash icons"></i></a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="buttons-cart--inner">
                    <div class="buttons-cart">
                        <a href="/">Продолжить покупки</a>
                    </div>
                    <div class="buttons-cart checkout--btn">
                        <input type = "submit" value = "обновить" />
                        <a href="/checkout">оформить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12"></div>
    <div class="col-md-6 col-sm-12 col-xs-12 smt-40 xmt-40">
        <div class="htc__cart__total">
            <h6>Итого</h6>
            <div class="cart__total">
                <span>Всего на сумму</span>
                <span>${total} &#8381;</span>
            </div>
            <ul class="payment__btn">
                <li class="active"><a href="checkout">оформить</a></li>
                <li><a href="/">продолжить покупки</a></li>
            </ul>
        </div>
    </div>
</form>
</div>
</c:if>
<c:if test="${empty cartItems}">
    <h2>Корзина пуста</h2>
    <div class="mt--10 buttons-cart">
        <a href = "/">Перейти к покупкам</a>
    </div>
</c:if>
<%@ include file = "layouts/footer.jsp" %>