<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="pagina" value="${requestScope['javax.servlet.forward.request_uri']}" />
<div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-12 col-xs-12 smt-40 xmt-40">
    <div class="menu_header">Меню</div>
    <div class="htc__category">
        <ul class="ht__cat__list">
            <li class="nav-item ${pagina.endsWith('/user/orders') ? 'active' : ''}"><a href="/user/orders">История заказов</a></li>
            <li class="nav-item ${pagina.endsWith('/user/account') ? 'active' : ''}"><a href="/user/account">Аккаунт</a></li>
            <li class="nav-item ${pagina.endsWith('/user/password') ? 'active' : ''}"><a href="/user/password">Сменить пароль</a></li>
            <li><a href="/cart">Корзина</a></li>
            <li><a href="/checkout">Оформление заказа</a></li>
        </ul>
    </div>
</div>
