<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${!empty title}">
    <h1>${title}</h1>
</c:if>
<c:if test="${empty title}">
    <h1>Интернет-магазин шин</h1>
</c:if>
