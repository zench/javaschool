<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
    <form action = "/admin/statistics" method="get">
        <div class="row bilinfo">
            <div class="col-md-4">
                <div class="single-input">
                    <input type="date" name = "dateFrom" placeholder="Дата с" value="${param.dateFrom}" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-input">
                    <input type="date" name = "dateTo" placeholder="Дата по" value="${param.dateTo}" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-input">
                    <select name = "type">
                        <option value = "client">По клиентам</option>
                        <option ${(param.type == 'product') ? 'selected="selected"' : ''} value = "product">По товарам</option>
                        <option ${(param.type == 'income') ? 'selected="selected"' : ''} value = "income">По выручке</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="dark-btn mt--20">
                    <input type="submit" value="Поиск">
                </div>
            </div>
        </div>
    </form>
</div>