<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Start Pagination -->
<div class="row">
    <div class="col-xs-12">
        <ul class="htc__pagenation">
            <li><a href="#"></a></li>
            <c:forEach begin="1" end="${pagesCount}" step="1" varStatus="i">
                <c:url value="${url}" var="urlAdd">
                    <c:param name="page" value="${i.index}"/>
                    <c:if test="${!empty param.orderBy}">
                        <c:param name="orderBy" value="${param.orderBy}"/>
                    </c:if>
                    <c:if test="${!empty param.season}">
                        <c:param name="season" value="${param.season}"/>
                    </c:if>
                    <c:if test="${!empty param.spike}">
                        <c:param name="spike" value="${param.spike}"/>
                    </c:if>
                    <c:if test="${!empty param.priceFrom}">
                        <c:param name="priceFrom" value="${param.priceFrom}"/>
                    </c:if>
                    <c:if test="${!empty param.priceTo}">
                        <c:param name="priceTo" value="${param.priceTo}"/>
                    </c:if>
                    <c:if test="${!empty param.brand}">
                        <c:forEach var="brandChecked" items="${paramValues.brand}">
                            <c:param name="brand" value="${brandChecked}"/>
                        </c:forEach>
                    </c:if>
                </c:url>
                <li class="${((param.page == i.index) || ((empty param.page) && (i.index == 1))) ? 'active' : ''}"><a href="${urlAdd}">${i.index}</a></li>
            </c:forEach>
            <li><a href="#"></a></li>
        </ul>
    </div>
</div>
<!-- End Pagination -->
