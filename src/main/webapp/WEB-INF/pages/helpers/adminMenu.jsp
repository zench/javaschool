<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="pagina" value="${requestScope['javax.servlet.forward.request_uri']}" />
<div class="col-lg-2 col-lg-pull-9 col-md-2 col-md-pull-9 col-sm-12 col-xs-12 smt-40 xmt-40">
    <div class="menu_header">Меню</div>
    <div class="htc__category">
        <ul class="ht__cat__list">
            <li class="nav-item ${pagina.contains('/admin/categories') ? 'active' : ''}"><a href="/admin/categories">Категории</a></li>
            <li class="nav-item ${pagina.endsWith('/admin/orders') ? 'active' : ''}"><a href="/admin/orders">Заказы</a></li>
            <li class="nav-item ${pagina.endsWith('/admin/statistics') ? 'active' : ''}"><a href="/admin/statistics">Статистика</a></li>
        </ul>
    </div>
</div>