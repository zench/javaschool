<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="offset__wrapper">
    <!-- Start Cart Panel -->
    <div class="shopping__cart">
        <div class="shopping__cart__inner">

                <div class="offsetmenu__close__btn">
                    <a href="#"><i class="zmdi zmdi-close"></i></a>
                </div>
            <c:if test="${empty cartItems}">
                Корзина пуста.
            </c:if>
            <c:if test="${!empty cartItems}">
                <div class="shp__cart__wrap">
                    <c:forEach var="cartItem" items="${cartItems}" varStatus="loop">
                        <div class="shp__single__product">
                            <div class="shp__pro__thumb">
                                <a href="/shop/${cartItem.modification.id}"><img src="${cartItem.modification.product.photo}" alt="product img" /></a>
                            </div>
                            <div class="shp__pro__details">
                                <h2><a href="/shop/${cartItem.modification.id}">${cartItem.modification.product.name}
                                    <c:forEach var="modificationProperty" items="${cartItem.modification.modificationProperties}">
                                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                                        </c:if>
                                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                                        </c:if>
                                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                                        </c:if>
                                    </c:forEach>
                                </a></h2>
                                <span class="quantity">${cartItem.count} шт.</span>
                                <span class="shp__price">${cartItem.modification.price*cartItem.count} &#8381;</span>
                            </div>
                            <div class="remove__btn">
                                <a href="/cart/delete/${cartItem.id}" title="Удалить из корзины"><i class="zmdi zmdi-close"></i></a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <ul class="shoping__total">
                    <li class="subtotal">Итого:</li>
                    <li class="total__price">${total} &#8381;</li>
                </ul>
                <ul class="shopping__btn">
                    <li><a href="/cart">Корзина</a></li>
                    <li class="shp__checkout"><a href="/checkout">Оформить</a></li>
                </ul>
            </c:if>
        </div>
    </div>
    <!-- End Cart Panel -->
</div>
