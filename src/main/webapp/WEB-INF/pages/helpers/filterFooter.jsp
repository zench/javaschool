<%@ page contentType="text/html;charset=UTF-8" language="java" %>
</div>
<div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-12 col-xs-12 smt-40 xmt-40">
    <div class="htc__product__leftsidebar  bilinfo">
        <!-- Start Prize Range -->
        <form action = "/" method="get">
            <div class="htc-grid-range">
                <h4 class="title__line--4">Цена</h4>
                <div class="content-shopby form-inline">
                    <div class="form-group short single-input">
                        <label class="sr-only" for="priceFrom"></label>
                        <input type="number" name="priceFrom" max = "${maxPrice}" min = "${minPrice}" value = "${((!empty param.priceFrom) && (param.priceFrom != '')) ? param.priceFrom : ''}" id="priceFrom" placeholder="от ${minPrice}">
                    </div>
                    <div class="form-group short single-input">
                        <label class="sr-only" for="priceTo"></label>
                        <input type="number" name="priceTo" max = "${maxPrice}" min = "${minPrice}" value = "${((!empty param.priceTo) && (param.priceTo != '')) ? param.priceTo : ''}" id="priceTo" placeholder="до ${maxPrice}">
                    </div>
                </div>
            </div>
            <!-- End Prize Range -->
            <!-- Start Category Area -->
            <div class="htc__category">
                <h4 class="title__line--4">Бренды</h4>
                <ul class="ht__cat__list mt--10">
                    <c:forEach var="brand" items="${brands}">
                        <c:set var="contains" value="false" />
                        <c:forEach var="brandChecked" items="${paramValues.brand}">
                            <c:if test="${brand.id == brandChecked}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>
                        <li>
                            <input type="checkbox" ${(contains == true) ? 'checked' : ''} name = "brand" id = "brand-${brand.id}" value="${brand.id}" />
                            <label for = "brand-${brand.id}">${brand.value}</label>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <!-- End Category Area -->
            <!-- Start Category Area -->
            <div class="htc__category">
                <h4 class="title__line--4">Сезон</h4>
                <ul class="ht__cat__list">
                    <li class = "single-input">
                    <select name = "season" >
                    <c:forEach var="season" items="${seasons}">
                        <option ${(param.season == season.id) ? 'selected="selected"' : ''} value="${season.id}">${season.value}</option>
                    </c:forEach>
                    </select>
                    </li>
                </ul>
            </div>
            <!-- End Category Area -->
            <!-- Start Category Area -->
            <div class="htc__category">
                <h4 class="title__line--4">Шипы</h4>
                <ul class="ht__cat__list">
                    <li class = "single-input">
                        <select name = "spike" >
                            <option ${(empty param.spike) ? 'selected="selected"' : ''} value = "">Не важно</option>
                            <option ${((param.spike == 0) && (param.spike != "")) ? 'selected="selected"' : ''} value="0">Нет</option>
                            <option ${(param.spike == 1) ? 'selected="selected"' : ''} value="1">Да</option>
                        </select>
                    </li>
                </ul>
            </div>
            <!-- End Category Area -->
            <div class="htc__category">
                <div class="price--filter">
                    <input type="submit" value="Подобрать">
                </div>
            </div>
        </form>
    </div>
</div>
<script type = "text/javascript" src="/static/js/app.js"></script>
