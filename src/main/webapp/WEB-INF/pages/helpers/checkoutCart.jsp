<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-4">
    <div class="order-details">
        <h5 class="order-details__title">Ваш заказ</h5>
        <div class="order-details__item">
            <c:forEach var="cartItem" items="${cartItems}" varStatus="loop">
                <div class="single-item">
                    <div class="single-item__thumb">
                        <img src="${cartItem.modification.product.photo}" alt="ordered item">
                    </div>
                    <div class="single-item__content">
                        <a href="/shop/${cartItem.modification.id}">${cartItem.modification.product.name}
                            <c:forEach var="modificationProperty" items="${cartItem.modification.modificationProperties}">
                                <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                                </c:if>
                                <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                                </c:if>
                                <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                                </c:if>
                            </c:forEach>
                        </a>
                        <span class="price">${cartItem.modification.price} &#8381;</span>
                        <span class="price">${cartItem.count} шт.</span>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="ordre-details__total">
            <h5>Итого:</h5>
            <span class="price">${total} &#8381;</span>
        </div>
    </div>
</div>

