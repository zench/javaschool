<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
    <div class="htc__product__rightidebar">
        <div class="htc__grid__top">
            <div class="htc__select__option">
                <select class="ht__select" name = "orderBy" id = "orderBy">
                    <option ${(param.orderBy == 'price_asc') ? 'selected="selected"' : ''} value = "price_asc">По цене &#8593;</option>
                    <option ${(param.orderBy == 'price_desc') ? 'selected="selected"' : ''} value = "price_desc">По цене &#8595;</option>
                    <option ${(param.orderBy == 'name_asc') ? 'selected="selected"' : ''} value = "name_asc">По названию &#8593;</option>
                    <option ${(param.orderBy == 'name_desc') ? 'selected="selected"' : ''} value = "name_desc">По названию &#8595;</option>
                </select>
            </div>
            <div class="ht__pro__qun">
                <c:if test="${itemsCount > 0}">
                    <span>Показано ${(page-1)*12+1}-${(itemsCount < 12*page) ? itemsCount : (page*12)} из ${itemsCount}</span>
                </c:if>
                <c:if test="${itemsCount == 0}">
                    <span>Ничего не найдено</span>
                </c:if>
            </div>
        </div>
