<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/wideH1.jsp" %>
<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
    <img src = "/static/images/404_1.jpg " alt="Not found" width="600px" />
    <div class="mt--10 buttons-cart">
        <a href = "/">Перейти к покупкам</a>
    </div>
</div>
<%@ include file = "../layouts/footer.jsp" %>