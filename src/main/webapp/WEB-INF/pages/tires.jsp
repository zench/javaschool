<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/filterHeader.jsp" %>
    <div class="row">
        <div class="shop__grid__view__wrap">
            <table class="table">
                <tr>
                    <th>id</th>
                    <th>brandId</th>
                    <th>name</th>
                    <th>produceCountryId</th>
                    <th>action</th>
                </tr>
                <c:forEach var="tire" items="${tiresList}">
                    <tr>
                        <td>${tire.id}</td>
                        <td>${tire.brandId}</td>
                        <td>${tire.name}</td>
                        <td>${tire.produceCountryId}</td>
                        <td>
                            <a href="/edit/${tire.id}">Редактировать</a>
                            <a href="/delete/${tire.id}">Удалить</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<%@ include file = "helpers/pagination.jsp" %>

    <h2>Добавление</h2>
    <c:url value="/add" var="add"/>
    <a href="${add}">Добавить новую шину</a>
<%@ include file = "helpers/filterFooter.jsp" %>
<%@ include file = "layouts/footer.jsp" %>

