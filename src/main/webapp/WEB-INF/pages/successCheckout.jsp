<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>

    <c:if test="${!empty order.id}">
        <h2>Заказ № ${order.id} успешно создан!</h2>
        <p>Мы свяжемся с Вами в ближайшее время для уточнения деталей заказа.</p>
    </c:if>
<%@ include file = "layouts/footer.jsp" %>