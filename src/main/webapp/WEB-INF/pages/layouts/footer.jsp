<%@ page contentType="text/html;charset=UTF-8" language="java" %>
</div>
</div>
</section>
<div class="htc__brand__area bg__cat--4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ht__brand__inner">
                    <ul class="brand__list owl-carousel clearfix">
                        <li><a href="#"><img src="<c:url value="/static/images/brand/1.png"/>" alt="brand images"></a></li>
                        <li><a href="#"><img src="<c:url value="/static/images/brand/2.png"/>" alt="brand images"></a></li>
                        <li><a href="#"><img src="<c:url value="/static/images/brand/3.png"/>" alt="brand images"></a></li>
                        <li><a href="#"><img src="<c:url value="/static/images/brand/4.png"/>" alt="brand images"></a></li>
                        <li><a href="#"><img src="<c:url value="/static/images/brand/5.png"/>" alt="brand images"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Brand Area -->
<!-- Start Footer Area -->
<footer id="htc__footer">
    <!-- Start Footer Widget -->
    <div class="footer__container bg__cat--1">
        <div class="container">
            <div class="row">
                <!-- Start Single Footer Widget -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer">
                        <h2 class="title__line--2">О нас</h2>
                        <div class="ft__details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim</p>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer Widget -->
                <!-- Start Single Footer Widget -->
                <div class="col-md-3 col-sm-6 col-xs-12 xmt-40">
                    <div class="footer">
                        <h2 class="title__line--2">Информация</h2>
                        <div class="ft__inner">
                            <ul class="ft__list">
                                <li><a href="/about">О нас</a></li>
                                <li><a href="/delivery">Доставка</a></li>
                                <li><a href="/personalData">О персональных данных</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer Widget -->
                <!-- Start Single Footer Widget -->
                <div class="col-md-2 col-sm-6 col-xs-12 xmt-40 smt-40">
                    <div class="footer">
                        <h2 class="title__line--2">Аккаунт</h2>
                        <div class="ft__inner">
                            <ul class="ft__list">
                                <li><a href="/user/account">Аккаунт</a></li>
                                <li><a href="/cart">Корзина</a></li>
                                <li><a href="/login">Логин</a></li>
                                <li><a href="/registration">Регистрация</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6 col-xs-12 xmt-40 smt-40">
                    <div class="footer">
                        <h2 class="title__line--2">Контакты</h2>
                        <div class="ft__inner">
                            <ul class="ft__list">
                                <li><a href="/contacts">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Widget -->
    <!-- Start Copyright Area -->
    <div class="htc__copyright bg__cat--5">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright__inner">
                        <p>Copyright© <a href="/">Весёлая шина</a> 2019. Все права защищены.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Copyright Area -->
</footer>
<!-- End Footer Style -->
</div>
<!-- Body main wrapper end -->

<!-- Placed js at the end of the document so the pages load faster -->

<!-- jquery latest version -->
<script src="<c:url value="/static/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/static/js/plugins.js"/>"></script>
<script src="<c:url value="/static/js/slick.min.js"/>"></script>
<script src="<c:url value="/static/js/waypoints.min.js"/>"></script>
<script src="<c:url value="/static/js/main.js"/>"></script>
</body>
</html>
