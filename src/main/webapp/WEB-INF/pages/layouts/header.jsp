<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="ru.t_systems.samushenkov.tires_shop.constants.Constants" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <link rel="shortcut icon" href="<c:url value="/static/favicon.ico"/>" id="favicon">
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/static/css/default.css"/>"/>
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="<c:url value="/static/css/shortcodes.css"/>">
    <link rel="stylesheet" href="<c:url value="/static/css/style.css"/>" type="text/css"/>
    <!-- Responsive css -->
    <link rel="stylesheet" href="<c:url value="/static/css/responsive.css"/>">
    <link rel="stylesheet" href="<c:url value="/static/css/simple-line-icons.css"/>">
    <link rel="stylesheet" href="<c:url value="/static/css/material-design-iconic-font.min.css"/>">
    <!-- User style -->
    <link rel="stylesheet" href="<c:url value="/static/css/custom.css"/>">
    <script src="<c:url value="/static/js/jquery-3.2.1.min.js"/>"></script>
    <c:if test="${!empty title}">
        <title>${title}</title>
    </c:if>
    <c:if test="${empty title}">
        <title>Интернет-магазин шин</title>
    </c:if>
</head>
<body>
<div class="wrapper">
    <!-- Start Header Style -->
    <header id="htc__header" class="htc__header__area header--one">
        <!-- Start Mainmenu Area -->
        <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
            <div class="container">
                <div class="row">
                    <div class="menumenu__container clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                            <div class="logo">
                                <a href="/"><img src="<c:url value="/static/images/logo.png"/>" alt="logo images"></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-7 col-sm-5 col-xs-3">
                            <nav class="main__menu__nav hidden-xs hidden-sm">
                                <ul class="main__menu">
                                    <li class="drop"><a href="/about">О нас</a></li>
                                    <li class="drop"><a href="/">Шины</a>
                                        <ul class="dropdown">
                                            <li><a href="/?season=16">Летние шины</a></li>
                                            <li><a href="/?season=15">Зимние шины</a></li>
                                        </ul>
                                    <!--<li class="drop"><a href="/fitting">Шиномонтаж</a>-->
                                    </li>
                                    <li><a href="/contacts">Контакты</a></li>
                                </ul>
                            </nav>

                            <div class="mobile-menu clearfix visible-xs visible-sm">
                                <nav id="mobile_dropdown">
                                    <ul>
                                        <li><a href="/about">О нас</a></li>
                                        <li><a href="/">Шины</a>
                                            <ul>
                                                <li><a href="blog.html">Летние шины</a></li>
                                                <li><a href="blog-details.html">Зимние шины</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="/contacts">Контакты</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-2 col-sm-4 col-xs-4">
                            <div class="header__right">
                                <div class="header__account">
                                    <c:if test="${empty userId}">
                                        <a href="/login">Войти</a>
                                    </c:if>
                                    <c:if test="${!empty userId}">
                                        <c:if test="${(empty pageContext.request.userPrincipal) || !pageContext.request.isUserInRole('ADMIN')}">
                                            <a href="/user/account" title = "${userName}"><i class="icon-user icons"></i></a>
                                        </c:if>
                                        <c:if test="${(!empty pageContext.request.userPrincipal) && pageContext.request.isUserInRole('ADMIN')}">
                                            <a href="/admin/orders" title = "${userName}"><i class="icon-user icons"></i></a>
                                        </c:if>
                                    </c:if>
                                </div>
                                <c:if test="${!empty userId}">
                                    <div class="header__account">
                                        <a href="/logout">Выйти</a>
                                    </div>
                                </c:if>
                                <c:if test="${(empty pageContext.request.userPrincipal) || !pageContext.request.isUserInRole('ADMIN')}">
                                    <div class="htc__shopping__cart">
                                        <a class="cart__menu" href="#"><i class="icon-basket icons"></i></a>
                                        <c:if test="${(!empty totalCount) && (totalCount > 0)}" >
                                            <a class="cart__menu" href="#"><span class="htc__qua">
                                                ${totalCount}
                                            </span></a>
                                        </c:if>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile-menu-area"></div>
            </div>
        </div>
        <!-- End Mainmenu Area -->
    </header>
    <!-- End Header Area -->

    <div class="body__overlay"></div>
    <c:if test="${(empty pageContext.request.userPrincipal) || !pageContext.request.isUserInRole('ADMIN')}">
            <%@ include file = "../helpers/cartPopup.jsp" %>
    </c:if>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(<c:url value="/static/images/bg/4.jpg"/>) no-repeat scroll center center / cover ;">
        <div class="ht__bradcaump__wrap">
            <div class="container">
            </div>
        </div>
    </div>
    <section class="htc__product__grid bg__white ptb--50">
        <div class="container">
            <div class="row">