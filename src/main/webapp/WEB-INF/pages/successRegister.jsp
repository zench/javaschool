<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>
<h2>
    <c:if test="${!empty user.name}">
        ${user.name}
    </c:if>
    <c:if test="${!empty user.lastName}">
        ${user.lastName}
    </c:if>
 успешно зарегистрирован!</h2>
<div class="mt--10">
    <a href = "/user/account">Перейти в профиль пользователя</a>
</div>
<%@ include file = "layouts/footer.jsp" %>
