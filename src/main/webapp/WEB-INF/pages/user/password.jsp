<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
    <div class="bilinfo">
        <c:if test="${not empty success}">
            <div class="alert alert-success">
                Информация успешно обновлена
            </div>
        </c:if>
        <c:if test="${not empty hasErrors}">
            <div class="alert alert-danger">
                Ошибка, попробуйте еще раз!
            </div>
        </c:if>
        <form action="/user/password/edit" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="password" name = "oldPassword" placeholder="Текущий пароль">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="password" name = "password" placeholder="Новый пароль">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="password" name = "matchingPassword" placeholder="Подтверждение пароля">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="dark-btn mt--10">
                        <input type="submit" value="Обновить">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<%@ include file = "../helpers/userMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>