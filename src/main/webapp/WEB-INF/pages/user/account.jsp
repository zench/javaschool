<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
    <div class="bilinfo">
        <c:if test="${not empty success}">
            <div class="alert alert-success">
                <strong>Информация успешно обновлена</strong>
            </div>
        </c:if>
        <c:if test="${not empty hasErrors}">
            <div class="alert alert-danger">
                <strong>Ошибка, попробуйте еще раз!</strong>
            </div>
        </c:if>
        <c:if test="${(not empty errors) && (not empty tempUser)}">
            <div class="alert alert-danger">
                <strong>Ошибка, попробуйте еще раз!</strong>
                <br/>
                <c:forEach var="error" items="${errors}" varStatus="loop">
                    <c:if test="${error.field != 'birthday'}">
                        ${error.defaultMessage}<br />
                    </c:if>
                    <c:if test="${error.field == 'email'}">
                        <c:set var="emailError" value="has-error" />
                    </c:if>
                    <c:if test="${error.field == 'name'}">
                        <c:set var="nameError" value="has-error" />
                    </c:if>
                    <c:if test="${error.field == 'lastName'}">
                        <c:set var="lastNameError" value="has-error" />
                    </c:if>
                    <c:if test="${error.field == 'birthday'}">
                        <c:set var="birthdayError" value="has-error" />
                        Формат даты неверный<br />
                    </c:if>
                </c:forEach>
            </div>
        </c:if>
        <form action="/user/account/edit" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="text" class = "${nameError}" name = "name" value="${!empty userTemp ? userTemp.name : user.name}" placeholder="Имя">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="text" class = "${lastNameError}"  value="${!empty userTemp ? userTemp.lastName : user.lastName}" name = "lastName" placeholder="Фамилия">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="email" class = "${emailError}" value="${!empty userTemp ? userTemp.email : user.email}" name = "email" placeholder="Email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="date" class = "${birthdayError}" value="${!empty userTemp ? userTemp.birthday : user.birthday}" name = "birthday" placeholder="Дата рождения">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="dark-btn mt--10">
                        <input type="submit" value="Обновить">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <c:if test="${!empty userAddresses}" >
        <h2 class="mt--20">Адреса</h2>
    </c:if>
    <c:forEach var="address" items="${userAddresses}" varStatus="loop">
        <c:set var="cityError" value="" />
        <c:set var="streetError" value="" />
        <c:set var="houseError" value="" />
        <c:set var="zipError" value="" />
        <c:if test="${(not empty errors) && (not empty tempUserAddress) && (tempUserAddress.id == address.id)}">
        <div class="alert alert-danger mt--20">
            <c:forEach var="error" items="${errors}" varStatus="loop">
                ${error.defaultMessage}<br />
                <c:if test="${(!empty error.field) && (error.field == 'city')}">
                    <c:set var="cityError" value="has-error" />
                </c:if>
                <c:if test="${(!empty error.field) && (error.field == 'street')}">
                    <c:set var="streetError" value="has-error" />
                </c:if>
                <c:if test="${(!empty error.field) && (error.field == 'house')}">
                    <c:set var="houseError" value="has-error" />
                </c:if>
                <c:if test="${(!empty error.field) && (error.field == 'zip')}">
                    <c:set var="zipError" value="has-error" />
                </c:if>
            </c:forEach>
        </div>
        </c:if>
        <div class="bilinfo bg__cat--4 mt--20">
            <form action="/user/address/edit" method="post">
                <input type = "hidden" name = "id" value = "${address.id}" />
                <div class="row">
                    <div class="col-md-12">
                        <div class="single-input mt-0">
                            <select name="country.id" id="bil-country">
                                <c:forEach var="country" items="${countriesList}">
                                    <option ${(address.country.id == country.id) ? 'selected="selected"' : ''} value="${country.id}">${country.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-input">
                            <input type="text" class = "${cityError}" name = "city" value = "${(!empty tempUserAddress && (tempUserAddress.id == address.id)) ? tempUserAddress.city : address.city}" placeholder="Город">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-input">
                            <input type="text" class = "${zipError}" name = "zip" value = "${(!empty tempUserAddress && (tempUserAddress.id == address.id)) ? tempUserAddress.zip : address.zip}" placeholder="Почтовый код">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="single-input">
                            <input type="text" class = "${streetError}" name = "street" value = "${(!empty tempUserAddress && (tempUserAddress.id == address.id)) ? tempUserAddress.street : address.street}" placeholder="Улица">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-input">
                            <input type="text" class = "${houseError}" name = "house" value = "${(!empty tempUserAddress && (tempUserAddress.id == address.id)) ? tempUserAddress.house : address.house}" placeholder="Дом">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-input">
                            <input type="text" name = "flat" value = "${(!empty tempUserAddress && (tempUserAddress.id == address.id)) ? tempUserAddress.flat : address.flat}" placeholder="Квартира">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="dark-btn mt--10">
                            <input type="submit" value="Обновить">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </c:forEach>
</div>
<%@ include file = "../helpers/userMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>
