<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
    <table class="table table-striped">
        <tr>
            <th>№</th>
            <th>Дата создания</th>
            <th width = "30%">Позиции</th>
            <th>Контактное лицо</th>
            <th>Телефон</th>
            <th>Адрес</th>
            <th>Сумма заказа</th>
            <th>Статус</th>
        </tr>
        <c:forEach var="order" items="${ordersList}">
            <tr>
                    <td>${order.id}</td>
                    <td>
                        <fmt:parseDate  value="${order.created}"  type="date" pattern="yyyy-MM-dd'T'HH:mm:ss" var="parsedDateTime" />
                        <fmt:formatDate value="${parsedDateTime}" type="date" pattern="dd.MM.yyyy HH:mm:ss" var="stdDatum" />
                            ${stdDatum}
                    </td>
                    <td>
                        <c:forEach var="item" items="${order.getContentItems()}">
                            <div class="mb--10">
                                <a href = "/shop/${item.modificationId}"><img width = "50px" alt = "product image" src = "${item.photoUrl}" /></a><br/>
                                <a href = "/shop/${item.modificationId}">${item.name}</a><br/>
                                Цена: ${item.price} &#8381;<br/>
                                Кол-во: ${item.count} шт.<br/>
                                <c:forEach items="${item.properties}" var="entry">
                                    ${entry.key}: ${entry.value}<br />
                                </c:forEach>
                            </div>
                        </c:forEach>
                    </td>
                    <td>
                            ${order.name}
                        <c:if test="${!empty order.lastName}">
                            ${order.lastName}
                        </c:if>
                    </td>
                    <td>
                            ${order.phone}
                    </td>
                    <td>
                            ${order.userAddress.zip}, ${order.userAddress.city}, ${order.userAddress.street}, ${order.userAddress.house},
                        <c:if test="${!empty order.userAddress.flat}">
                            ${order.userAddress.flat}
                        </c:if>
                    </td>
                    <td>
                            ${order.total} &#8381;
                    </td>
                    <td>
                        ${order.status.value}<br />
                        ${order.paymentType.value}<br />
                        ${order.deliveryType.value}<br />
                        ${order.paymentStatus.value}
                    </td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@ include file = "../helpers/userMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>