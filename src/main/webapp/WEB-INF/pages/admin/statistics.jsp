<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<%@ include file = "../helpers/statisticsFilterHelper.jsp" %>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>
