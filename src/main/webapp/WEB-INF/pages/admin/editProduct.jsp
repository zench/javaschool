<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <c:if test="${(empty product.id) || (product.id == 0)}">
        <c:url value="/admin/products/add" var="formSubmitUrl"/>
    </c:if>
    <c:if test="${(!empty product.id) && (product.id != 0)}">
        <c:url value="/admin/products/edit" var="formSubmitUrl"/>
    </c:if>
    <h2><a href = "/admin/products/${product.id}/modifications" class="big-link">Модификации</a></h2>
    <form action="${formSubmitUrl}" method="POST" enctype="multipart/form-data">
        <c:if test="${(!empty product.id) && (product.id != 0)}">
            <input type="hidden" name="id" value="${product.id}">
        </c:if>
        <input type="hidden" name="category.id" value="${product.category.id}"/>
        <div class="form-group">
            <label for="name">Название товара</label>
            <c:if test="${empty product.name}">
                <input class="form-control" type="text" name="name" id="name">
            </c:if>
            <c:if test="${!empty product.name}">
                <input class="form-control" type="text" name="name" value="${product.name}">
            </c:if>
        </div>
        <div class="form-group">
            <c:if test="${!empty product.photo}">
                <img src="${product.photo}" height="100px" alt = "product image" />
            </c:if>
        </div>
        <div class="form-group">
            <label for="image">Загрузить изображение</label>
            <input id = "image" type="file" value="${!empty product.photo ? product.photo : ''}" name="image"/>
        </div>
        <div class="form-group">
            <label for="country">Страна-производитель</label>
            <select class="form-control" name="producerCountry.id" id = "country">
                <c:forEach var="country" items="${countriesList}">
                    <option ${(country.id == product.producerCountry.id) ? 'selected="selected"' : ''} value="${country.id}">${country.name}</option>
                </c:forEach>
            </select>
        </div>
        <c:forEach var="productProperty" items="${product.productProperties}" varStatus="loop">
            <div class="form-group">
                <input type="hidden" name="productProperties[${loop.index}].property.id" value="${productProperty.property.id}">
                <input type="hidden" name="productProperties[${loop.index}].id" value="${productProperty.id}">
                <label for="property-${productProperty.property.id}">${productProperty.property.name}</label>
                <c:choose>
                    <c:when test="${productProperty.property.type.name == 'enum'}">
                        <select class="form-control" id = "property-${productProperty.property.id}" name="productProperties[${loop.index}].value">
                            <c:forEach var="peValue" items="${productProperty.property.propertyEnumValues}">
                                <option ${(peValue.id == productProperty.value) ? 'selected="selected"' : ''} value="${peValue.id}">${peValue.value}</option>
                            </c:forEach>
                        </select>
                    </c:when>
                    <c:when test="${productProperty.property.type.name == 'bool'}">
                        <select class="form-control" id = "property-${productProperty.property.id}" name="productProperties[${loop.index}].value">
                            <option ${((!empty productProperty.value) && (productProperty.value == 1)) ? 'selected="selected"' : ''} value="1">Да</option>
                            <option ${((empty productProperty.value) || (productProperty.value == 0)) ? 'selected="selected"' : ''} value="0">Нет</option>
                        </select>
                    </c:when>
                    <c:otherwise>
                        <input class="form-control" type="text" id = "property-${productProperty.property.id}" name="productProperties[${loop.index}].value" value="${productProperty.value}">
                    </c:otherwise>
                </c:choose>
            </div>
        </c:forEach>
        <c:if test="${(empty product.id) || (product.id == 0)}">
            <input class="btn btn-default" type="submit" value="Добавить">
        </c:if>
        <c:if test="${(!empty product.id) && (product.id != 0)}">
            <input class="btn btn-default" type="submit" value="Обновить">
        </c:if>
    </form>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>

