<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<%@ include file = "../helpers/statisticsFilterHelper.jsp" %>
<%@ include file = "../helpers/adminMenu.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12 mt--20">
    <table class="table table-striped">
        <tr>
            <th>Продукт</th>
            <th>Количество</th>
            <th>Сумма</th>
        </tr>
        <c:forEach var="item" items="${products}">
            <tr>
                <td><a href = "/shop/${item.modification.id}">${item.modification.product.name}
                    <c:forEach var="modificationProperty" items="${item.modification.modificationProperties}">
                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                        </c:if>
                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                        </c:if>
                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                        </c:if>
                    </c:forEach>
                    </a>
                </td>
                <td>${item.totalCount} шт.</td>
                <td>${item.totalIncome} &#8381;</td>
            </tr>
        </c:forEach>
    </table>
    <%@ include file = "../helpers/pagination.jsp" %>
</div>
<%@ include file = "../layouts/footer.jsp" %>
