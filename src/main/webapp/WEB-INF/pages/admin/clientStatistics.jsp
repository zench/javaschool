<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<%@ include file = "../helpers/statisticsFilterHelper.jsp" %>
<%@ include file = "../helpers/adminMenu.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12 mt--20">
    <table class="table table-striped">
        <tr>
            <th>Клиент</th>
            <th>Количество</th>
            <th>Сумма</th>
        </tr>
    <c:forEach var="client" items="${clients}">
        <tr>
            <td>${client.user.name}
            <c:if test="${!empty client.user.lastName}" >
                ${client.user.lastName}
            </c:if>
                (${client.user.email})
            </td>
            <td>${client.totalCount} шт.</td>
            <td>${client.totalIncome} &#8381;</td>
        </tr>
    </c:forEach>
    </table>
    <%@ include file = "../helpers/pagination.jsp" %>
</div>
<%@ include file = "../layouts/footer.jsp" %>