<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <table class="table table-striped">
        <tr>
            <th>Название</th>
            <th>Категория</th>
            <th>Страна производства</th>
            <th>Действие</th>
        </tr>
        <c:forEach var="product" items="${productsList}">
            <tr>
                <td>${product.name}</td>
                <td>
                    ${product.category.name}
                </td>
                <td>
                    ${product.producerCountry.name}
                </td>
                <td>
                    <a href="/admin/products/edit/${product.id}" title="Редактировать"><i class="icon-pencil icons"></i></a>
                    <a href="/admin/products/${product.id}/modifications" title="Модификации"><i class="icon-list icons"></i></a>
                    <a href="/admin/products/delete/${product.id}" title="Удалить"><i class="icon-close icons"></i></a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a class="btn btn-success" href="/admin/categories/${category.id}/products/add" role="button">Добавить</a>
    <%@ include file = "../helpers/pagination.jsp" %>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>
