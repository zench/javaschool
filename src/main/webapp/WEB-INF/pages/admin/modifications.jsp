<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <h2>${product.name}</h2>
    <table class="table table-striped">
        <tr>
            <th>Название</th>
            <th>Цена</th>
            <th>Остаток на складе</th>
            <th>Действие</th>
        </tr>
        <c:forEach var="modification" items="${modificationsList}">
            <tr>
                <td>${product.name}
                    <c:forEach var="modificationProperty" items="${modification.modificationProperties}">
                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                        </c:if>
                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                        </c:if>
                        <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                        </c:if>
                    </c:forEach>
                </td>
                <td>
                    ${modification.price} &#8381;
                </td>
                <td>
                    ${modification.stockBalance} шт.
                </td>
                <td>
                    <a href="/admin/modifications/edit/${modification.id}" title="Редактировать"><i class="icon-pencil icons"></i></a>
                    <a href="/admin/modifications/delete/${modification.id}" title="Удалить"><i class="icon-close icons"></i></a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a class="btn btn-success" href="/admin/products/${product.id}/modifications/add" role="button">Добавить</a>
    <div class = "mt--10">
        <a href="/admin/categories/${product.category.id}/products">Вернуться к списку товаров</a>
    </div>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>