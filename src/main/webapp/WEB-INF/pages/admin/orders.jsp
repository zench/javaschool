<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<c:url value="/admin/order/edit?${requestScope['javax.servlet.forward.query_string']}" var="orderEditPage"/>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <table class="table table-striped">
        <tr>
            <th>№</th>
            <th>Дата создания</th>
            <th>Позиции</th>
            <th>Клиент</th>
            <th>Телефон</th>
            <th>Адрес</th>
            <th>Сумма заказа</th>
            <th width = "15%">Статусы</th>
        </tr>
        <c:forEach var="order" items="${ordersList}">
            <tr>
            <form action = "${orderEditPage}" method="post">
                <td>${order.id}</td>
                <td>
                    <fmt:parseDate  value="${order.created}"  type="date" pattern="yyyy-MM-dd'T'HH:mm:ss" var="parsedDateTime" />
                    <fmt:formatDate value="${parsedDateTime}" type="date" pattern="dd.MM.yyyy HH:mm:ss" var="stdDatum" />
                    ${stdDatum}
                </td>
                <td>
                    <c:forEach var="item" items="${order.orderStatistics}">
                        <div class="mb--10">
                            <a href = "/shop/${item.modification.id}">${item.modification.product.name}
                                <c:forEach var="modificationProperty" items="${item.modification.modificationProperties}">
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                                    </c:if>
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                                    </c:if>
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                                    </c:if>
                                </c:forEach>
                            </a><br/>
                            ${item.price} &#8381;<br/>
                            ${item.count} шт.
                        </div>
                    </c:forEach>
                </td>
                <td>
                    ${order.name}
                    <c:if test="${!empty order.lastName}">
                        ${order.lastName}
                    </c:if>
                </td>
                <td>
                    ${order.phone}
                </td>
                <td>
                    ${order.userAddress.zip}, ${order.userAddress.city}, ${order.userAddress.street}, ${order.userAddress.house},
                    <c:if test="${!empty order.userAddress.flat}">
                        ${order.userAddress.flat}
                    </c:if>
                </td>
                <td>
                    ${order.total} &#8381;
                </td>
                <td>
                    <div class="form-group">
                        <select class="form-control" name = "status.id">
                            <c:forEach var="status" items="${statusesList}">
                                <option ${(status.id == order.status.id) ? 'selected="selected"' : ''} value="${status.id}">${status.value}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name = "paymentType.id">
                            <c:forEach var="paymentType" items="${paymentTypesList}">
                                <option ${(paymentType.id == order.paymentType.id) ? 'selected="selected"' : ''} value="${paymentType.id}">${paymentType.value}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name = "deliveryType.id">
                            <c:forEach var="deliveryType" items="${deliveryTypesList}">
                                <option ${(deliveryType.id == order.deliveryType.id) ? 'selected="selected"' : ''} value="${deliveryType.id}">${deliveryType.value}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name = "paymentStatus.id">
                            <c:forEach var="paymentStatus" items="${paymentStatusesList}">
                                <option ${(paymentStatus.id == order.paymentStatus.id) ? 'selected="selected"' : ''} value="${paymentStatus.id}">${paymentStatus.value}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type = "hidden" name = "id" value="${order.id}" />
                        <input class="btn" type = "submit" value = "Обновить" />
                    </div>
                </td>
            </form>
            </tr>
        </c:forEach>
    </table>
    <%@ include file = "../helpers/pagination.jsp" %>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>