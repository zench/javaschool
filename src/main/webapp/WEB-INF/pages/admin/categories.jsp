<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <table class="table table-striped">
        <tr>
            <th>Название</th>
            <th>Родительская категория</th>
            <th>Действие</th>
        </tr>
        <c:forEach var="category" items="${categoriesList}">
            <tr>
                <td>${category.name}</td>
                <td>
                    <c:if test="${empty category.parentCategoryId}">
                        ---
                    </c:if>
                    <c:if test="${!empty category.parentCategoryId}">
                            ${category.parentCategoryId}
                    </c:if>
                </td>
                <td>
                    <a href="/admin/categories/edit/${category.id}" title="Редактировать"><i class="icon-pencil icons"></i></a>
                    <a href="/admin/categories/${category.id}/products" title="Товары"><i class="icon-list icons"></i></a>
                    <a href="/admin/categories/${category.id}/properties" title="Свойства"><i class="icon-settings icons"></i></a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a class="btn btn-success" href="/admin/categories/add" role="button">Добавить</a>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>
