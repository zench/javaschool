<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <c:if test="${(empty modification.id) || (modification.id == 0)}">
        <c:url value="/admin/modifications/add" var="formSubmitUrl"/>
    </c:if>
    <c:if test="${(!empty modification.id) && (modification.id != 0)}">
        <c:url value="/admin/modifications/edit" var="formSubmitUrl"/>
    </c:if>
    <h2>${modification.product.name}</h2>
    <form action="${formSubmitUrl}" method="POST">
        <c:if test="${!empty modification.id}">
            <input type="hidden" name="id" value="${modification.id}">
        </c:if>
        <input type="hidden" name="product.id" value="${empty product ? modification.product.id : product.id}">
        <div class="form-group">
            <label for="price">Цена*</label>
            <input class="form-control" type="text" id = "price" name="price" value="${empty modification.price ? "" : modification.price}">
        </div>
        <c:forEach var="modificationProperty" items="${modification.modificationProperties}" varStatus="loop">
        <div class="form-group">
            <input type="hidden" name="modificationProperties[${loop.index}].property.id" value="${modificationProperty.property.id}">
            <input type="hidden" name="modificationProperties[${loop.index}].id" value="${modificationProperty.id}">
            <label for="property-${modificationProperty.property.id}">${modificationProperty.property.name}</label>
            <input class="form-control" type="text" id = "property-${modificationProperty.property.id}" name="modificationProperties[${loop.index}].value" value="${modificationProperty.value}">
        </div>
        </c:forEach>
        <div class="form-group">
            <label for="stockBalance">Остаток на складе*</label>
            <input class="form-control" type="text" id = "stockBalance" name="stockBalance" value="${!empty modification.stockBalance ? modification.stockBalance : 0}">
        </div>
        <div class="form-group">
            <label for="sort">Сортировка*</label>
            <input class="form-control" type="text" id = "sort" name="sort" value="${!empty modification.sort ? modification.sort : 1}">
        </div>
        <c:if test="${(empty modification.id) || (modification.id == 0)}">
            <input class="btn btn-default" type="submit" value="Добавить">
        </c:if>
        <c:if test="${(!empty modification.id) && (modification.id != 0)}">
            <input class="btn btn-default" type="submit" value="Обновить">
        </c:if>
    </form>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>