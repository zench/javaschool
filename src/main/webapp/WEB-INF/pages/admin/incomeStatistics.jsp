<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<%@ include file = "../helpers/statisticsFilterHelper.jsp" %>
<%@ include file = "../helpers/adminMenu.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12 mt--20">
    <h2>Выручка:
        <c:if test="${!empty income}">
            ${income} &#8381;
        </c:if>
    </h2>
</div>
<%@ include file = "../layouts/footer.jsp" %>
