<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <c:if test="${empty property.id}">
        <c:url value="/admin/properties/add" var="formSubmitUrl"/>
    </c:if>
    <c:if test="${!empty property.id}">
        <c:url value="/admin/properties/edit" var="formSubmitUrl"/>
    </c:if>
    <h2>Категория: ${category.name}</h2>
    <form action="${formSubmitUrl}" method="POST">
        <c:if test="${!empty property.id}">
            <input type="hidden" name="id" value="${property.id}">
        </c:if>
        <input type="hidden" name="categoryId" value="${category.id}">
        <div class="form-group">
            <label for="name">Название</label>
            <input class="form-control" type="text" name="name" id="name" value="${(!empty property.name) ? property.name : ''}" />
        </div>
        <div class="form-group">
            <label for="typeId">Тип свойства</label>
            <select class="form-control" name="type.id" id = "typeId">
                <c:forEach var="type" items="${types}">
                    <option ${(type.id == property.type.id) ? 'selected="selected"' : ''} value="${type.id}">${type.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="unit">Единица измерения</label>
            <select class="form-control" name="unit.id" id = "unit">
                    <option value="0"></option>
                <c:forEach var="unit" items="${units}">
                    <option ${(unit.id == property.unit.id) ? 'selected="selected"' : ''} value="${unit.id}">${unit.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="unit">Продукт/Модификация</label>
            <select class="form-control" name="entityType.id" id = "entityType">
                <c:forEach var="entityType" items="${entityTypes}">
                    <option ${(entityType.id == property.entityType.id) ? 'selected="selected"' : ''} value="${entityType.id}">${entityType.value}</option>
                </c:forEach>
            </select>
        </div>
        <c:if test="${empty property.id}">
            <input class="btn btn-default" type="submit" value="Добавить">
        </c:if>
        <c:if test="${!empty property.id}">
            <input class="btn btn-default" type="submit" value="Обновить">
        </c:if>
    </form>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>