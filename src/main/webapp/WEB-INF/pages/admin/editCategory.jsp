<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../layouts/header.jsp" %>
<%@ include file = "../helpers/narrowH1.jsp" %>
<div class="col-lg-10 col-lg-push-3 col-md-10 col-md-push-3 col-sm-12 col-xs-12">
    <c:if test="${empty category.name}">
        <c:url value="/admin/categories/add" var="formSubmitUrl"/>
    </c:if>
    <c:if test="${!empty category.name}">
        <c:url value="/admin/categories/edit" var="formSubmitUrl"/>
    </c:if>
    <h2><a href = "/admin/categories/${category.id}/properties" class="big-link">Свойства</a></h2>
    <h2><a href = "/admin/categories/${category.id}/products" class="big-link">Товары</a></h2>
    <form action="${formSubmitUrl}" method="POST">
        <c:if test="${!empty category.name}">
            <input type="hidden" name="id" value="${category.id}">
        </c:if>
        <div class="form-group">
            <label for="name">Название категории</label>
            <c:if test="${empty category.name}">
                <input class="form-control" type="text" name="name" id="name">
            </c:if>
            <c:if test="${!empty category.name}">
                <input class="form-control" type="text" name="name" value="${category.name}">
            </c:if>
        </div>
        <c:if test="${empty category.name}">
            <input class="btn btn-default" type="submit" value="Добавить">
        </c:if>
        <c:if test="${!empty category.name}">
            <input class="btn btn-default" type="submit" value="Обновить">
        </c:if>
    </form>
</div>
<%@ include file = "../helpers/adminMenu.jsp" %>
<%@ include file = "../layouts/footer.jsp" %>
