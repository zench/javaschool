<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>

<c:set var="emailError" value="" />
<c:set var="nameError" value="" />
<c:set var="lastNameError" value="" />
<c:set var="passwordError" value="" />
<c:set var="birthdayError" value="" />
<div class="accordion__body col-lg-12 col-sm-12 col-xs-12">
<c:if test="${!empty userId}">
    <h2>Вы уже зарегистрированы!</h2>
</c:if>
<c:if test="${empty userId}">
    <c:if test="${not empty errors}">
        <div class="alert alert-danger">
            <strong>Ошибка регистрации</strong>
            <br/>
            <c:forEach var="error" items="${errors}" varStatus="loop">
                ${error.defaultMessage}<br />
                <c:catch var="exception"><c:if test="${(!empty error.field)}"></c:if></c:catch>
                <c:if test="${empty exception}">
                    <c:if test="${(!empty error.field) && (error.field == 'email')}">
                        <c:set var="emailError" value="has-error" />
                    </c:if>
                    <c:if test="${(!empty error.field) && (error.field == 'name')}">
                        <c:set var="nameError" value="has-error" />
                    </c:if>
                    <c:if test="${(!empty error.field) && (error.field == 'lastName')}">
                        <c:set var="lastNameError" value="has-error" />
                    </c:if>
                    <c:if test="${(!empty error.field) && (error.field == 'password')}">
                        <c:set var="passwordError" value="has-error" />
                    </c:if>
                    <c:if test="${(!empty error.field) && (error.field == 'birthday')}">
                        <c:set var="birthdayError" value="has-error" />
                    </c:if>
                </c:if>
            </c:forEach>
        </div>
    </c:if>
    <div class="bilinfo">
        <form action="/registration" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="text" class = "${nameError}" value="${!empty userDto.name ? userDto.name : ''}" name = "name" placeholder="Имя">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="text" class = "${lastNameError}" value="${!empty userDto.lastName ? userDto.lastName : ''}"  name = "lastName" placeholder="Фамилия">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="email" class = "${emailError}" value="${!empty userDto.email ? userDto.email : ''}" name = "email" placeholder="Email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="date" class = "${birthdayError}" value="${!empty userDto.birthday ? userDto.birthday : ''}" name = "birthday" placeholder="Дата рождения">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="password" class = "${passwordError}" name = "password" placeholder="Пароль">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-input">
                        <input type="password" class = "${passwordError}" name = "matchingPassword" placeholder="Подтверждение пароля">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="dark-btn mt--10">
                        <input type="submit" value="Регистрация">
                    </div>
                </div>
            </div>
        </form>
    </div>
</c:if>
</div>
<%@ include file = "layouts/footer.jsp" %>
