<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>
<c:if test="${!empty cartItems}">
<div class="checkout-wrap ptb--100">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="checkout__inner">
                    <div class="accordion-list">
                        <div class="accordion">
                            <c:if test="${empty userId}">
                                <div class="accordion__title">
                                    Авторизация
                                </div>
                                <div class="accordion__body">
                                    <div class="accordion__body__form">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="checkout-method">
                                                    <div class="checkout-method__single">
                                                        <h5 class="checkout-method__title"><i class="zmdi zmdi-caret-right"></i>Для оформления заказа войдите в свой профиль</h5>
                                                        <p><a href = "/registration">Зарегистрироваться</a> / <a href = "/login">Войти</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${!empty userId}">
                                <div class="accordion__title">
                                    Адрес доставки
                                </div>
                                <c:if test="${not empty errors}">
                                    <div class="alert alert-danger">
                                        <strong>Ошибка оформления</strong>
                                        <br/>
                                        <c:forEach var="error" items="${errors}" varStatus="loop">
                                            ${error.defaultMessage}<br />
                                            <c:catch var="exception"><c:if test="${(!empty error.field)}"></c:if></c:catch>
                                            <c:if test="${empty exception}">
                                                <c:if test="${(!empty error.field) && (error.field == 'name')}">
                                                    <c:set var="nameError" value="has-error" />
                                                </c:if>
                                                <c:if test="${(!empty error.field) && (error.field == 'lastName')}">
                                                    <c:set var="lastNameError" value="has-error" />
                                                </c:if>
                                                <c:if test="${(!empty error.field) && (error.field == 'userAddress.city')}">
                                                    <c:set var="cityError" value="has-error" />
                                                </c:if>
                                                <c:if test="${(!empty error.field) && (error.field == 'userAddress.street')}">
                                                    <c:set var="streetError" value="has-error" />
                                                </c:if>
                                                <c:if test="${(!empty error.field) && (error.field == 'userAddress.house')}">
                                                    <c:set var="houseError" value="has-error" />
                                                </c:if>
                                                <c:if test="${(!empty error.field) && (error.field == 'userAddress.zip')}">
                                                    <c:set var="zipError" value="has-error" />
                                                </c:if>
                                                <c:if test="${(!empty error.field) && (error.field == 'phone')}">
                                                    <c:set var="phoneError" value="has-error" />
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </c:if>
                                <div class="accordion__body">
                                    <div class="bilinfo">
                                        <form action="/checkout" method="post">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="single-input mt-0">
                                                        <c:set var="savedCountry" value="" />
                                                        <c:if test="${!empty lastOrder && !empty lastOrder.userAddress.country}" >
                                                            <c:set var="savedCountry" value="${lastOrder.userAddress.country.id}" />
                                                        </c:if>
                                                        <select name = "userAddress.country.id">
                                                            <c:forEach var="country" items="${countriesList}">
                                                                <option ${(country.id == savedCountry) ? 'selected="selected"' : ''} value="${country.id}">${country.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" class = "${nameError}" name = "name" value = "${(!empty lastOrder && !empty lastOrder.name) ? lastOrder.name : ''}${(empty lastOrder || empty lastOrder.name) ? user.name : ''}" placeholder="Имя">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" class = "${lastNameError}" name = "lastName" value = "${(!empty lastOrder && !empty lastOrder.lastName) ? lastOrder.lastName : ''}${(empty lastOrder || empty lastOrder.lastName) ? user.lastName : ''}"  placeholder="Фамилия">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" class = "${cityError}" name = "userAddress.city" value = "${(!empty lastOrder && !empty lastOrder.userAddress.city) ? lastOrder.userAddress.city : ''}" placeholder="Город">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" class = "${zipError}" name = "userAddress.zip" value = "${(!empty lastOrder && !empty lastOrder.userAddress.zip) ? lastOrder.userAddress.zip : ''}" placeholder="Почтовый код">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="single-input">
                                                        <input type="text" class = "${streetError}" name = "userAddress.street" value = "${(!empty lastOrder && !empty lastOrder.userAddress.street) ? lastOrder.userAddress.street : ''}" placeholder="Улица">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" class = "${houseError}" name = "userAddress.house" value = "${(!empty lastOrder && !empty lastOrder.userAddress.house) ? lastOrder.userAddress.house : ''}" placeholder="Дом">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" name = "userAddress.flat" value = "${(!empty lastOrder && !empty lastOrder.userAddress.flat) ? lastOrder.userAddress.flat : ''}" placeholder="Квартира">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <input type="text" class = "${phoneError}" name = "phone" value = "${(!empty lastOrder && !empty lastOrder.phone) ? lastOrder.phone : ''}" placeholder="Телефон">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <select name = "paymentType.id">
                                                            <c:forEach var="paymentType" items="${paymentTypesList}">
                                                                <option ${(!empty(lastOrder) && !empty(lastOrder.paymentType) && (paymentType.id == lastOrder.paymentType.id)) ? 'selected="selected"' : ''} value="${paymentType.id}">${paymentType.value}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="single-input">
                                                        <select name = "deliveryType.id">
                                                            <c:forEach var="deliveryType" items="${deliveryTypesList}">
                                                                <option ${(!empty(lastOrder) && !empty(lastOrder.deliveryType) && (deliveryType.id == lastOrder.deliveryType.id)) ? 'selected="selected"' : ''} value="${deliveryType.id}">${deliveryType.value}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="dark-btn mt--10">
                                                        <input type="submit" value="Оформить">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <c:if test="${!empty userId}">
                <%@ include file = "helpers/checkoutCart.jsp" %>
            </c:if>
        </div>
    </div>
</div>
</c:if>
<c:if test="${empty cartItems}">
    <h2>Корзина пуста</h2>
    <div class="mt--10 buttons-cart">
        <a href = "/">Перейти к покупкам</a>
    </div>
</c:if>
<%@ include file = "layouts/footer.jsp" %>
