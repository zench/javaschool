<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file = "layouts/header.jsp" %>
<%@ include file = "helpers/wideH1.jsp" %>
<div class="row">
    <section class="htc__product__details bg__white ptb--100">
        <!-- Start Product Details Top -->
        <div class="htc__product__details__top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                        <div class="htc__product__details__tab__content">
                            <!-- Start Product Big Images -->
                            <div class="product__big__images">
                                <div class="portfolio-full-image tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="img-tab-1">
                                        <img src="${modification.product.photo}" alt="full-image">
                                    </div>
                                </div>
                            </div>
                            <!-- End Product Big Images -->
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12 smt-40 xmt-40">
                        <div class="ht__product__dtl">
                            <h2><c:forEach var="productProperty" items="${productProperties}" varStatus="loop">
                                <c:if test="${productProperty.property.id == Constants.BRAND_PROPERTY_ID}">
                                    <c:forEach var="peValue" items="${productProperty.property.propertyEnumValues}">
                                        <c:if test = "${(peValue.id == productProperty.value)}">${peValue.value}
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </c:forEach>
                                ${modification.product.name}
                                <c:forEach var="modificationProperty" items="${modification.modificationProperties}">
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.WIDTH_PROPERTY_ID)}">${modificationProperty.value}/
                                    </c:if>
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.PROFILE_PROPERTY_ID)}">${modificationProperty.value}/
                                    </c:if>
                                    <c:if test="${(!empty modificationProperty.id) && (modificationProperty.property.id == Constants.DIAMETER_PROPERTY_ID)}">R${modificationProperty.value}
                                    </c:if>
                                </c:forEach>
                            </h2>
                            <ul  class="pro__prize">
                                <li>${modification.price} &#8381;</li>
                            </ul>
                            <div class="ht__pro__desc">
                                <div class="sin__desc">
                                    <p><span>На складе:</span> ${modification.stockBalance} шт.</p>
                                </div>
                                <c:forEach var="modificationProperty" items="${modification.modificationProperties}">
                                    <div class="sin__desc align--left">
                                        <p>
                                            <b>${modificationProperty.property.name}:</b> <span> ${modificationProperty.value}</span> <span> ${(!empty modificationProperty.property.entityType.value) ? modificationProperty.property.unit.name : ""}</span>
                                        </p>
                                    </div>
                                </c:forEach>
                                <c:forEach var="productProperty" items="${productProperties}" varStatus="loop">
                                    <div class="sin__desc align--left">
                                        <p>
                                            <b>${productProperty.property.name}:</b>
                                            <span>
                                                <c:if test="${(productProperty.property.type.name == 'bool') && (productProperty.value == '0')}">
                                                    Нет
                                                </c:if>
                                                 <c:if test="${(productProperty.property.type.name == 'bool') && (productProperty.value == '1')}">
                                                     Да
                                                 </c:if>
                                                <c:if test="${(productProperty.property.type.name == 'enum')}">
                                                    <c:forEach var="peValue" items="${productProperty.property.propertyEnumValues}">
                                                        <c:if test = "${(peValue.id == productProperty.value)}">${peValue.value}
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </span>
                                            <span> ${(!empty productProperty.property.entityType.value) ? productProperty.property.unit.name : ""}</span>
                                        </p>
                                    </div>
                                </c:forEach>
                                <c:if test="${modification.stockBalance > 0}">
                                    <div class="sin__desc align--left">
                                        <div class="buttons-cart checkout--btn">
                                            <a href="/cart/${modification.id}/1">купить</a>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Product Details Top -->
    </section>
    <!-- End Product Details Area -->
</div>
</div>
<%@ include file = "layouts/footer.jsp" %>

