<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "layouts/header.jsp" %>
    <c:if test="${empty tire.name}">
        <c:url value="/add" var="var"/>
    </c:if>
    <c:if test="${!empty tire.name}">
        <c:url value="/edit" var="var"/>
    </c:if>
    <form action="${var}" method="POST">
        <c:if test="${!empty tire.name}">
            <input type="hidden" name="id" value="${tire.id}">
        </c:if>
        <label for="name">Name</label>
        <c:if test="${empty tire.name}">
            <input type="text" name="name" id="name">
        </c:if>
        <c:if test="${!empty tire.name}">
            <input type="text" name="name" value="${tire.name}">
        </c:if>
        <label for="brandId">BrandId</label>
        <c:if test="${empty tire.brandId}">
            <input type="text" name="brandId" id="brandId">
        </c:if>
        <c:if test="${!empty tire.brandId}">
            <input type="text" name="brandId" id="brandId" value="${tire.brandId}">
        </c:if>
        <label for="produceCountryId">ProduceCountryId</label>
        <c:if test="${empty tire.produceCountryId}">
            <input type="text" name="produceCountryId" id="produceCountryId">
        </c:if>
        <c:if test="${!empty tire.produceCountryId}">
            <input type="text" name="produceCountryId" id="produceCountryId" value="${tire.produceCountryId}">
        </c:if>
        <c:if test="${empty tire.name}">
            <input type="submit" value="Добавить">
        </c:if>
        <c:if test="${!empty tire.name}">
            <input type="submit" value="Обновить">
        </c:if>
    </form>
<%@ include file = "layouts/footer.jsp" %>
