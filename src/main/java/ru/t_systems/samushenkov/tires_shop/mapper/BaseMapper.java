package ru.t_systems.samushenkov.tires_shop.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.dto.BaseDTO;
import ru.t_systems.samushenkov.tires_shop.model.BaseEntity;

public class BaseMapper implements MapperInterface {

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public BaseEntity convertToEntity(BaseDTO dto) {
        return modelMapper.map(dto, BaseEntity.class);
    }

    @Override
    public BaseDTO convertToDTO(BaseEntity entity) {
        return modelMapper.map(entity, BaseDTO.class);
    }

    public ModelMapper getModelMapper() {
        return modelMapper;
    }

    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }
}
