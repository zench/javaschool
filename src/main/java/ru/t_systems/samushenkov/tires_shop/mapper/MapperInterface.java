package ru.t_systems.samushenkov.tires_shop.mapper;

import ru.t_systems.samushenkov.tires_shop.dto.BaseDTO;
import ru.t_systems.samushenkov.tires_shop.model.BaseEntity;

public interface MapperInterface {
    BaseEntity convertToEntity(BaseDTO dto);
    BaseDTO convertToDTO(BaseEntity entity);
}
