package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.UserAddressDTO;
import ru.t_systems.samushenkov.tires_shop.model.UserAddress;

@Component
public class UserAddressMapper extends BaseMapper {
    public UserAddress convertToEntity(UserAddressDTO userAddressDTO) {
        return super.getModelMapper().map(userAddressDTO, UserAddress.class);
    }

    public UserAddressDTO convertToDTO(UserAddress userAddress) {
        return super.getModelMapper().map(userAddress, UserAddressDTO.class);
    }
}
