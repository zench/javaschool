package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.PropertyDTO;
import ru.t_systems.samushenkov.tires_shop.model.Property;

@Component
public class PropertyMapper extends BaseMapper {

    public Property convertToEntity(PropertyDTO propertyDTO) {
        return super.getModelMapper().map(propertyDTO, Property.class);
    }

    public PropertyDTO convertToDTO(Property property) {
        return super.getModelMapper().map(property, PropertyDTO.class);
    }
}
