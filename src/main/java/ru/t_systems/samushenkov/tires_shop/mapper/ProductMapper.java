package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.ProductDTO;
import ru.t_systems.samushenkov.tires_shop.model.Product;

@Component
public class ProductMapper  extends BaseMapper {

    public Product convertToEntity(ProductDTO productDTO) {
        return super.getModelMapper().map(productDTO, Product.class);
    }

    public ProductDTO convertToDTO(Product product) {
        return super.getModelMapper().map(product, ProductDTO.class);
    }
}
