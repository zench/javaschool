package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.dto.ShowCaseDTO;

@Component
public class ProductStatisticsShowCaseMapper {
    public ShowCaseDTO getShowCaseDTO(ProductStatisticsDTO psDTO) {
        return new ShowCaseDTO(psDTO.getModification());
    }
}
