package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.OrderDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;

@Component
public class OrderMapper extends BaseMapper {
    public Order convertToEntity(OrderDTO orderDTO) {
        return super.getModelMapper().map(orderDTO, Order.class);
    }

    public OrderDTO convertToDTO(Order order) {
        return super.getModelMapper().map(order, OrderDTO.class);
    }
}
