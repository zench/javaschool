package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.ModificationDTO;
import ru.t_systems.samushenkov.tires_shop.model.Modification;

@Component
public class ModificationMapper extends BaseMapper {
    public Modification convertToEntity(ModificationDTO modificationDTO) {
        return super.getModelMapper().map(modificationDTO, Modification.class);
    }

    public ModificationDTO convertToDTO(Modification modification) {
        return super.getModelMapper().map(modification, ModificationDTO.class);
    }
}
