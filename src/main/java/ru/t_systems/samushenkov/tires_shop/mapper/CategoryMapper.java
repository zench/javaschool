package ru.t_systems.samushenkov.tires_shop.mapper;

import org.springframework.stereotype.Component;
import ru.t_systems.samushenkov.tires_shop.dto.CategoryDTO;
import ru.t_systems.samushenkov.tires_shop.model.Category;

@Component
public class CategoryMapper extends BaseMapper {

    public Category convertToEntity(CategoryDTO categoryDTO) {
        return super.getModelMapper().map(categoryDTO, Category.class);
    }

    public CategoryDTO convertToDTO(Category category) {
        return super.getModelMapper().map(category, CategoryDTO.class);
    }
}
