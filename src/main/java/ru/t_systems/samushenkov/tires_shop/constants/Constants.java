package ru.t_systems.samushenkov.tires_shop.constants;

public final class Constants {
    //Hide the constructor
    private Constants(){}
    public static final int BRAND_PROPERTY_ID = 1;
    public static final int SEASON_PROPERTY_ID = 3;
    public static final int SPIKE_PROPERTY_ID = 4;
    public static final int WIDTH_PROPERTY_ID = 11;
    public static final int PROFILE_PROPERTY_ID = 12;
    public static final int DIAMETER_PROPERTY_ID = 13;
    public static final int TIRES_CATEGORY_ID = 1;
    public static final int PRODUCT_ENTITY_TYPE_ID = 1;
    public static final int MODIFICATION_ENTITY_TYPE_ID = 2;
    public static final int TOP_MODIFICATIONS_COUNT = 8;
    public static final String EARLIEST_DATE = "2000-01-01";
    public static final String LATEST_DATE = "2100-01-01";
    public static final String SHOWCASE_SERVER_URL = "http://localhost:8090";
    public static final String SHOWCASE_UPDATE_MESSAGE = "updateTop";
    public static final String STATISTICS_CLIENT_TYPE = "client";
    public static final String STATISTICS_INCOME_TYPE = "income";
    public static final String STATISTICS_PRODUCT_TYPE = "product";
    public static final String STATISTICS_MAIN_PAGE = "admin/statistics";
    public static final String STATISTICS_CLIENT_PAGE = "admin/clientStatistics";
    public static final String STATISTICS_INCOME_PAGE = "admin/incomeStatistics";
    public static final String STATISTICS_PRODUCT_PAGE = "admin/productStatistics";
    public static final String STOCK_IMAGE_STORAGE = "/static/images/stock/";
}
