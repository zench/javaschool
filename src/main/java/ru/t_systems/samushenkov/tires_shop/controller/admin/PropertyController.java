package ru.t_systems.samushenkov.tires_shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.PropertyDTO;
import ru.t_systems.samushenkov.tires_shop.model.Property;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.*;

@Controller
public class PropertyController {
    private static final String ALL_TITLE = "Администрирование: Свойства категории";
    private static final String EDIT_TITLE = "Администрирование: редактирование свойства";
    private static final String ADD_TITLE = "Администрирование: добавление свойства";
    private static final String TITLE = "title";
    private static final String CATEGORIES_REDIRECT = "redirect:/admin/categories/";
    private static final String PROPERTIES_PATH = "/properties";

    private PropertyService propertyService;
    private CategoryService categoryService;
    private PropertyTypeService propertyTypeService;
    private UnitService unitService;
    private PropertyEntityTypeService propertyEntityTypeService;

    @Autowired
    public void setPropertyService(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setPropertyTypeService(PropertyTypeService propertyTypeService) {
        this.propertyTypeService = propertyTypeService;
    }

    @Autowired
    public void setPropertyEntityTypeService(PropertyEntityTypeService propertyEntityTypeService) {
        this.propertyEntityTypeService = propertyEntityTypeService;
    }

    @Autowired
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    @GetMapping(value = "/admin/categories/{id}/properties")
    public ModelAndView allProperties(@PathVariable("id") int categoryId) {
        ModelAndView modelAndView = propertyService.allMVProperties(categoryId, 0);
        modelAndView.addObject(TITLE, ALL_TITLE);
        modelAndView.setViewName("properties");
        return modelAndView;
    }

    @GetMapping(value = "/admin/properties/edit/{id}")
    public ModelAndView editPage(@PathVariable("id") int id) {
        ModelAndView modelAndView = propertyService.getProperty(id);
        modelAndView.addObject(TITLE, EDIT_TITLE);
        modelAndView.setViewName("admin/editProperty");
        return modelAndView;
    }

    @PostMapping(value = "/admin/properties/edit")
    public ModelAndView editProperty(@ModelAttribute("property") PropertyDTO propertyDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT+propertyDTO.getCategoryId()+PROPERTIES_PATH);
        propertyService.edit(propertyDTO);
        return modelAndView;
    }

    @GetMapping(value = "/admin/categories/{id}/properties/add")
    public ModelAndView addPage(@PathVariable("id") int categoryId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("category", categoryService.getById(categoryId));
        modelAndView.addObject("types", propertyTypeService.allTypes());
        modelAndView.addObject("units", unitService.allUnits());
        modelAndView.addObject("entityTypes", propertyEntityTypeService.allEntityTypes());
        modelAndView.addObject(TITLE, ADD_TITLE);
        modelAndView.setViewName("admin/editProperty");
        return modelAndView;
    }

    @PostMapping(value = "/admin/properties/add")
    public ModelAndView addModification(@ModelAttribute("property") PropertyDTO propertyDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT+propertyDTO.getCategoryId()+PROPERTIES_PATH);
        propertyService.add(propertyDTO);
        return modelAndView;
    }
    @GetMapping(value="/admin/properties/delete/{id}")
    public ModelAndView deleteProperty(@PathVariable("id") int id) {
        Property property = propertyService.getById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT+property.getCategoryId()+PROPERTIES_PATH);
        propertyService.delete(id);
        return modelAndView;
    }
}
