package ru.t_systems.samushenkov.tires_shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.OrderDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.OrderService;

@Controller
public class OrderController {
    private static final String ALL_TITLE = "Администрирование: Заказы";

    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/admin/orders")
    public ModelAndView allOrders(@RequestParam(defaultValue = "1") int page) {
        String url = "/admin/orders";
        ModelAndView modelAndView = orderService.allOrders(page);
        modelAndView.addObject("title", ALL_TITLE);
        modelAndView.addObject("url", url);
        modelAndView.setViewName("admin/orders");
        return modelAndView;
    }

    @PostMapping(value = "/admin/order/edit")
    public ModelAndView editProduct(@RequestParam(defaultValue = "1") int page, @ModelAttribute("order") OrderDTO orderDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/admin/orders/?page=" + page);
        orderService.edit(orderDTO);
        return modelAndView;
    }
}
