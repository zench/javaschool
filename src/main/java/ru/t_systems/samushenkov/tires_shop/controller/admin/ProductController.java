package ru.t_systems.samushenkov.tires_shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ProductDTO;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CategoryService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductPropertyService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ProductController {
    private static final String ALL_TITLE = "Администрирование: Товары";
    private static final String EDIT_TITLE = "Администрирование: редактирование товара";
    private static final String ADD_TITLE = "Администрирование: добавление товара";
    private static final String TITLE = "title";
    private static final String CATEGORIES_REDIRECT = "redirect:/admin/categories/";
    private static final String PRODUCTS_PATH = "/products";
    private ProductService productService;
    private ProductPropertyService productPropertyService;
    private CategoryService categoryService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setProductPropertyService(ProductPropertyService productPropertyService) {
        this.productPropertyService = productPropertyService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/admin/categories/{id}/products")
    public ModelAndView allProducts(@PathVariable("id") int categoryId, @RequestParam(defaultValue = "1") int page) {
        String url = "/admin/categories/"+categoryId+PRODUCTS_PATH;
        ModelAndView modelAndView = productService.allProducts(page, categoryId);
        modelAndView.addObject(TITLE, ALL_TITLE);
        modelAndView.addObject("url", url);
        modelAndView.setViewName("admin/products");
        return modelAndView;
    }

    @GetMapping(value = "/admin/products/edit/{id}")
    public ModelAndView editPage(@PathVariable("id") int id) {
        ModelAndView modelAndView = productService.getProduct(id);
        modelAndView.addObject(TITLE, EDIT_TITLE);
        modelAndView.setViewName("admin/editProduct");
        return modelAndView;
    }

    @PostMapping(value = "/admin/products/edit")
    public ModelAndView editProduct(@ModelAttribute("product") ProductDTO productDTO, HttpServletRequest servletRequest) {
        ModelAndView modelAndView = new ModelAndView();
        productService.edit(productDTO, servletRequest.getServletContext().getRealPath("/static/images/stock"));
        modelAndView.setViewName(CATEGORIES_REDIRECT+productDTO.getCategory().getId()+PRODUCTS_PATH);
        return modelAndView;
    }

    @GetMapping(value = "/admin/categories/{id}/products/add")
    public ModelAndView addPage(@PathVariable("id") int categoryId) {
        ModelAndView modelAndView = new ModelAndView();
        Product product = new Product();
        product.setCategory(categoryService.getById(categoryId));
        product.setProductProperties(productPropertyService.getProductProperties(product));
        product.setCategory(categoryService.getById(categoryId));
        modelAndView.addObject("product", product);
        modelAndView.addObject("countriesList", productService.getProductCountries());
        modelAndView.addObject(TITLE, ADD_TITLE);
        modelAndView.setViewName("admin/editProduct");
        return modelAndView;
    }

    @PostMapping(value = "/admin/products/add")
    public ModelAndView addProduct(@ModelAttribute("product") ProductDTO productDTO, HttpServletRequest servletRequest) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT+productDTO.getCategory().getId()+PRODUCTS_PATH);
        productService.add(productDTO, servletRequest.getServletContext().getRealPath("/static/images/stock"));
        return modelAndView;
    }
    @GetMapping(value="/admin/products/delete/{id}")
    public ModelAndView deleteProduct(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT+productService.getById(id).getCategory().getId()+PRODUCTS_PATH);
        productService.delete(id);
        return modelAndView;
    }
}
