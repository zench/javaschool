package ru.t_systems.samushenkov.tires_shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ModificationDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.ModificationMapper;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.service.AMQServiceImpl;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationPropertyService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductService;

@Controller
public class ModificationController {
    private static final String ALL_TITLE = "Администрирование: Модификации";
    private static final String EDIT_TITLE = "Администрирование: редактирование модификации";
    private static final String ADD_TITLE = "Администрирование: добавление модификации";
    private static final String TITLE = "title";
    private static final String EDIT_MODIFICATION_PATH = "admin/editModification";
    private ModificationService modificationService;
    private ModificationPropertyService modificationPropertyService;
    private ProductService productService;
    private AMQServiceImpl amqService;
    private ModificationMapper modificationMapper;

    @Autowired
    public void setModificationService(ModificationService modificationService) {
        this.modificationService = modificationService;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setModificationPropertyService(ModificationPropertyService modificationPropertyService) {
        this.modificationPropertyService = modificationPropertyService;
    }

    @Autowired
    public void setAmqService(AMQServiceImpl amqService) {
        this.amqService = amqService;
    }

    @Autowired
    public void setModificationMapper(ModificationMapper modificationMapper) {
        this.modificationMapper = modificationMapper;
    }

    @GetMapping(value = "/admin/products/{id}/modifications")
    public ModelAndView allModifications(@PathVariable("id") int productId) {
        ModelAndView modelAndView = modificationService.productModifications(productId);
        modelAndView.addObject(TITLE, ALL_TITLE);
        modelAndView.setViewName("admin/modifications");
        return modelAndView;
    }

    @GetMapping(value = "/admin/modifications/edit/{id}")
    public ModelAndView editPage(@PathVariable("id") int id) {
        ModelAndView modelAndView = modificationService.getModification(id);
        modelAndView.addObject(TITLE, EDIT_TITLE);
        modelAndView.setViewName(EDIT_MODIFICATION_PATH);
        return modelAndView;
    }

    @PostMapping(value = "/admin/modifications/edit")
    public ModelAndView editModification(@ModelAttribute("modification") ModificationDTO modificationDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(getModificationsPath(modificationDTO));
        amqService.updateModification(modificationDTO);
        return modelAndView;
    }

    @GetMapping(value = "/admin/products/{id}/modifications/add")
    public ModelAndView addPage(@PathVariable("id") int productId) {
        ModelAndView modelAndView = new ModelAndView();
        Modification modification = new Modification();
        modification.setModificationProperties(modificationPropertyService.getModificationProperties(null, productService.getById(productId)));
        modelAndView.addObject("modification", modification);
        modelAndView.addObject("product", productService.getById(productId));
        modelAndView.addObject(TITLE, ADD_TITLE);
        modelAndView.setViewName(EDIT_MODIFICATION_PATH);
        return modelAndView;
    }

    @PostMapping(value = "/admin/modifications/add")
    public ModelAndView addModification(@ModelAttribute("modification") ModificationDTO modificationDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(getModificationsPath(modificationDTO));
        modificationService.add(modificationDTO);
        return modelAndView;
    }
    @GetMapping(value="/admin/modifications/delete/{id}")
    public ModelAndView deleteModification(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView();
        Modification modification = modificationService.getById(id);
        modelAndView.setViewName(getModificationsPath(modificationMapper.convertToDTO(modification)));
        modificationService.delete(id);
        return modelAndView;
    }

    private String getModificationsPath(ModificationDTO modificationDTO) {
        return "redirect:/admin/products/"+modificationDTO.getProduct().getId()+"/modifications";
    }
}
