package ru.t_systems.samushenkov.tires_shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.UserDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class RegistrationController extends AbstractCartItemsController {
    private static final String TITLE = "Регистрация";

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/registration")
    public ModelAndView showRegistrationForm(HttpSession session,
                                             HttpServletResponse response,
                                             @CookieValue(value = "cartId", required = false) Cookie cartId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject("title", TITLE);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping(value = "/registration")
    public ModelAndView registerUserAccount(
            @ModelAttribute("userDto") @Valid UserDTO userDto,
            BindingResult result) {
        userService.registerNewUserAccount(userDto, result);

        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("registration");
            modelAndView.addObject("errors", result.getAllErrors());
            modelAndView.addObject("user", userDto);
            return  modelAndView;
        }
        else {
            return new ModelAndView("successRegister", "user", userDto);
        }
    }
}
