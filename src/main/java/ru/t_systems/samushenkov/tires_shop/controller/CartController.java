package ru.t_systems.samushenkov.tires_shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.model.Cart;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartItemService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class CartController extends AbstractCartItemsController {
    private static final String TITLE = "Корзина";
    private static final String CART_REDIRECT = "redirect:/cart";

    private CartItemService cartItemService;

    @Autowired
    public void setCartItemService(CartItemService cartItemService) {
        this.cartItemService = cartItemService;
    }

    @GetMapping(value = "/cart")
    public ModelAndView cart(HttpSession session,
                             HttpServletResponse response,
                             @CookieValue(value = "cartId", required = false) Cookie cartId) {

        ModelAndView modelAndView = getCartItems(session, response, cartId, null);
        modelAndView.addObject("title", TITLE);
        modelAndView.setViewName("cart");
        return modelAndView;
    }

    @GetMapping(value = "/cart/{id}/{count}")
    public String addToCart(HttpServletResponse response,
                            HttpSession session,
                            @PathVariable("id") int id,
                            @PathVariable("count") int count,
                            @CookieValue(value = "cartId", required = false) Cookie cartId) {
        Object sessionUserId = session.getAttribute("userId");
        if(cartId == null) {
            cartId = getCartCookie(session);
            response.addCookie(cartId);
        }
        Integer userId = null;
        if(sessionUserId != null) {
            userId = Integer.parseInt(sessionUserId.toString());
        }
        cartItemService.add(cartId.getValue(), id, count, userId);
        return CART_REDIRECT;
    }

    @GetMapping(value="/cart/delete/{id}")
    public ModelAndView deleteCategory(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CART_REDIRECT);
        cartItemService.delete(id);
        return modelAndView;
    }

    @PostMapping(value="/cart/update")
    public ModelAndView updateCart(@ModelAttribute("cart") Cart cart) {
        ModelAndView modelAndView = new ModelAndView();
        cartService.updateCart(cart, null);
        modelAndView.setViewName(CART_REDIRECT);
        return modelAndView;
    }
}
