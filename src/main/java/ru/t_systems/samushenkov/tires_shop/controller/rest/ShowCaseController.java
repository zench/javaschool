package ru.t_systems.samushenkov.tires_shop.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dto.ShowCaseDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ShowCaseService;
import java.util.List;

@RestController
public class ShowCaseController {
    private ShowCaseService showCaseService;

    @Autowired
    public void setShowCaseService(ShowCaseService showCaseService) {
        this.showCaseService = showCaseService;
    }

    @CrossOrigin(origins = Constants.SHOWCASE_SERVER_URL)
    @GetMapping(value = "/rest/showcase")
    public List<ShowCaseDTO> topModifications() {
        return showCaseService.getTopSortElements();
    }
}
