package ru.t_systems.samushenkov.tires_shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.StatisticsService;

@Controller
public class StatisticsController {
    private static final String STATISTICS_TITLE = "Администрирование: Статистика по заказам";

    private StatisticsService statisticsService;

    @Autowired
    public void setStatisticsService(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping(value = "/admin/statistics")
    public ModelAndView adminStatistics(@RequestParam(defaultValue = "1") int page,
                                         @RequestParam(required = false) String dateFrom,
                                         @RequestParam(required = false) String dateTo,
                                         @RequestParam(required = false) String type) {
        ModelAndView modelAndView = statisticsService.getAllStatisticsItems(page, dateFrom, dateTo, type);
        modelAndView.addObject("title", STATISTICS_TITLE);
        return modelAndView;
    }
}
