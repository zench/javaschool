package ru.t_systems.samushenkov.tires_shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AbstractCartItemsController {
    protected CartService cartService;

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    protected ModelAndView getCartItems(HttpSession session,
                                        HttpServletResponse response,
                                        Cookie cartId,
                                        ModelAndView modelAndView) {
        if(cartId == null) {
            cartId = getCartCookie(session);
            response.addCookie(cartId);
        }
        session.setAttribute("cartSession", cartId.getValue());
        ModelAndView cartMV = cartService.getMVCartItems(session);
        if(modelAndView == null) {
            return cartMV;
        }
        modelAndView.addObject("total", cartMV.getModel().get("total"));
        modelAndView.addObject("totalCount", cartMV.getModel().get("totalCount"));
        modelAndView.addObject("cartItems", cartMV.getModel().get("cartItems"));
        return modelAndView;
    }

    protected Cookie getCartCookie(HttpSession session) {
        String cartSessionId = cartService.getCartSessionId(session);
        Cookie cookie = new Cookie("cartId", cartSessionId);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        cookie.setMaxAge(36000000);
        return cookie;
    }
}
