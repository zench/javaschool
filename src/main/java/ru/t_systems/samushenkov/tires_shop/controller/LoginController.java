package ru.t_systems.samushenkov.tires_shop.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SessionAttributes({"currentUser"})
@Controller
public class LoginController extends AbstractCartItemsController {
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);
    private static final String LOGIN_PAGE = "login";

    @GetMapping(value = "/login")
    public ModelAndView login(HttpSession session,
                        HttpServletResponse response,
                        @CookieValue(value = "cartId", required = false) Cookie cartId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.setViewName(LOGIN_PAGE);
        return modelAndView;
    }

    @PostMapping(value = "/login")
    public String doLogin() {
        return LOGIN_PAGE;
    }

    @GetMapping(value = "/loginFailed")
    public String loginError(Model model) {
        log.info("Login attempt failed");
        model.addAttribute("error", "true");
        return LOGIN_PAGE;
    }

    @GetMapping(value = "/logout")
    public String logout(SessionStatus session, HttpSession httpSession) {
        SecurityContextHolder.getContext().setAuthentication(null);
        httpSession.setAttribute("userId", null);
        session.setComplete();
        return "redirect:/";
    }
}
