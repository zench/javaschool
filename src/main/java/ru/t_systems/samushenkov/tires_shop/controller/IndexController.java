package ru.t_systems.samushenkov.tires_shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dto.TiresFilterDTO;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class IndexController extends AbstractCartItemsController {
    private ModificationService modificationService;

    @Autowired
    public void setModificationService(ModificationService modificationService) {
        this.modificationService = modificationService;
    }

    @GetMapping(value = "/")
    public ModelAndView topModifications(@RequestParam(defaultValue = "1") int page,
                                         @ModelAttribute("tiresFilterDTO") TiresFilterDTO tiresFilterDTO,
                                         HttpSession session,
                                         HttpServletResponse response,
                                         @CookieValue(value = "cartId", required = false) Cookie cartId) {
        ModelAndView modelAndView = modificationService.allModifications(page, Constants.TIRES_CATEGORY_ID, tiresFilterDTO);
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping(value = "/shop/{id}")
    public ModelAndView editPage(HttpServletRequest request,
                                 HttpSession session,
                                 HttpServletResponse response,
                                 @CookieValue(value = "cartId", required = false) Cookie cartId,
                                 @PathVariable("id") int id) throws NoHandlerFoundException {
        Modification modification = modificationService.getById(id);
        if(modification == null) {
            throw new NoHandlerFoundException(request.getMethod(), request.getRequestURI(), new HttpHeaders());
        } else {
            ModelAndView modelAndView = modificationService.getMVById(id);
            modelAndView.addObject("title", modification.getProduct().getName());
            modelAndView = getCartItems(session, response, cartId, modelAndView);
            modelAndView.setViewName("modificationDetail");
            return modelAndView;
        }
    }
}
