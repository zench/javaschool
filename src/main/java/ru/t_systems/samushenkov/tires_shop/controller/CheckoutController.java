package ru.t_systems.samushenkov.tires_shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.CheckoutInfoDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.service.AMQServiceImpl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class CheckoutController extends AbstractCartItemsController {
    private static final String TITLE = "Оформление заказа";

    private AMQServiceImpl amqService;

    @Autowired
    public void setAmqService(AMQServiceImpl amqService) {
        this.amqService = amqService;
    }

    @GetMapping(value = "/checkout")
    public ModelAndView checkout(HttpSession session,
                                 HttpServletResponse response,
                                 @CookieValue(value = "cartId", required = false) Cookie cartId) {
        session.setAttribute("cartSession", cartId.getValue());
        ModelAndView modelAndView = getCartItems(session, response, cartId, null);
        modelAndView.addObject("title", TITLE);
        modelAndView.setViewName("checkout");
        return modelAndView;
    }

    @PostMapping(value = "/checkout")
    public ModelAndView doCheckout(
            @Valid @ModelAttribute("order") CheckoutInfoDTO checkoutInfoDTO,
            BindingResult result,
            HttpSession session,
            HttpServletResponse response,
            @CookieValue(value = "cartId", required = false) Cookie cartId) {

        Order order = null;
        if (!result.hasErrors()) {
            order = amqService.finishCheckout(checkoutInfoDTO, session, result);
        }

        if(result.hasErrors() || (order == null)) {
            ModelAndView modelAndView = getCartItems(session, response, cartId, null);
            modelAndView.setViewName("checkout");
            modelAndView.addObject("title", TITLE);
            modelAndView.addObject("errors", result.getAllErrors());
            modelAndView.addObject("lastOrder", checkoutInfoDTO);
            return  modelAndView;
        } else {
            return new ModelAndView("successCheckout", "order", order);
        }
    }
}
