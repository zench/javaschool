package ru.t_systems.samushenkov.tires_shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class CompanyController extends AbstractCartItemsController {
    private static final String ABOUT = "О нас";
    private static final String DELIVERY = "Доставка";
    private static final String CONTACTS = "Контакты";
    private static final String ABOUT_PERSONAL_DATA = "О персональных данных";
    private static final String TITLE = "title";

    @GetMapping(value = "/about")
    public ModelAndView about(HttpSession session,
                              HttpServletResponse response,
                              @CookieValue(value = "cartId", required = false) Cookie cartId){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, ABOUT);
        modelAndView.setViewName("about");
        return modelAndView;
    }

    @GetMapping(value = "/delivery")
    public ModelAndView delivery(HttpSession session,
                                 HttpServletResponse response,
                                 @CookieValue(value = "cartId", required = false) Cookie cartId){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, DELIVERY);
        modelAndView.setViewName("delivery");
        return modelAndView;
    }

    @GetMapping(value = "/contacts")
    public ModelAndView contacts(HttpSession session,
                                 HttpServletResponse response,
                                 @CookieValue(value = "cartId", required = false) Cookie cartId){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, CONTACTS);
        modelAndView.setViewName("contacts");
        return modelAndView;
    }

    @GetMapping(value = "/personalData")
    public ModelAndView personalData(HttpSession session,
                                     HttpServletResponse response,
                                     @CookieValue(value = "cartId", required = false) Cookie cartId){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, ABOUT_PERSONAL_DATA);
        modelAndView.setViewName("personalData");
        return modelAndView;
    }
}
