package ru.t_systems.samushenkov.tires_shop.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.t_systems.samushenkov.tires_shop.controller.AbstractCartItemsController;
import ru.t_systems.samushenkov.tires_shop.dto.UserAddressDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserNoPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class UserController extends AbstractCartItemsController {
    private static final String ORDERS_TITLE = "История заказов";
    private static final String ACCOUNT_TITLE = "Профиль пользователя";
    private static final String PASSWORD_TITLE = "Сменить пароль";
    private static final String TITLE = "title";
    private static final String SUCCESS_ATTR = "success";
    private static final String HAS_ERRORS_ATTR = "hasErrors";

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/user/orders")
    public ModelAndView allOrders(HttpSession session,
                                  HttpServletResponse response,
                                  @CookieValue(value = "cartId", required = false) Cookie cartId) {
        ModelAndView modelAndView = userService.getUserOrders(session);
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, ORDERS_TITLE);
        modelAndView.setViewName("user/orders");
        return modelAndView;
    }

    @GetMapping(value = "/user/account")
    public ModelAndView account(HttpSession session,
                                HttpServletResponse response,
                                @CookieValue(value = "cartId", required = false) Cookie cartId) {
        ModelAndView modelAndView = userService.getUserInformation(session);
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, ACCOUNT_TITLE);
        modelAndView.setViewName("user/account");
        return modelAndView;
    }

    @GetMapping(value = "/user/password")
    public ModelAndView password(HttpSession session,
                                HttpServletResponse response,
                                @CookieValue(value = "cartId", required = false) Cookie cartId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView = getCartItems(session, response, cartId, modelAndView);
        modelAndView.addObject(TITLE, PASSWORD_TITLE);
        modelAndView.setViewName("user/password");
        return modelAndView;
    }

    @PostMapping(value = "/user/password/edit")
    public ModelAndView editAccount(@ModelAttribute("userPasswordDTO") UserPasswordDTO userPasswordDTO, HttpSession session, RedirectAttributes redir) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/user/password");
        if(userService.editPassword(userPasswordDTO, session)) {
            redir.addFlashAttribute(SUCCESS_ATTR, true);
        } else {
            redir.addFlashAttribute(HAS_ERRORS_ATTR, true);
        }
        return modelAndView;
    }

    @PostMapping(value = "/user/account/edit")
    public ModelAndView editAccount(@ModelAttribute("userNoPasswordDTO") @Valid UserNoPasswordDTO userNoPasswordDTO,
                                    BindingResult result,
                                    HttpSession session,
                                    RedirectAttributes redir,
                                    Errors errors) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/user/account");
        userService.editUser(userNoPasswordDTO, session, result);
        if(!result.hasErrors()) {
            redir.addFlashAttribute(SUCCESS_ATTR, true);
        } else {
            redir.addFlashAttribute("userTemp", userNoPasswordDTO);
            redir.addFlashAttribute("errors", result.getAllErrors());
        }
        return modelAndView;
    }

    @PostMapping(value = "/user/address/edit")
    public ModelAndView editAddress(@ModelAttribute("userAddress") @Valid UserAddressDTO userAddressDTO,
                                    BindingResult result,
                                    HttpSession session,
                                    RedirectAttributes redir) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/user/account");
        if(!result.hasErrors()) {
            if(userService.editUserAddress(userAddressDTO, session)) {
                redir.addFlashAttribute(SUCCESS_ATTR, true);
            } else {
                redir.addFlashAttribute(HAS_ERRORS_ATTR, true);
            }
        } else {
            redir.addFlashAttribute("tempUserAddress", userAddressDTO);
            redir.addFlashAttribute("errors", result.getAllErrors());
            redir.addFlashAttribute(HAS_ERRORS_ATTR, true);
        }
        return modelAndView;
    }
}
