package ru.t_systems.samushenkov.tires_shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.CategoryDTO;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CategoryService;

@Controller
public class CategoryController {
    private static final String ALL_TITLE = "Администрирование: Категории товаров";
    private static final String EDIT_TITLE = "Администрирование: редактирование категории товаров";
    private static final String ADD_TITLE = "Администрирование: добавление категории товаров";
    private static final String TITLE = "title";
    private static final String CATEGORIES_REDIRECT = "redirect:/admin/categories";
    private static final String EDIT_CATEGORY_PAGE = "admin/editCategory";
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/admin/categories")
    public ModelAndView allCategories() {
        ModelAndView modelAndView = categoryService.allCategories();
        modelAndView.addObject(TITLE, ALL_TITLE);
        modelAndView.setViewName("admin/categories");
        return modelAndView;
    }

    @GetMapping(value = "/admin/categories/edit/{id}")
    public ModelAndView editPage(@PathVariable("id") int id) {
        Category category = categoryService.getById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(TITLE, EDIT_TITLE);
        modelAndView.setViewName(EDIT_CATEGORY_PAGE);
        modelAndView.addObject("category", category);
        return modelAndView;
    }

    @PostMapping(value = "/admin/categories/edit")
    public ModelAndView editCategory(@ModelAttribute("category") CategoryDTO categoryDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT);
        categoryService.edit(categoryDTO);
        return modelAndView;
    }

    @GetMapping(value = "/admin/categories/add")
    public ModelAndView addPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(TITLE, ADD_TITLE);
        modelAndView.setViewName(EDIT_CATEGORY_PAGE);
        return modelAndView;
    }

    @PostMapping(value = "/admin/categories/add")
    public ModelAndView addCategory(@ModelAttribute("category") CategoryDTO categoryDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT);
        categoryService.add(categoryDTO);
        return modelAndView;
    }

    @GetMapping(value="/admin/categories/delete/{id}")
    public ModelAndView deleteCategory(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(CATEGORIES_REDIRECT);
        categoryService.delete(id);
        return modelAndView;
    }
}
