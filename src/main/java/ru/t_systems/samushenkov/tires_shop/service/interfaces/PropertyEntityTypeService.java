package ru.t_systems.samushenkov.tires_shop.service.interfaces;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;

import java.util.List;

public interface PropertyEntityTypeService {
    List<PropertyEntityType> allEntityTypes();
    PropertyEntityType getById(int id);
}
