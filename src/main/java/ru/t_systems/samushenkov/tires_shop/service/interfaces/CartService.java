package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.model.Cart;
import ru.t_systems.samushenkov.tires_shop.model.CartItem;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface CartService {
    void updateCart(Cart cart, Integer userId);
    List<CartItem> getCartItems(HttpSession session);
    void setCartUser(HttpSession session);
    ModelAndView getMVCartItems(HttpSession session);
    String getCartSessionId(HttpSession session);
}
