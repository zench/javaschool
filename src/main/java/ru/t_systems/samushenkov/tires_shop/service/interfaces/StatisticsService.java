package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;

import java.time.LocalDateTime;
import java.util.List;

public interface StatisticsService {
    ModelAndView getAllStatisticsItems(int page, String dateFrom, String dateTo, String type);
    LocalDateTime getDateFrom(String dateFrom);
    LocalDateTime getDateTo(String dateTo);
    List<ProductStatisticsDTO> fillProductsWithModification(List<ProductStatisticsDTO> productsList);
}
