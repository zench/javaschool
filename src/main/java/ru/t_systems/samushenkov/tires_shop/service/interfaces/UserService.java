package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.UserAddressDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserNoPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.model.User;

import javax.servlet.http.HttpSession;

public interface UserService {
    User getById(int id);
    User registerNewUserAccount(UserDTO userDto, BindingResult result);
    ModelAndView getUserOrders(HttpSession session);
    ModelAndView getUserInformation(HttpSession session);
    UserNoPasswordDTO editUser(UserNoPasswordDTO userNoPasswordDTO, HttpSession session, BindingResult result);
    boolean editUserAddress(UserAddressDTO userAddressDTO, HttpSession session);
    int getUserIdFromSession(HttpSession session);
    void deleteUser(User user);
    boolean editPassword(UserPasswordDTO userPasswordDTO, HttpSession session);
}
