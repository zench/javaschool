package ru.t_systems.samushenkov.tires_shop.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CategoryDAO;
import ru.t_systems.samushenkov.tires_shop.dto.CategoryDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.CategoryMapper;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CategoryService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryDAO categoryDAO;
    private CategoryMapper categoryMapper;
    private static final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    public void setCategoryDao(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Autowired
    public void setCategoryMapper(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public ModelAndView allCategories() {
        List<Category> categories =  categoryDAO.allCategories();
        List<CategoryDTO> categoriesDTO = categories.stream()
                .map(category ->categoryMapper.convertToDTO(category))
                .collect(Collectors.toList());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("categoriesList", categoriesDTO);
        return modelAndView;
    }

    @Transactional
    @Override
    public CategoryDTO add(CategoryDTO categoryDTO) {
        Category category = categoryDAO.add(categoryMapper.convertToEntity(categoryDTO));
        return categoryMapper.convertToDTO(category);
    }

    @Transactional
    @Override
    public void delete(int categoryId) {
        categoryDAO.delete(getById(categoryId));
    }

    @Transactional
    @Override
    public void edit(CategoryDTO categoryDTO) {
        categoryDAO.edit(categoryMapper.convertToEntity(categoryDTO));
        log.info("Edited category: {} ", categoryDTO.getId());
    }

    @Transactional
    @Override
    public Category getById(int id) {
        return categoryDAO.getById(id);
    }
}
