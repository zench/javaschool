package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductPropertyDAO;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;
import ru.t_systems.samushenkov.tires_shop.model.Property;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductPropertyService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductPropertyServiceImpl implements ProductPropertyService {
    private ProductPropertyDAO productPropertyDAO;
    private PropertyService propertyService;

    @Autowired
    public void setPropertyService(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Autowired
    public void setProductPropertyDAO(ProductPropertyDAO productPropertyDAO) {
        this.productPropertyDAO = productPropertyDAO;
    }

    @Override
    @Transactional
    public List<ProductProperty> getProductProperties(Product product) {
        List<Property> dbProperties = propertyService.allProperties(product.getCategory().getId(), Constants.PRODUCT_ENTITY_TYPE_ID);
        List<ProductProperty> productProperties;
        if(product.getId() != 0) {
            productProperties = product.getProductProperties();
            for (ProductProperty pp : productProperties) {
                if (dbProperties.contains(pp.getProperty())) {
                    int i = dbProperties.indexOf(pp.getProperty());
                    dbProperties.remove(i);
                }
            }
        } else {
            productProperties = new ArrayList<>();
        }
        for(Property dbp : dbProperties) {
            ProductProperty newMProductProperty = new ProductProperty();
            newMProductProperty.setProduct(product);
            newMProductProperty.setProperty(dbp);
            productProperties.add(newMProductProperty);
        }
        return productProperties;
    }

    @Override
    @Transactional
    public ProductProperty add(ProductProperty productProperty) {
        LocalDateTime dateTime = LocalDateTime.now();
        productProperty.setCreated(dateTime);
        productProperty.setModified(dateTime);
        return productPropertyDAO.add(productProperty);
    }

    @Override
    @Transactional
    public void delete(ProductProperty productProperty) {
        productPropertyDAO.delete(productProperty);
    }

    @Override
    @Transactional
    public void edit(ProductProperty productProperty) {
        LocalDateTime dateTime = LocalDateTime.now();
        productProperty.setModified(dateTime);
        productPropertyDAO.edit(productProperty);
    }

    @Override
    @Transactional
    public ProductProperty getById(int id) {
        return productPropertyDAO.getById(id);
    }
}
