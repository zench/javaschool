package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.OrderDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;

public interface OrderService {
    ModelAndView allOrders(int page);
    void edit(OrderDTO orderDTO);
    Order getById(int id);
}
