package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dao.DeliveryTypeDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.OrderStatusDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.PaymentStatusDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.PaymentTypeDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderStatisticsDAO;
import ru.t_systems.samushenkov.tires_shop.dto.OrderDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.OrderMapper;
import ru.t_systems.samushenkov.tires_shop.model.*;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.OrderService;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderDAO orderDAO;
    private OrderStatusDAOImpl orderStatusDAO;
    private PaymentTypeDAOImpl paymentTypeDAO;
    private DeliveryTypeDAOImpl deliveryTypeDAO;
    private PaymentStatusDAOImpl paymentStatusDAO;
    private OrderStatisticsDAO orderStatisticsDAO;
    private OrderMapper orderMapper;

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setOrderStatusDAO(OrderStatusDAOImpl orderStatusDAO) {
        this.orderStatusDAO = orderStatusDAO;
    }

    @Autowired
    public void setPaymentTypeDAO(PaymentTypeDAOImpl paymentTypeDAO) {
        this.paymentTypeDAO = paymentTypeDAO;
    }

    @Autowired
    public void setDeliveryTypeDAO(DeliveryTypeDAOImpl deliveryTypeDAO) {
        this.deliveryTypeDAO = deliveryTypeDAO;
    }

    @Autowired
    public void setPaymentStatusDAO(PaymentStatusDAOImpl paymentStatusDAO) {
        this.paymentStatusDAO = paymentStatusDAO;
    }

    @Autowired
    public void setOrderStatisticsDAO(OrderStatisticsDAO orderStatisticsDAO) {
        this.orderStatisticsDAO = orderStatisticsDAO;
    }

    @Autowired
    public void setOrderMapper(OrderMapper orderMapper) {
        this.orderMapper = orderMapper;
    }

    @Override
    @Transactional
    public ModelAndView allOrders(int page) {
        List<Order> orders =  orderDAO.getAllOrders(page);
        for (Order order: orders) {
            order.setOrderStatistics(orderStatisticsDAO.getAllOrderItems(order));
        }
        int ordersCount = ordersCount();
        int pagesCount = (ordersCount + 9)/10;
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("ordersList", orders);
        modelAndView.addObject("page", page);
        modelAndView.addObject("itemsCount", ordersCount);
        modelAndView.addObject("pagesCount", pagesCount);
        modelAndView.addObject("statusesList", getOrderStatuses());
        modelAndView.addObject("deliveryTypesList", getDeliveryTypes());
        modelAndView.addObject("paymentTypesList", getPaymentTypes());
        modelAndView.addObject("paymentStatusesList", getPaymentStatuses());
        return modelAndView;
    }

    @Override
    @Transactional
    public void edit(OrderDTO orderDTO) {
        orderDTO.setStatus(orderStatusDAO.getById(orderDTO.getStatus().getId()));
        orderDTO.setDeliveryType(deliveryTypeDAO.getById(orderDTO.getDeliveryType().getId()));
        orderDTO.setPaymentType(paymentTypeDAO.getById(orderDTO.getPaymentType().getId()));
        orderDTO.setPaymentStatus(paymentStatusDAO.getById(orderDTO.getPaymentStatus().getId()));
        orderDAO.edit(orderMapper.convertToEntity(orderDTO));
    }

    @Override
    @Transactional
    public Order getById(int id) {
        return orderDAO.getById(id);
    }


    private List<OrderStatus> getOrderStatuses() {
        return orderStatusDAO.allOrderStatuses();
    }

    private List<PaymentType> getPaymentTypes() {
        return paymentTypeDAO.allPaymentTypes();
    }

    private List<DeliveryType> getDeliveryTypes() {
        return deliveryTypeDAO.allDeliveryTypes();
    }

    private List<PaymentStatus> getPaymentStatuses() {
        return paymentStatusDAO.allPaymentStatuses();
    }

    private int ordersCount() {
        return orderDAO.ordersCount();
    }
}
