package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CountryDAO;
import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CountryService;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private CountryDAO countryDAO;

    @Autowired
    public void setCountryDAO(CountryDAO countryDAO) {
        this.countryDAO = countryDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Country> allCountries() {
        return countryDAO.allCountries();
    }

    @Transactional
    public Country getById(int id) {
        return countryDAO.getById(id);
    }
}
