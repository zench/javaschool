package ru.t_systems.samushenkov.tires_shop.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dto.ModificationDTO;
import ru.t_systems.samushenkov.tires_shop.dto.CheckoutInfoDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CheckoutService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationService;

import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;

@Service
public class AMQServiceImpl {
    private static final Logger log = LoggerFactory.getLogger(AMQServiceImpl.class);
    private SenderService senderService;
    private ModificationService modificationService;
    private CheckoutService checkoutService;

    @Autowired
    public void setSenderService(SenderService senderService) {
        this.senderService = senderService;
    }

    @Autowired
    public void setModificationService(ModificationService modificationService) {
        this.modificationService = modificationService;
    }

    @Autowired
    public void setCheckoutService(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    public void updateModification(ModificationDTO modificationDTO) {
        modificationService.edit(modificationDTO);
        try {
            senderService.send(Constants.SHOWCASE_UPDATE_MESSAGE);
        } catch (NamingException | JMSException ignored) {
            log.warn("Message {} was not sent, modification - {}", Constants.SHOWCASE_UPDATE_MESSAGE, modificationDTO.getId());
        }
    }

    public Order finishCheckout(CheckoutInfoDTO checkoutInfoDTO, HttpSession session, BindingResult result) {
        Order order = checkoutService.finishCheckout(checkoutInfoDTO, session, result);
        try {
            senderService.send(Constants.SHOWCASE_UPDATE_MESSAGE);
        } catch (NamingException | JMSException ignored) {
            log.warn("Message {} was not sent, order - {}",  Constants.SHOWCASE_UPDATE_MESSAGE, order.getId());
        }
        return order;
    }
}
