package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;

import java.util.List;

public interface ProductPropertyService {
    List<ProductProperty> getProductProperties(Product product);
    ProductProperty add(ProductProperty productProperty);
    void delete(ProductProperty productProperty);
    void edit(ProductProperty productProperty);
    ProductProperty getById(int id);
}
