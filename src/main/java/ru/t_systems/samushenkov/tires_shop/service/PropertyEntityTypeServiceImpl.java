package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.PropertyEntityTypeDAO;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyEntityTypeService;

import java.util.List;

@Service
public class PropertyEntityTypeServiceImpl implements PropertyEntityTypeService {
    private PropertyEntityTypeDAO propertyEntityTypeDAO;

    @Autowired
    public void setPropertyEntityTypeDAO(PropertyEntityTypeDAO propertyEntityTypeDAO) {
        this.propertyEntityTypeDAO = propertyEntityTypeDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PropertyEntityType> allEntityTypes() {
        return propertyEntityTypeDAO.allEntityTypes();
    }

    @Transactional
    public PropertyEntityType getById(int id) {
        return propertyEntityTypeDAO.getById(id);
    }
}
