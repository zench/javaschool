package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.PropertyTypeDAO;
import ru.t_systems.samushenkov.tires_shop.model.PropertyType;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyTypeService;

import java.util.List;

@Service
public class PropertyTypeServiceImpl implements PropertyTypeService {
    private PropertyTypeDAO propertyTypeDAO;

    @Autowired
    public void setPropertyTypeDAO(PropertyTypeDAO propertyTypeDAO) {
        this.propertyTypeDAO = propertyTypeDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PropertyType> allTypes() {
        return propertyTypeDAO.allTypes();
    }

    @Transactional
    public PropertyType getById(int id) {
        return propertyTypeDAO.getById(id);
    }
}
