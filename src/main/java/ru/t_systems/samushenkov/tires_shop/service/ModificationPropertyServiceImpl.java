package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ModificationPropertyDAO;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.model.Property;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationPropertyService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ModificationPropertyServiceImpl implements ModificationPropertyService {

    private ModificationPropertyDAO modificationPropertyDAO;
    private PropertyService propertyService;

    @Autowired
    public void setPropertyService(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Autowired
    public void setModificationPropertyDAO(ModificationPropertyDAO modificationPropertyDAO) {
        this.modificationPropertyDAO = modificationPropertyDAO;
    }

    @Override
    @Transactional
    public List<ModificationProperty> getModificationProperties(Modification modification, Product product) {
        List<Property> dbProperties = propertyService.allProperties(product.getCategory().getId(), Constants.MODIFICATION_ENTITY_TYPE_ID);
        List<ModificationProperty> modificationProperties;
        if(modification != null) {
            modificationProperties = modification.getModificationProperties();
            for (ModificationProperty mp : modificationProperties) {
                if (dbProperties.contains(mp.getProperty())) {
                    int i = dbProperties.indexOf(mp.getProperty());
                    dbProperties.remove(i);
                }
            }
        } else {
            modificationProperties = new ArrayList<>();
        }
        for(Property dbp : dbProperties) {
            ModificationProperty newModificationProperty = new ModificationProperty();
            newModificationProperty.setModification(modification);
            newModificationProperty.setProperty(dbp);
            modificationProperties.add(newModificationProperty);
        }
        return modificationProperties;
    }

    @Override
    @Transactional
    public ModificationProperty add(ModificationProperty modificationProperty) {
        LocalDateTime dateTime = LocalDateTime.now();
        modificationProperty.setCreated(dateTime);
        modificationProperty.setModified(dateTime);
        return modificationPropertyDAO.add(modificationProperty);
    }

    @Override
    @Transactional
    public void delete(ModificationProperty modificationProperty) {
        modificationPropertyDAO.delete(modificationProperty);
    }

    @Override
    @Transactional
    public void edit(ModificationProperty modificationProperty) {
        LocalDateTime dateTime = LocalDateTime.now();
        modificationProperty.setModified(dateTime);
        modificationPropertyDAO.edit(modificationProperty);
    }

    @Override
    @Transactional
    public ModificationProperty getById(int id) {
        return modificationPropertyDAO.getById(id);
    }
}
