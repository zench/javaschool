package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ProductDTO;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.model.Product;

import java.util.List;

public interface ProductService {
    ModelAndView allProducts(int page, int categoryId);
    ProductDTO add(ProductDTO productDTO, String pathToImage);
    void delete(int productId);
    void edit(ProductDTO productDTO, String pathToImage);
    ModelAndView getProduct(int id);
    Product getById(int id);
    int productsCount(int categoryId);
    List<Category> getProductCategories();
    List<Country> getProductCountries();
}
