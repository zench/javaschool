package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.PropertyType;

import java.util.List;

public interface PropertyTypeService {
    List<PropertyType> allTypes();
    PropertyType getById(int id);
}
