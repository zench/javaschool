package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.validation.BindingResult;
import ru.t_systems.samushenkov.tires_shop.dto.CheckoutInfoDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;

import javax.servlet.http.HttpSession;

public interface CheckoutService {
    Order finishCheckout(CheckoutInfoDTO checkoutInfoDTO, HttpSession session, BindingResult result);
}
