package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.PropertyDTO;
import ru.t_systems.samushenkov.tires_shop.model.Property;

import java.util.List;

public interface PropertyService {
    List<Property> allProperties(int categoryId, int entityTypeId);
    ModelAndView allMVProperties(int categoryId, int entityTypeId);
    PropertyDTO add(PropertyDTO propertyDTO);
    void delete(int propertyId);
    void edit(PropertyDTO propertyDTO);
    ModelAndView getProperty(int id);
    Property getById(int id);
}
