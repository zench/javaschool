package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.CategoryDTO;
import ru.t_systems.samushenkov.tires_shop.model.Category;

public interface CategoryService {
    ModelAndView allCategories();
    CategoryDTO add(CategoryDTO categoryDTO);
    void delete(int categoryId);
    void edit(CategoryDTO categoryDTO);
    Category getById(int id);
}
