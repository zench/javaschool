package ru.t_systems.samushenkov.tires_shop.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import ru.t_systems.samushenkov.tires_shop.dao.DeliveryTypeDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.OrderStatusDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.PaymentStatusDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.PaymentTypeDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.*;
import ru.t_systems.samushenkov.tires_shop.dto.CheckoutInfoDTO;
import ru.t_systems.samushenkov.tires_shop.dto.SavedCartItemDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserAddressDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.UserAddressMapper;
import ru.t_systems.samushenkov.tires_shop.model.*;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CheckoutService;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CheckoutServiceImpl implements CheckoutService {
    private static final Logger log = LoggerFactory.getLogger(CheckoutServiceImpl.class);
    private Order order;
    private List<OrderStatistics> orderStatisticsList = new ArrayList<>();
    private BindingResult bindingResult;

    private UserAddressDAO userAddressDAO;
    private UserDAO userDAO;
    private OrderDAO orderDAO;
    private ModificationDAO modificationDAO;
    private CartItemDAO cartItemDAO;
    private OrderStatisticsDAO orderStatisticsDAO;
    private OrderStatusDAOImpl orderStatusDAO;
    private PaymentStatusDAOImpl paymentStatusDAO;
    private DeliveryTypeDAOImpl deliveryTypeDAO;
    private PaymentTypeDAOImpl paymentTypeDAO;
    private UserAddressMapper userAddressMapper;

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setUserAddressDAO(UserAddressDAO userAddressDAO) {
        this.userAddressDAO = userAddressDAO;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setModificationDAO(ModificationDAO modificationDAO) {
        this.modificationDAO = modificationDAO;
    }

    @Autowired
    public void setCartItemDAO(CartItemDAO cartItemDAO) {
        this.cartItemDAO = cartItemDAO;
    }

    @Autowired
    public void setOrderStatisticsDAO(OrderStatisticsDAO orderStatisticsDAO) {
        this.orderStatisticsDAO = orderStatisticsDAO;
    }

    @Autowired
    public void setOrderStatusDAO(OrderStatusDAOImpl orderStatusDAO) {
        this.orderStatusDAO = orderStatusDAO;
    }

    @Autowired
    public void setPaymentStatusDAO(PaymentStatusDAOImpl paymentStatusDAO) {
        this.paymentStatusDAO = paymentStatusDAO;
    }

    @Autowired
    public void setDeliveryTypeDAO(DeliveryTypeDAOImpl deliveryTypeDAO) {
        this.deliveryTypeDAO = deliveryTypeDAO;
    }

    @Autowired
    public void setPaymentTypeDAO(PaymentTypeDAOImpl paymentTypeDAO) {
        this.paymentTypeDAO = paymentTypeDAO;
    }

    @Autowired
    public void setUserAddressMapper(UserAddressMapper userAddressMapper) {
        this.userAddressMapper = userAddressMapper;
    }

    @Transactional
    @Override
    public Order finishCheckout(CheckoutInfoDTO checkoutInfoDTO, HttpSession session, BindingResult result) {
        cleanCheckoutService();
        this.bindingResult = result;
        User user = userDAO.getById(Integer.parseInt(session.getAttribute("userId").toString()));
        if(user == null) {
            this.bindingResult.reject("isNull.checkoutService.user", "Пользователь не обнаружен");
            log.error("Failed with getting user");
            return null;
        }
        UserAddress address  = getUserAddress(checkoutInfoDTO, user);
        UserAddress savedAddress = saveAddress(address);
        if(savedAddress == null) {
            this.bindingResult.reject("isNull.checkoutService.userAddress", "Ошибка сохранения адреса");
            log.error("Failed with saving user address");
            return null;
        }

        /**convert orderDTO to Order
         *set userAddress to the Order
         */
        getOrder(checkoutInfoDTO, user).setUserAddress(savedAddress);

        /**update modifications stock balance
         *update order total
         *remove order items from cart
         *save order
         */
        if(processOrder(user)){
            this.order = saveOrder(this.order);
            saveStatistics();
            return this.order;
        } else {
            return null;
        }
    }

    /**
     * set Order total, change modifications stockBalance, delete cartItems
     * @param user
     * @return boolean
     */
    private boolean processOrder(User user) {
        List<CartItem> cartItems = user.getCartItems();
        ObjectMapper objectMapper = new ObjectMapper();

        BigDecimal total = new BigDecimal(0);
        List<SavedCartItemDTO> savedCartItems = new ArrayList<>();
        for (CartItem cartItem: cartItems) {
            Modification mod = cartItem.getModification();
            BigDecimal count = new BigDecimal(cartItem.getCount());
            total = total.add(count.multiply(cartItem.getModification().getPrice()));
            if(mod.getStockBalance() >= cartItem.getCount()) {
                mod.setStockBalance(mod.getStockBalance()-cartItem.getCount());
                modificationDAO.edit(mod);
                savedCartItems.add(new SavedCartItemDTO(cartItem));
                this.orderStatisticsList.add(new OrderStatistics(cartItem));
                cartItemDAO.delete(cartItem);
            } else {
                this.bindingResult.reject("Less.checkoutService.stockBalance",
                        "Недостаточно товара на складе: "+mod.getProduct().getName()+" вы можете купить не более "+mod.getStockBalance()+" шт.");
                return false;
            }
        }
        String orderContent = "";
        try {
            orderContent = objectMapper.writeValueAsString(savedCartItems);
        } catch (JsonProcessingException e) {
            log.error("processOrder is failed with content json encode");
            log.error(e.getMessage());
        }
        this.order.setContent(orderContent);
        this.order.setTotal(total);
        return true;
    }

    private UserAddress saveAddress(UserAddress address) {
        List<UserAddress> userAddresses = userAddressDAO.getAllUserAddresses(address.getUser());
        for (UserAddress savedAddress: userAddresses) {
            if(savedAddress.equals(address)) {
                return savedAddress;
            }
        }
        return userAddressDAO.add(address);
    }

    private Order saveOrder(Order order) {
        return orderDAO.add(order);
    }

    private UserAddress getUserAddress(CheckoutInfoDTO checkoutInfoDTO, User user) {
        UserAddressDTO userAddressDTO = checkoutInfoDTO.getUserAddress();
        userAddressDTO.setUser(user);
        return userAddressMapper.convertToEntity(userAddressDTO);
    }

    private Order getOrder(CheckoutInfoDTO checkoutInfoDTO, User user) {
        Order newOrder = new Order(checkoutInfoDTO);
        newOrder.setUser(user);
        newOrder.setStatus(orderStatusDAO.getById(OrderStatus.CREATED));
        newOrder.setPaymentType(paymentTypeDAO.getById(checkoutInfoDTO.getPaymentType().getId()));
        newOrder.setPaymentStatus(paymentStatusDAO.getById(PaymentStatus.NOT_PAYED));
        newOrder.setDeliveryType(deliveryTypeDAO.getById(checkoutInfoDTO.getDeliveryType().getId()));

        this.order = newOrder;
        return this.order;
    }

    private void saveStatistics() {
        for (OrderStatistics orderStat: this.orderStatisticsList) {
            orderStat.setOrder(this.order);
            orderStatisticsDAO.add(orderStat);
        }
    }

    private void cleanCheckoutService() {
        this.orderStatisticsList = new ArrayList<>();
        this.order = new Order();
        this.bindingResult = null;
    }

}
