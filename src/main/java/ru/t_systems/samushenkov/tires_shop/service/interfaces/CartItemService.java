package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.CartItem;

import java.util.List;

public interface CartItemService {
    List<CartItem> getCartItemsBySessionId(String sessionId);
    CartItem add(String sessionId, int modificationId, int count, Integer userId);
    void delete(int cartItemId);
    void edit(CartItem cartItem);
    CartItem getById(int id);
}
