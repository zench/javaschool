package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.*;
import ru.t_systems.samushenkov.tires_shop.dto.PropertyDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.PropertyMapper;
import ru.t_systems.samushenkov.tires_shop.model.Property;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyService;

import java.util.List;

@Service
public class PropertyServiceImpl implements PropertyService {
    private PropertyDAO propertyDAO;
    private CategoryDAO categoryDAO;
    private UnitDAO unitDAO;
    private PropertyTypeDAO propertyTypeDAO;
    private PropertyEntityTypeDAO propertyEntityTypeDAO;
    private PropertyMapper propertyMapper;

    @Autowired
    public void setPropertyDAO(PropertyDAO propertyDAO) {
        this.propertyDAO = propertyDAO;
    }

    @Autowired
    public void setCategoryDAO(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Autowired
    public void setUnitDAO(UnitDAO unitDAO) {
        this.unitDAO = unitDAO;
    }

    @Autowired
    public void setPropertyTypeDAO(PropertyTypeDAO propertyTypeDAO) {
        this.propertyTypeDAO = propertyTypeDAO;
    }

    @Autowired
    public void setPropertyEntityTypeDAO(PropertyEntityTypeDAO propertyEntityTypeDAO) {
        this.propertyEntityTypeDAO = propertyEntityTypeDAO;
    }

    @Autowired
    public void setPropertyMapper(PropertyMapper propertyMapper) {
        this.propertyMapper = propertyMapper;
    }


    @Override
    @Transactional(readOnly = true)
    public List<Property> allProperties(int categoryId, int entityTypeId) {
        return  propertyDAO.allProperties(categoryId, entityTypeId);
    }

    @Override
    @Transactional(readOnly = true)
    public ModelAndView allMVProperties(int categoryId, int entityTypeId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("propertiesList", allProperties(categoryId, entityTypeId));
        modelAndView.addObject("category", categoryDAO.getById(categoryId));
        return modelAndView;
    }

    @Override
    @Transactional
    public PropertyDTO add(PropertyDTO propertyDTO) {
        propertyDTO.setType(propertyTypeDAO.getById(propertyDTO.getType().getId()));
        if(propertyDTO.getUnit() != null) {
            propertyDTO.setUnit(unitDAO.getById(propertyDTO.getUnit().getId()));
        }
        propertyDTO.setEntityType(propertyEntityTypeDAO.getById(propertyDTO.getEntityType().getId()));
        return propertyMapper.convertToDTO(propertyDAO.add(propertyMapper.convertToEntity(propertyDTO)));
    }

    @Override
    @Transactional
    public void delete(int propertyId) {
        propertyDAO.delete(getById(propertyId));
    }

    @Override
    @Transactional
    public void edit(PropertyDTO propertyDTO) {
        propertyDAO.edit(propertyMapper.convertToEntity(propertyDTO));
    }

    @Override
    @Transactional
    public ModelAndView getProperty(int id) {
        ModelAndView modelAndView = new ModelAndView();
        Property property = propertyDAO.getById(id);
        modelAndView.addObject("property", property);
        modelAndView.addObject("category", categoryDAO.getById(property.getCategoryId()));
        modelAndView.addObject("units", unitDAO.allUnits());
        modelAndView.addObject("types", propertyTypeDAO.allTypes());
        modelAndView.addObject("entityTypes", propertyEntityTypeDAO.allEntityTypes());
        return modelAndView;
    }

    @Override
    @Transactional
    public Property getById(int id) {
        return propertyDAO.getById(id);
    }
}
