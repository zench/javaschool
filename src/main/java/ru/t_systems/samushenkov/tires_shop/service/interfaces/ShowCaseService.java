package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.dto.ShowCaseDTO;

import java.util.List;

public interface ShowCaseService {
    List<ShowCaseDTO> getTopSortElements();
    public List<ShowCaseDTO> getTopSoldElements();
}
