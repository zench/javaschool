package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Unit;

import java.util.List;

public interface UnitService {
    List<Unit> allUnits();
    Unit getById(int id);
}
