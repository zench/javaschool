package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ModificationDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderStatisticsDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductPropertyDAO;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.dto.ShowCaseDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.ProductStatisticsShowCaseMapper;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ShowCaseService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.StatisticsService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShowCaseServiceImpl implements ShowCaseService {
    private ModificationDAO modificationDAO;
    private ProductPropertyDAO productPropertyDAO;
    private OrderStatisticsDAO orderStatisticsDAO;
    private StatisticsService statisticsService;
    private ProductStatisticsShowCaseMapper showCaseMapper;

    @Autowired
    public void setModificationDAO(ModificationDAO modificationDAO) {
        this.modificationDAO = modificationDAO;
    }

    @Autowired
    public void setProductPropertyDAO(ProductPropertyDAO productPropertyDAO) {
        this.productPropertyDAO = productPropertyDAO;
    }

    @Autowired
    public void setOrderStatisticsDAO(OrderStatisticsDAO orderStatisticsDAO) {
        this.orderStatisticsDAO = orderStatisticsDAO;
    }

    @Autowired
    public void setStatisticsService(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @Autowired
    public void setShowCaseMapper(ProductStatisticsShowCaseMapper showCaseMapper) {
        this.showCaseMapper = showCaseMapper;
    }

    @Transactional
    @Override
    public List<ShowCaseDTO> getTopSortElements() {
        List<Modification> modifications = modificationDAO.topModifications();
        List<ShowCaseDTO> showCaseDTOList = new ArrayList<>();
        for (Modification mod: modifications) {
            Product product = mod.getProduct();
            product.setProductProperties(productPropertyDAO.getProductProperties(product));
            ShowCaseDTO sc = new ShowCaseDTO(mod);
            showCaseDTOList.add(sc);
        }
        return showCaseDTOList;
    }

    @Transactional
    @Override
    public List<ShowCaseDTO> getTopSoldElements() {
        LocalDateTime dateFromFormatted = statisticsService.getDateFrom(Constants.EARLIEST_DATE);
        LocalDateTime dateToFormatted = statisticsService.getDateTo(Constants.LATEST_DATE);
        List<ProductStatisticsDTO> productStatisticsDTOList = orderStatisticsDAO.getAggregatedProducts(dateFromFormatted, dateToFormatted);
        productStatisticsDTOList = statisticsService.fillProductsWithModification(productStatisticsDTOList);
        List<ShowCaseDTO> showCaseDTOList = productStatisticsDTOList.stream()
                .map(psDTO -> showCaseMapper.getShowCaseDTO(psDTO))
                .collect(Collectors.toList());
        showCaseDTOList = showCaseDTOList.subList(0, Constants.TOP_MODIFICATIONS_COUNT);
        return showCaseDTOList;
    }
}
