package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UnitDAO;
import ru.t_systems.samushenkov.tires_shop.model.Unit;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UnitService;

import java.util.List;

@Service
public class UnitServiceImpl implements UnitService {
    private UnitDAO unitDAO;

    @Autowired
    public void setUnitDAO(UnitDAO unitDAO) {
        this.unitDAO = unitDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Unit> allUnits() {
        return unitDAO.allUnits();
    }

    @Transactional
    public Unit getById(int id) {
        return unitDAO.getById(id);
    }
}
