package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ModificationDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderStatisticsDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UserDAO;
import ru.t_systems.samushenkov.tires_shop.dto.ClientStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.StatisticsService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    OrderStatisticsDAO orderStatisticsDAO;
    UserDAO userDAO;
    ModificationDAO modificationDAO;

    @Autowired
    public void setOrderStatisticsDAO(OrderStatisticsDAO orderStatisticsDAO) {
        this.orderStatisticsDAO = orderStatisticsDAO;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setModificationDAO(ModificationDAO modificationDAO) {
        this.modificationDAO = modificationDAO;
    }

    @Transactional
    public ModelAndView getAllStatisticsItems(int page, String dateFrom, String dateTo, String type) {
        ModelAndView modelAndView = new ModelAndView();
        if((type == null) || (dateFrom == null) || (dateTo == null)) {
            modelAndView.setViewName(Constants.STATISTICS_MAIN_PAGE);
        } else {
            switch (type) {
                case Constants.STATISTICS_INCOME_TYPE:
                    modelAndView.addObject("income", getIncomeStatistics(dateFrom, dateTo));
                    modelAndView.setViewName(Constants.STATISTICS_INCOME_PAGE);
                    break;
                case Constants.STATISTICS_CLIENT_TYPE:
                    List<ClientStatisticsDTO> clientsList = getClientStatistics(dateFrom, dateTo);
                    fillClientsWithUser(clientsList);
                    modelAndView.addObject("clients", clientsList);
                    modelAndView.setViewName(Constants.STATISTICS_CLIENT_PAGE);
                    break;
                case Constants.STATISTICS_PRODUCT_TYPE:
                    List<ProductStatisticsDTO> productsList = getProductStatistics(dateFrom, dateTo);
                    fillProductsWithModification(productsList);
                    modelAndView.addObject("products", productsList);
                    modelAndView.setViewName(Constants.STATISTICS_PRODUCT_PAGE);
                    break;
                default:
                    modelAndView.setViewName(Constants.STATISTICS_MAIN_PAGE);
                    break;
            }
        }
        return modelAndView;
    }

    @Override
    public LocalDateTime getDateFrom(String dateFrom) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        formatter = formatter.withLocale(Locale.US);
        return LocalDate.parse(dateFrom, formatter).atTime(0,0,0);
    }

    @Override
    public LocalDateTime getDateTo(String dateTo) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        formatter = formatter.withLocale(Locale.US);
        return LocalDate.parse(dateTo, formatter).atTime(23,59,59);
    }

    private List<ClientStatisticsDTO> getClientStatistics(String dateFrom, String dateTo) {
        LocalDateTime dateFromFormatted = getDateFrom(dateFrom);
        LocalDateTime dateToFormatted = getDateTo(dateTo);
        return orderStatisticsDAO.getAggregatedClients(dateFromFormatted, dateToFormatted);
    }

    private List<ProductStatisticsDTO> getProductStatistics(String dateFrom, String dateTo) {
        LocalDateTime dateFromFormatted = getDateFrom(dateFrom);
        LocalDateTime dateToFormatted = getDateTo(dateTo);
        return orderStatisticsDAO.getAggregatedProducts(dateFromFormatted, dateToFormatted);
    }

    private Object getIncomeStatistics(String dateFrom, String dateTo) {
        LocalDateTime dateFromFormatted = getDateFrom(dateFrom);
        LocalDateTime dateToFormatted = getDateTo(dateTo);
        return orderStatisticsDAO.getTotalAmount(dateFromFormatted, dateToFormatted);
    }

    private List<ClientStatisticsDTO> fillClientsWithUser(List<ClientStatisticsDTO> clientsList) {
        for (ClientStatisticsDTO client: clientsList) {
            client.setUser(userDAO.getById(client.getUserId()));
        }
        return clientsList;
    }

    @Override
    public List<ProductStatisticsDTO> fillProductsWithModification(List<ProductStatisticsDTO> productsList) {
        for (ProductStatisticsDTO product: productsList) {
            product.setModification(modificationDAO.getById(product.getModification().getId()));
        }
        return productsList;
    }
}
