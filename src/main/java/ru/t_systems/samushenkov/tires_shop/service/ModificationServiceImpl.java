package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ModificationDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductPropertyDAO;
import ru.t_systems.samushenkov.tires_shop.dto.ModificationDTO;
import ru.t_systems.samushenkov.tires_shop.dto.TiresFilterDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.ModificationMapper;
import ru.t_systems.samushenkov.tires_shop.model.*;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationPropertyService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationService;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ModificationServiceImpl implements ModificationService {
    private ModificationDAO modificationDAO;
    private ProductDAO productDAO;
    private ProductPropertyDAO productPropertyDAO;
    private ModificationPropertyService modificationPropertyService;
    private ModificationMapper modificationMapper;

    @Autowired
    public void setModificationDAO(ModificationDAO modificationDAO) {
        this.modificationDAO = modificationDAO;
    }

    @Autowired
    public void setModificationPropertyService(ModificationPropertyService modificationPropertyService) {
        this.modificationPropertyService = modificationPropertyService;
    }

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Autowired
    public void setProductPropertyDAO(ProductPropertyDAO productPropertyDAO) {
        this.productPropertyDAO = productPropertyDAO;
    }

    @Autowired
    public void setModificationMapper(ModificationMapper modificationMapper) {
        this.modificationMapper = modificationMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public ModelAndView productModifications(int productId) {
        List<Modification> modifications = modificationDAO.productModifications(productId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("modificationsList", modifications);
        modelAndView.addObject("product", productDAO.getById(productId));
        return modelAndView;
    }

    @Override
    @Transactional(readOnly = true)
    public ModelAndView allModifications(int page, int categoryId, TiresFilterDTO tiresFilterDTO) {
        int modificationsCount = modificationDAO.allModificationsCount(categoryId, tiresFilterDTO);
        int pagesCount = (modificationsCount + 11)/12;
        List<Modification> modifications = modificationDAO.allModifications(page, categoryId, tiresFilterDTO);
        List<PropertyEnumValue> brands = modificationDAO.getStockBrands(categoryId);
        List<PropertyEnumValue> seasons = modificationDAO.getStockSeasons(categoryId);
        BigDecimal maxPrice = modificationDAO.getMaxPrice(categoryId);
        BigDecimal minPrice = modificationDAO.getMinPrice(categoryId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("page", page);
        modelAndView.addObject("itemsCount", modificationsCount);
        modelAndView.addObject("pagesCount", pagesCount);
        modelAndView.addObject("brands", brands);
        modelAndView.addObject("seasons", seasons);
        modelAndView.addObject("maxPrice", maxPrice);
        modelAndView.addObject("minPrice", minPrice);
        modelAndView.addObject("modificationsList", modifications);
        return modelAndView;
    }

    @Override
    @Transactional
    public ModificationDTO add(ModificationDTO modificationDTO) {
        Modification modification = modificationDAO.add(modificationMapper.convertToEntity(modificationDTO));
        editModificationProperties(modification);
        return modificationMapper.convertToDTO(modification);
    }

    @Override
    @Transactional
    public void delete(int modificationId) {
        modificationDAO.delete(getById(modificationId));
    }

    @Override
    @Transactional
    public void edit(ModificationDTO modificationDTO) {
        modificationDAO.edit(modificationMapper.convertToEntity(modificationDTO));
        editModificationProperties(modificationMapper.convertToEntity(modificationDTO));
    }

    @Override
    @Transactional
    public ModelAndView getModification(int id) {
        ModelAndView modelAndView = new ModelAndView();
        Modification modification = modificationDAO.getById(id);
        List<ModificationProperty> modificationProperties = modificationPropertyService.getModificationProperties(modification, productDAO.getById(modification.getProduct().getId()));
        modification.setModificationProperties(modificationProperties);
        modelAndView.addObject("modification", modification);
        return modelAndView;
    }

    @Override
    @Transactional(readOnly = true)
    public Modification getById(int id) {
        return modificationDAO.getById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ModelAndView getMVById(int id) {
        Modification modification = modificationDAO.getById(id);
        Product product = modification.getProduct();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", modification.getProduct().getName());
        modelAndView.addObject("modification",  modification);
        modelAndView.addObject("productProperties", productPropertyDAO.getProductProperties(product));
        return modelAndView;
    }

    private void editModificationProperties(Modification modification) {
        if(modification.getModificationProperties() != null) {
            for (ModificationProperty mp : modification.getModificationProperties()) {
                if (mp.getId() != 0) {
                    modificationPropertyService.edit(mp);
                } else if((mp.getId() == 0) && (!mp.getValue().equals(""))) {
                    mp.setModification(modification);
                    modificationPropertyService.add(mp);
                }
            }
        }
    }
}
