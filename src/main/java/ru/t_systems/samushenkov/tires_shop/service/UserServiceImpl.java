package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.*;
import ru.t_systems.samushenkov.tires_shop.dto.UserAddressDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserNoPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.UserAddressMapper;
import ru.t_systems.samushenkov.tires_shop.model.Authority;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.model.UserAddress;
import ru.t_systems.samushenkov.tires_shop.model.type.AuthorityType;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UserService;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;
    private AuthorityDAO authorityDAO;
    private OrderDAO orderDAO;
    private UserAddressDAO userAddressDAO;
    private CountryDAO countryDAO;
    private UserAddressMapper userAddressMapper;

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setAuthorityDAO(AuthorityDAO authorityDAO) {
        this.authorityDAO = authorityDAO;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setUserAddressDAO(UserAddressDAO userAddressDAO) {
        this.userAddressDAO = userAddressDAO;
    }

    @Autowired
    public void setCountryDAO(CountryDAO countryDAO) {
        this.countryDAO = countryDAO;
    }

    @Autowired
    public void setUserAddressMapper(UserAddressMapper userAddressMapper) {
        this.userAddressMapper = userAddressMapper;
    }

    @Transactional
    @Override
    public User getById(int id) {
        return userDAO.getById(id);
    }

    @Transactional
    @Override
    public User registerNewUserAccount(UserDTO userDto, BindingResult result) {

        if(result.hasErrors()) {
            return null;
        }
        if (emailExist(userDto.getEmail())) {
            result.rejectValue("email", "Exists.userDTO.email", "Пользователь с таким Email уже существует");
            return null;
        }
        final User user = new User(userDto, true);
        Authority authority = authorityDAO.getByName(AuthorityType.ROLE_USER);
        Set<Authority> setAuth = new HashSet<>();
        setAuth.add(authority);
        user.setAuthorities(setAuth);
        return userDAO.add(user);
    }

    @Transactional
    @Override
    public ModelAndView getUserOrders(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        int userId = getUserIdFromSession(session);
        if(userId != 0) {
            User user = userDAO.getById(userId);
            List<Order> ordersList = orderDAO.getAllUserOrders(user);
            modelAndView.addObject("ordersList", ordersList);
        }
        return modelAndView;
    }

    @Transactional
    @Override
    public ModelAndView getUserInformation(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        int userId = getUserIdFromSession(session);
        if(userId != 0) {
            User user = userDAO.getById(userId);
            List<UserAddress> userAddresses = userAddressDAO.getAllUserAddresses(user);
            modelAndView.addObject("user", user);
            modelAndView.addObject("countriesList", countryDAO.allCountries());
            modelAndView.addObject("userAddresses", userAddresses);
        }
        return modelAndView;
    }

    @Transactional
    @Override
    public UserNoPasswordDTO editUser(UserNoPasswordDTO userNoPasswordDTO, HttpSession session, BindingResult result) {
        if(result.hasErrors()) {
            return null;
        }
        int userId = getUserIdFromSession(session);
        if (emailExist(userNoPasswordDTO.getEmail())) {
            result.rejectValue("email", "Exists.userDTO.email", "Пользователь с таким Email уже существует");
        }
        if(userId != 0) {
            User user = new User(userNoPasswordDTO);
            user.setId(userId);
            userDAO.edit(user);
            userNoPasswordDTO.setId(user.getId());
        }
        return userNoPasswordDTO;
    }

    @Transactional
    @Override
    public boolean editPassword(UserPasswordDTO userPasswordDTO, HttpSession session) {
        int userId = getUserIdFromSession(session);
        if(userId != 0) {
            try{
                User user = userDAO.getById(userId);
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                if(passwordEncoder.matches(userPasswordDTO.getOldPassword(), user.getPassword())
                        && userPasswordDTO.getPassword().equals(userPasswordDTO.getMatchingPassword())
                        && (userPasswordDTO.getPassword().length() > 4)) {
                    user.setPassword(passwordEncoder.encode(userPasswordDTO.getPassword()));
                    userDAO.edit(user);
                    return true;
                }

            } catch (NullPointerException e) {
                return false;
            }
        }
        return false;
    }

    @Transactional
    @Override
    public boolean editUserAddress(UserAddressDTO userAddressDTO, HttpSession session) {
        int userId = getUserIdFromSession(session);
        if(userId != 0) {
            User user = userDAO.getById(userId);
            userAddressDTO.setUser(user);
            userAddressDAO.edit(userAddressMapper.convertToEntity(userAddressDTO));
            return true;
        }
        return false;
    }

    @Override
    public int getUserIdFromSession(HttpSession session) {
        Object userId = session.getAttribute("userId");
        if(userId != null) {
            return Integer.parseInt(userId.toString());
        }
        return 0;
    }

    @Transactional
    @Override
    public void deleteUser(User user) {
        userDAO.delete(user);
    }

    private boolean emailExist(String email) {
        User user = userDAO.getUserByUsername(email);
        return user != null;
    }
}
