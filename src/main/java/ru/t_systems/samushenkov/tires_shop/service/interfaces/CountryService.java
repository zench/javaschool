package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Country;
import java.util.List;

public interface CountryService {
    List<Country> allCountries();
    Country getById(int id);
}
