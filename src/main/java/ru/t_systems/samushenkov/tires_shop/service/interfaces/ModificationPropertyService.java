package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;
import ru.t_systems.samushenkov.tires_shop.model.Product;

import java.util.List;

public interface ModificationPropertyService {
    List<ModificationProperty> getModificationProperties(Modification modification, Product product);
    ModificationProperty add(ModificationProperty modificationProperty);
    void delete(ModificationProperty modificationProperty);
    void edit(ModificationProperty modificationProperty);
    ModificationProperty getById(int id);
}
