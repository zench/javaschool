package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CartItemDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UserDAO;
import ru.t_systems.samushenkov.tires_shop.model.CartItem;
import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartItemService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationService;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CartItemServiceImpl implements CartItemService {
    private CartItemDAO cartItemDAO;
    private ModificationService modificationService;
    private UserDAO userDAO;

    @Autowired
    public void setCartItemDAO(CartItemDAO cartItemDAO) {
        this.cartItemDAO = cartItemDAO;
    }

    @Autowired
    public void setModificationService(ModificationService modificationService) {
        this.modificationService = modificationService;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CartItem> getCartItemsBySessionId(String sessionId) {
        return cartItemDAO.getCartItemsBySessionId(sessionId);
    }

    @Override
    @Transactional
    public CartItem add(String sessionId, int modificationId, int count, Integer userId) {
        CartItem dbCartItem = cartItemDAO.getCartItemByModIdAndSessionId(modificationId, sessionId);
        LocalDateTime dateTime = LocalDateTime.now();
        if(dbCartItem != null) {
            if(dbCartItem.getModification().getStockBalance() >= count) {
                dbCartItem.setModified(dateTime);
                dbCartItem.setCount(dbCartItem.getCount() + count);
                cartItemDAO.edit(dbCartItem);
                return dbCartItem;
            } else {
                /*TODO
                *  create jsp error*/
            }
        } else {
            CartItem cartItem = new CartItem();
            cartItem.setCreated(dateTime);
            cartItem.setModified(dateTime);
            cartItem.setSessionId(sessionId);
            cartItem.setModification(modificationService.getById(modificationId));
            cartItem.setCount(count);
            if(userId != null) {
                User user = userDAO.getById(userId);
                cartItem.setUser(user);
            } else {
                cartItem.setUser(null);
            }
            return cartItemDAO.add(cartItem);
        }
        return null;
    }

    @Override
    @Transactional
    public void delete(int cartItemId) {
        CartItem cartItem = cartItemDAO.getById(cartItemId);
        cartItemDAO.delete(cartItem);
    }

    @Override
    @Transactional
    public void edit(CartItem cartItem) {
        LocalDateTime dateTime = LocalDateTime.now();
        cartItem.setModified(dateTime);
        cartItemDAO.edit(cartItem);
    }

    @Override
    @Transactional(readOnly = true)
    public CartItem getById(int id) {
        return cartItemDAO.getById(id);
    }
}
