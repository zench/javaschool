package ru.t_systems.samushenkov.tires_shop.service.interfaces;

import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ModificationDTO;
import ru.t_systems.samushenkov.tires_shop.dto.TiresFilterDTO;
import ru.t_systems.samushenkov.tires_shop.model.Modification;

public interface ModificationService {
    ModelAndView allModifications(int page, int categoryId, TiresFilterDTO tiresFilterDTO);
    ModelAndView productModifications(int productId);
    ModificationDTO add(ModificationDTO modificationDTO);
    void delete(int modificationId);
    void edit(ModificationDTO modificationDTO);
    ModelAndView getModification(int id);
    Modification getById(int id);
    ModelAndView getMVById(int id);
}
