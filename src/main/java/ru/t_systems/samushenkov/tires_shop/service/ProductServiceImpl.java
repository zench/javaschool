package ru.t_systems.samushenkov.tires_shop.service;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CountryDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductDAO;
import ru.t_systems.samushenkov.tires_shop.dto.ProductDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.ProductMapper;
import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CategoryDAO;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CategoryService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductPropertyService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductService;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    private ProductDAO productDAO;
    private CategoryDAO categoryDAO;
    private CountryDAO countryDAO;
    private ProductPropertyService productPropertyService;
    private CategoryService categoryService;
    private ProductMapper productMapper;

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Autowired
    public void setCategoryDAO(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Autowired
    public void setCountryDAO(CountryDAO countryDAO) {
        this.countryDAO = countryDAO;
    }

    @Autowired
    public void setProductPropertyService(ProductPropertyService productPropertyService) {
        this.productPropertyService = productPropertyService;
    }

    @Autowired
    public void setProductMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    @Transactional(readOnly = true)
    public ModelAndView allProducts(int page, int categoryId) {
        List<Product> products =  productDAO.allProducts(page, categoryId);
        int productsCount = productsCount(categoryId);
        int pagesCount = (productsCount + 9)/10;
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("productsList", products);
        modelAndView.addObject("category", categoryService.getById(categoryId));
        modelAndView.addObject("page", page);
        modelAndView.addObject("itemsCount", productsCount);
        modelAndView.addObject("pagesCount", pagesCount);
        return modelAndView;
    }

    @Override
    @Transactional
    public ProductDTO add(ProductDTO productDTO, String pathForImage) {
        if(pathForImage != null) {
            saveImage(productDTO, pathForImage);
        }
        Product product = productDAO.add(productMapper.convertToEntity(productDTO));
        editProductProperties(product);
        return productMapper.convertToDTO(product);
    }

    @Override
    @Transactional
    public void delete(int productId) {
        productDAO.delete(getById(productId));
    }

    @Override
    @Transactional
    public void edit(ProductDTO productDTO, String pathForImage) {
        if(pathForImage != null) {
            saveImage(productDTO, pathForImage);
        }
        productDAO.edit(productMapper.convertToEntity(productDTO));
        editProductProperties(productMapper.convertToEntity(productDTO));
    }

    @Override
    @Transactional
    public ModelAndView getProduct(int id) {
        ModelAndView modelAndView = new ModelAndView();
        Product product = productDAO.getById(id);
        List<ProductProperty> productProperties = productPropertyService.getProductProperties(product);
        product.setProductProperties(productProperties);
        modelAndView.addObject("product", product);
        modelAndView.addObject("categoriesList", getProductCategories());
        modelAndView.addObject("countriesList", getProductCountries());
        return modelAndView;
    }

    @Override
    @Transactional
    public Product getById(int id) {
        return productDAO.getById(id);
    }

    @Override
    @Transactional
    public int productsCount(int categoryId) {
        return productDAO.productsCount(categoryId);
    }

    @Override
    @Transactional
    public List<Category> getProductCategories() {
        return categoryDAO.allCategories();
    }

    @Override
    @Transactional
    public List<Country> getProductCountries() {
        return countryDAO.allCountries();
    }

    private void editProductProperties(Product product) {
        if(product.getProductProperties() != null) {
            for (ProductProperty pp : product.getProductProperties()) {
                if ((pp.getId() != 0) && (!pp.getValue().equals(""))) {
                    productPropertyService.edit(pp);
                } else if ((pp.getId() == 0) && (!pp.getValue().equals(""))) {
                    pp.setProduct(product);
                    productPropertyService.add(pp);
                } else if ((pp.getId() != 0) && (pp.getValue().equals(""))) {
                    productPropertyService.edit(pp);
                }
            }
        }
    }

    private void saveImage(ProductDTO productDTO, String pathForImage) {
        MultipartFile file = productDTO.getImage();

        if (null != file && file.getSize() > 0)
        {
            String fileName = "";
            if(file.getOriginalFilename().isEmpty()) {
                fileName = RandomStringUtils.randomAlphanumeric(20);
            } else {
                fileName = file.getOriginalFilename();
            }

            File imageFile = new File(pathForImage, fileName);
            try
            {
                file.transferTo(imageFile);
                productDTO.setPhoto(Constants.STOCK_IMAGE_STORAGE +fileName);
            } catch (IOException e)
            {
                log.error("Uploading of product image is failed");
                log.error(e.getMessage());
            }
        }
    }
}
