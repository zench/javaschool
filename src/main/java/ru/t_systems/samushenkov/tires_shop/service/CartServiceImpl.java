package ru.t_systems.samushenkov.tires_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dao.DeliveryTypeDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.PaymentTypeDAOImpl;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CountryDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderDAO;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UserDAO;
import ru.t_systems.samushenkov.tires_shop.model.Cart;
import ru.t_systems.samushenkov.tires_shop.model.CartItem;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartItemService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartService;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UserService;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    private static final String SESSION_CART_PARAM_NAME = "cartSession";
    private CartItemService cartItemService;
    private UserService userService;
    private OrderDAO orderDAO;
    private UserDAO userDAO;
    private DeliveryTypeDAOImpl deliveryTypeDAO;
    private PaymentTypeDAOImpl paymentTypeDAO;
    private CountryDAO countryDAO;

    @Autowired
    public void setCartItemService(CartItemService cartItemService) {
        this.cartItemService = cartItemService;
    }

    @Autowired
    public void setUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setDeliveryTypeDAO(DeliveryTypeDAOImpl deliveryTypeDAO) {
        this.deliveryTypeDAO = deliveryTypeDAO;
    }

    @Autowired
    public void setPaymentTypeDAO(PaymentTypeDAOImpl paymentTypeDAO) {
        this.paymentTypeDAO = paymentTypeDAO;
    }

    @Autowired
    public void setCountryDAO(CountryDAO countryDAO) {
        this.countryDAO = countryDAO;
    }

    @Transactional
    @Override
    public void updateCart(Cart cart, Integer userId) {
        if(!cart.getCartItems().isEmpty()) {
            for (CartItem cartItem : cart.getCartItems()) {
                if(userId != null) {
                    cartItem.setUser(userService.getById(userId));
                }
                cartItemService.edit(cartItem);
            }
        }
    }

    @Override
    public String getCartSessionId(HttpSession session) {
        Object cartSessionId = session.getAttribute(SESSION_CART_PARAM_NAME);
        if(cartSessionId == null) {
            session.setAttribute(SESSION_CART_PARAM_NAME, session.getId());
        }
        return session.getAttribute(SESSION_CART_PARAM_NAME).toString();
    }

    @Override
    @Transactional
    public void setCartUser(HttpSession session) {
        List<CartItem> cartItems = cartItemService.getCartItemsBySessionId(getCartSessionId(session));
        Object userId = session.getAttribute("userId");
        if(userId != null) {
            User user = userService.getById(Integer.parseInt(userId.toString()));
            if(cartItems != null) {
                for (CartItem cartItem : cartItems) {
                    if(cartItem.getUser() == null){
                        cartItem.setUser(user);
                        cartItemService.edit(cartItem);
                    }
                }
            }
        }
    }

    @Override
    public List<CartItem> getCartItems(HttpSession session) {
        Object userId = session.getAttribute("userId");
        if(userId != null) {
            return userService.getById(Integer.parseInt(userId.toString()))
                    .getCartItems();
        }
        return cartItemService.getCartItemsBySessionId(getCartSessionId(session));
    }

    @Override
    @Transactional
    public ModelAndView getMVCartItems(HttpSession session) {
        List<CartItem> cartItems = getCartItems(session);
        BigDecimal total = new BigDecimal(0);
        int totalCount = 0;
        for (CartItem cartItem : cartItems) {
            BigDecimal count = new BigDecimal(cartItem.getCount());
            total = total.add(count.multiply(cartItem.getModification().getPrice()));
            totalCount += cartItem.getCount();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("cartItems", cartItems);
        int userId = userService.getUserIdFromSession(session);
        if(userId != 0) {
            Order lastUserOrder = orderDAO.getLastUserOrder(userDAO.getById(userService.getUserIdFromSession(session)));
            modelAndView.addObject("lastOrder", lastUserOrder);
            modelAndView.addObject("user", userService.getById(userId));
        }

        modelAndView.addObject("deliveryTypesList", deliveryTypeDAO.allDeliveryTypes());
        modelAndView.addObject("paymentTypesList", paymentTypeDAO.allPaymentTypes());
        modelAndView.addObject("countriesList", countryDAO.allCountries());
        modelAndView.addObject("total", total);
        modelAndView.addObject("totalCount", totalCount);
        return modelAndView;
    }
}
