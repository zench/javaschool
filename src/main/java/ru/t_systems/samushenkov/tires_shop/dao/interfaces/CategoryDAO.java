package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Category;

import java.util.List;

public interface CategoryDAO {
    List<Category> allCategories();
    Category add(Category category);
    void delete(Category category);
    void edit(Category category);
    Category getById(int id);
}
