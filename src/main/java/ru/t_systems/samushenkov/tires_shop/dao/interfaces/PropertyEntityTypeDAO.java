package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;

import java.util.List;

public interface PropertyEntityTypeDAO {
    List<PropertyEntityType> allEntityTypes();
    PropertyEntityType getById(int id);
}
