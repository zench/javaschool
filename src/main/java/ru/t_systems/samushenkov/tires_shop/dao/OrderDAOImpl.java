package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderDAO;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.User;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class OrderDAOImpl extends AbstractBaseDAO implements OrderDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> getAllUserOrders(User user) {
        return getSession()
                .createQuery("from Order where user = :code order by created desc")
                .setParameter("code", user)
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> getAllOrders(int page) {
        return getSession()
                .createQuery("from Order order by created desc")
                .setFirstResult(10 * (page - 1))
                .setMaxResults(10)
                .list();
    }

    @Override
    public Order getLastUserOrder(User user) {
        try {
           return getSession()
                .createQuery("from Order where user = :code order by created desc", Order.class)
                .setParameter("code", user)
                .setMaxResults(1)
                .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Order add(Order order) {
        LocalDateTime dateTime = LocalDateTime.now();
        order.setCreated(dateTime);
        getSession().persist(order);
        return order;
    }

    @Override
    public void edit(Order order) {
        LocalDateTime dateTime = LocalDateTime.now();
        Order dbOrder = getById(order.getId());
        dbOrder.setModified(dateTime);
        dbOrder.setStatus(order.getStatus());
        dbOrder.setPaymentType(order.getPaymentType());
        dbOrder.setDeliveryType(order.getDeliveryType());
        dbOrder.setPaymentStatus(order.getPaymentStatus());
        getSession().update(dbOrder);
    }

    @Override
    public Order getById(int id) {
        return getSession().get(Order.class, id);
    }

    @Override
    public int ordersCount() {
        return getSession().createQuery("select count(*) from Order", Number.class).getSingleResult().intValue();
    }
}
