package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.model.PaymentType;

import java.util.List;

@Repository
public class PaymentTypeDAOImpl extends AbstractBaseDAO {
    @SuppressWarnings("unchecked")
    public List<PaymentType> allPaymentTypes() {
        return getSession().createQuery("from PaymentType ").list();
    }

    public PaymentType getById(int id) {
        return getSession().get(PaymentType.class, id);
    }
}
