package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CountryDAO;
import ru.t_systems.samushenkov.tires_shop.model.Country;

import java.util.List;

@Repository
public class CountryDAOImpl extends AbstractBaseDAO implements CountryDAO {

    public List<Country> allCountries() {
        return getSession().createQuery("from Country").list();
    }

    public Country getById(int id) {
        return getSession().get(Country.class, id);
    }
}
