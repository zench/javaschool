package ru.t_systems.samushenkov.tires_shop.dao;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CartItemDAO;
import ru.t_systems.samushenkov.tires_shop.model.CartItem;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class CartItemDAOImpl extends AbstractBaseDAO implements CartItemDAO {
    @Override
    public List<CartItem> getCartItemsBySessionId(String sessionId) {
        String condition = "from CartItem where sessionId = :code";

        Query query = getSession().createQuery(condition).setParameter("code", sessionId);
        return query.list();
    }

    @Override
    public CartItem getCartItemByModIdAndSessionId(int modificationId, String sessionId) {
        String condition = "from CartItem where sessionId = :code and modification.id = :code2";
        try {
            return getSession().createQuery(condition, CartItem.class).setParameter("code", sessionId).setParameter("code2", modificationId).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public CartItem add(CartItem cartItem) {
        getSession().persist(cartItem);
        return cartItem;
    }

    @Override
    public void delete(CartItem cartItem) {
            getSession().delete(cartItem);
    }

    @Override
    public void edit(CartItem cartItem) {
        CartItem dbCartItem = getById(cartItem.getId());
        dbCartItem.setModified(cartItem.getModified());
        if(cartItem.getModification() != null) {
            dbCartItem.setModification(cartItem.getModification());
        }
        if(cartItem.getUser() != null) {
            dbCartItem.setUser(cartItem.getUser());
        }
        dbCartItem.setCount(cartItem.getCount());
        getSession().evict(dbCartItem);
        getSession().update(dbCartItem);
    }

    @Override
    public CartItem getById(int id) {
        return getSession().get(CartItem.class, id);
    }

}
