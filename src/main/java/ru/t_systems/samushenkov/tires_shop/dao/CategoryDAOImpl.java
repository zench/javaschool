package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.CategoryDAO;
import ru.t_systems.samushenkov.tires_shop.model.Category;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class CategoryDAOImpl extends AbstractBaseDAO implements CategoryDAO {

    @SuppressWarnings("unchecked")
    public List<Category> allCategories() {
        return getSession().createQuery("from Category").list();
    }

    public Category add(Category category) {
        LocalDateTime dateTime = LocalDateTime.now();
        category.setCreated(dateTime);
        category.setModified(dateTime);
        getSession().persist(category);
        return category;
    }

    public void delete(Category category) {
        getSession().delete(category);
    }

    public void edit(Category category) {
        Category dbCat = getById(category.getId());
        LocalDateTime dateTime = LocalDateTime.now();
        dbCat.setModified(dateTime);
        dbCat.setName(category.getName());
        getSession().update(dbCat);
    }

    public Category getById(int id) {
        return getSession().get(Category.class, id);
    }
}
