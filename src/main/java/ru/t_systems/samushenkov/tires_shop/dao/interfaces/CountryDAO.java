package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Country;

import java.util.List;

public interface CountryDAO {
    List<Country> allCountries();
    Country getById(int id);
}
