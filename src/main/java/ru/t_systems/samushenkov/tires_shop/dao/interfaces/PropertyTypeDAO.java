package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.PropertyType;

import java.util.List;

public interface PropertyTypeDAO {
    List<PropertyType> allTypes();
    PropertyType getById(int id);
}
