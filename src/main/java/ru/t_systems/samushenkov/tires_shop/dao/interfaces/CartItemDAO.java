package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.CartItem;

import java.util.List;

public interface CartItemDAO {
    List<CartItem> getCartItemsBySessionId(String sessionId);
    CartItem getCartItemByModIdAndSessionId(int modificationId, String sessionId);
    CartItem add(CartItem cartItem);
    void delete(CartItem cartItem);
    void edit(CartItem cartItem);
    CartItem getById(int id);
}
