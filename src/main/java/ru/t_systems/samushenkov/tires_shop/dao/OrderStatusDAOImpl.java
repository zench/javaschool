package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.model.OrderStatus;

import java.util.List;

@Repository
public class OrderStatusDAOImpl extends AbstractBaseDAO {
    @SuppressWarnings("unchecked")
    public List<OrderStatus> allOrderStatuses() {
        return getSession().createQuery("from OrderStatus ").list();
    }

    public OrderStatus getById(int id) {
        return getSession().get(OrderStatus.class, id);
    }
}
