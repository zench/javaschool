package ru.t_systems.samushenkov.tires_shop.dao;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.PropertyDAO;
import ru.t_systems.samushenkov.tires_shop.model.Property;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class PropertyDAOImpl extends AbstractBaseDAO implements PropertyDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Property> allProperties(int categoryId, int entityTypeId) {
        String condition = "from Property where categoryId = :code ";
        if(entityTypeId != 0) {
            condition = condition + "AND entityType.id = :entityTypeId";
        }
        Query query = getSession().createQuery(condition).setParameter("code", categoryId);
        if(entityTypeId != 0) {
            query.setParameter("entityTypeId", entityTypeId);
        }
        return query.list();
    }

    @Override
    public Property add(Property property) {
        LocalDateTime dateTime = LocalDateTime.now();
        property.setCreated(dateTime);
        property.setModified(dateTime);
        getSession().persist(property);
        return property;
    }

    @Override
    public void delete(Property property) {
        getSession().delete(property);
    }

    @Override
    public void edit(Property property) {
        LocalDateTime dateTime = LocalDateTime.now();
        Property dbProp = getById(property.getId());
        dbProp.setModified(dateTime);
        dbProp.setName(property.getName());
        dbProp.setCategoryId(property.getCategoryId());
        dbProp.setType(property.getType());
        if((property.getUnit() != null) && (property.getUnit().getId() == 0)) {
            property.setUnit(null);
        }
        dbProp.setUnit(property.getUnit());
        dbProp.setEntityType(property.getEntityType());
        getSession().update(dbProp);
    }

    @Override
    public Property getById(int id) {
        return getSession().get(Property.class, id);
    }
}
