package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.model.DeliveryType;

import java.util.List;

@Repository
public class DeliveryTypeDAOImpl extends AbstractBaseDAO {
    @SuppressWarnings("unchecked")
    public List<DeliveryType> allDeliveryTypes() {
        return getSession().createQuery("from DeliveryType ").list();
    }

    public DeliveryType getById(int id) {
        return getSession().get(DeliveryType.class, id);
    }
}
