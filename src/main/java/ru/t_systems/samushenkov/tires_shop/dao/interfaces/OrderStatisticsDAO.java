package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.dto.ClientStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.OrderStatistics;
import ru.t_systems.samushenkov.tires_shop.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderStatisticsDAO {
    List<OrderStatistics> getAllUserItems(User user);
    List<OrderStatistics> getAllOrderItems(Order order);
    List<OrderStatistics> getAllItems(LocalDateTime from, LocalDateTime to);
    Object getTotalAmount(LocalDateTime fromDate, LocalDateTime toDate);
    List<ClientStatisticsDTO> getAggregatedClients(LocalDateTime fromDate, LocalDateTime toDate);
    List<ProductStatisticsDTO> getAggregatedProducts(LocalDateTime fromDate, LocalDateTime toDate);

    OrderStatistics add(OrderStatistics statistics);
}
