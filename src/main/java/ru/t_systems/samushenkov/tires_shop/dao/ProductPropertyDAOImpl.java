package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductPropertyDAO;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;

import java.util.List;

@Repository
public class ProductPropertyDAOImpl extends AbstractBaseDAO implements ProductPropertyDAO {

    @Override
    public ProductProperty add(ProductProperty productProperty) {
        getSession().persist(productProperty);
        return productProperty;
    }

    @Override
    public void delete(ProductProperty productProperty) {
        getSession().delete(productProperty);
    }

    @Override
    public void edit(ProductProperty productProperty) {
        ProductProperty dbModProp = getById(productProperty.getId());
        dbModProp.setModified(productProperty.getModified());
        dbModProp.setValue(productProperty.getValue());
        getSession().update(dbModProp);
    }

    @Override
    public ProductProperty getById(int id) {
        return getSession().get(ProductProperty.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ProductProperty> getProductProperties(Product product) {
        return getSession().createQuery("from ProductProperty where product = :product").setParameter("product", product).list();
    }
}
