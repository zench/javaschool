package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;

import java.util.List;

public interface ProductPropertyDAO {
    ProductProperty add(ProductProperty productProperty);
    void delete(ProductProperty productProperty);
    void edit(ProductProperty productProperty);
    ProductProperty getById(int id);
    List<ProductProperty> getProductProperties(Product product);
}
