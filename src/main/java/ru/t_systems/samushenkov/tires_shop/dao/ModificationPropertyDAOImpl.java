package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ModificationPropertyDAO;
import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;

@Repository
public class ModificationPropertyDAOImpl extends AbstractBaseDAO implements ModificationPropertyDAO {

    @Override
    public ModificationProperty add(ModificationProperty modificationProperty) {
        getSession().persist(modificationProperty);
        return modificationProperty;
    }

    @Override
    public void delete(ModificationProperty modificationProperty) {
        getSession().delete(modificationProperty);
    }

    @Override
    public void edit(ModificationProperty modificationProperty) {
        ModificationProperty dbModProp = getById(modificationProperty.getId());
        dbModProp.setModified(modificationProperty.getModified());
        dbModProp.setValue(modificationProperty.getValue());
        getSession().update(dbModProp);
    }

    @Override
    public ModificationProperty getById(int id) {
        return getSession().get(ModificationProperty.class, id);
    }
}
