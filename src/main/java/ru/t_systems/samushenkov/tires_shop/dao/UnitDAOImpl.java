package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UnitDAO;
import ru.t_systems.samushenkov.tires_shop.model.Unit;

import java.util.List;

@Repository
public class UnitDAOImpl extends AbstractBaseDAO implements UnitDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Unit> allUnits() {
        return getSession().createQuery("from Unit").list();
    }

    @Override
    public Unit getById(int id) {
        return getSession().get(Unit.class, id);
    }
}
