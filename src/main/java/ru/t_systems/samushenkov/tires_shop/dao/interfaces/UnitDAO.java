package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Unit;

import java.util.List;

public interface UnitDAO {
    List<Unit> allUnits();
    Unit getById(int id);
}
