package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.model.UserAddress;

import java.util.List;

public interface UserAddressDAO {
    List<UserAddress> getAllUserAddresses(User user);
    UserAddress add(UserAddress userAddress);
    void edit(UserAddress userAddress);
    UserAddress getById(int id);
}
