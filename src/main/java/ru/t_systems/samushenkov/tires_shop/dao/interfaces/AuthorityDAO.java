package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Authority;
import ru.t_systems.samushenkov.tires_shop.model.type.AuthorityType;

public interface AuthorityDAO {
    Authority getByName(AuthorityType roleName);
}
