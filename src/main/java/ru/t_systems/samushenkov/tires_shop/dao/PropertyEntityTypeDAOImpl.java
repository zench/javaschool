package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.PropertyEntityTypeDAO;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;

import java.util.List;

@Repository
public class PropertyEntityTypeDAOImpl extends AbstractBaseDAO implements PropertyEntityTypeDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<PropertyEntityType> allEntityTypes() {
        return getSession().createQuery("from PropertyEntityType").list();
    }

    @Override
    public PropertyEntityType getById(int id) {
        return getSession().get(PropertyEntityType.class, id);
    }
}
