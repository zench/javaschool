package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.User;

public interface UserDAO {
    User getUserByUsername(String username);
    User getById(int userId);
    User add(User user);
    void edit(User user);
    void delete(User user);
}
