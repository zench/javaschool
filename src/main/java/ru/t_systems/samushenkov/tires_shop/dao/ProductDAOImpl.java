package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ProductDAO;
import ru.t_systems.samushenkov.tires_shop.model.Product;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ProductDAOImpl extends AbstractBaseDAO implements ProductDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Product> allProducts(int page, int categoryId) {
        return getSession().createQuery("from Product where category.id = :code").setParameter("code", categoryId).setFirstResult(10 * (page - 1)).setMaxResults(10).list();
    }

    @Override
    public Product add(Product product) {
        LocalDateTime dateTime = LocalDateTime.now();
        product.setCreated(dateTime);
        product.setModified(dateTime);
        getSession().persist(product);
        return product;
    }

    @Override
    public void delete(Product product) {
        getSession().delete(product);
    }

    @Override
    public void edit(Product product) {
        LocalDateTime dateTime = LocalDateTime.now();
        Product dbProd = getById(product.getId());
        dbProd.setModified(dateTime);
        dbProd.setName(product.getName());
        dbProd.setCategory(product.getCategory());
        dbProd.setProducerCountry(product.getProducerCountry());
        dbProd.setPhoto(product.getPhoto());
        getSession().update(dbProd);
    }

    @Override
    public Product getById(int id) {
        return getSession().get(Product.class, id);
    }

    @Override
    public int productsCount(int categoryId) {
        return getSession().createQuery("select count(*) from Product where category.id = :code", Number.class).setParameter("code", categoryId).getSingleResult().intValue();
    }
}
