package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;

public interface ModificationPropertyDAO {
    ModificationProperty add(ModificationProperty modificationProperty);
    void delete(ModificationProperty modificationProperty);
    void edit(ModificationProperty modificationProperty);
    ModificationProperty getById(int id);
}
