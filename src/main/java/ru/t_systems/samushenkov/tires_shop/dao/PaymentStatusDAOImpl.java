package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.model.PaymentStatus;

import java.util.List;

@Repository
public class PaymentStatusDAOImpl extends AbstractBaseDAO {
    @SuppressWarnings("unchecked")
    public List<PaymentStatus> allPaymentStatuses() {
        return getSession().createQuery("from PaymentStatus ").list();
    }

    public PaymentStatus getById(int id) {
        return getSession().get(PaymentStatus.class, id);
    }
}
