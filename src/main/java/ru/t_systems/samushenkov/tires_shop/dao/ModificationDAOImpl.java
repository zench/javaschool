package ru.t_systems.samushenkov.tires_shop.dao;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.ModificationDAO;
import ru.t_systems.samushenkov.tires_shop.dto.TiresFilterDTO;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEnumValue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Repository
public class ModificationDAOImpl extends AbstractBaseDAO implements ModificationDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Modification> productModifications(int productId) {
        return getSession().createQuery("from Modification where product.id = :code").setParameter("code", productId).list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Modification> allModifications(int page, int categoryId, TiresFilterDTO tiresFilterDTO) {
        String q = getAllModificationsQueryString("SELECT mod", tiresFilterDTO);
        Query query = getSession().createQuery(q, Modification.class);
        query = getAllModificationsQuery(query, categoryId, tiresFilterDTO);

        if(page > 0) {
            query = query.setFirstResult(12 * (page - 1)).setMaxResults(12);
        }
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Modification> topModifications() {
        return getSession().createQuery("from Modification order by sort desc").setMaxResults(Constants.TOP_MODIFICATIONS_COUNT).list();
    }

    @Override
    public List<PropertyEnumValue> getStockBrands(int categoryId) {
        return getStockPropertyVariants(categoryId, Constants.BRAND_PROPERTY_ID);
    }

    @Override
    public List<PropertyEnumValue> getStockSeasons(int categoryId) {
        return getStockPropertyVariants(categoryId, Constants.SEASON_PROPERTY_ID);
    }

    @Override
    public Modification add(Modification modification) {
        LocalDateTime dateTime = LocalDateTime.now();
        modification.setCreated(dateTime);
        modification.setModified(dateTime);
        getSession().persist(modification);
        return modification;
    }

    @Override
    public void delete(Modification modification) {
        getSession().delete(modification);
    }

    @Override
    public void edit(Modification modification) {
        LocalDateTime dateTime = LocalDateTime.now();
        Modification dbModification = getById(modification.getId());
        dbModification.setModified(modification.getModified());
        dbModification.setPrice(modification.getPrice());
        dbModification.setProduct(modification.getProduct());
        dbModification.setStockBalance(modification.getStockBalance());
        dbModification.setModified(dateTime);
        dbModification.setSort(modification.getSort());
        getSession().update(dbModification);
    }

    @Override
    public Modification getById(int id) {
        return getSession().get(Modification.class, id);
    }

    @Override
    public int allModificationsCount(int categoryId, TiresFilterDTO tiresFilterDTO) {
        String q = getAllModificationsQueryString("SELECT COUNT(*)", tiresFilterDTO);
        Query query = getSession().createQuery(q, Number.class);
        query = getAllModificationsQuery(query, categoryId, tiresFilterDTO);
        return Integer.parseInt(query.getSingleResult().toString());
    }

    @Override
    public BigDecimal getMaxPrice(int categoryId) {
        String q = "SELECT max(price) FROM Modification WHERE product.category.id = :code AND stockBalance > 0";
        Query query = getSession().createQuery(q, BigDecimal.class);
        query.setParameter("code", categoryId);
        String result = query.getSingleResult().toString();
        return new BigDecimal(result);
    }

    @Override
    public BigDecimal getMinPrice(int categoryId) {
        String q = "SELECT min(price) FROM Modification WHERE product.category.id = :code  AND stockBalance > 0";
        Query query = getSession().createQuery(q, BigDecimal.class);
        query.setParameter("code", categoryId);
        String result = query.getSingleResult().toString();
        return new BigDecimal(result);
    }

    private List<PropertyEnumValue> getStockPropertyVariants(int categoryId, int propertyId) {
        TiresFilterDTO tiresFilterDTO = new TiresFilterDTO();
        List<Modification> modificationList = allModifications(0, categoryId, tiresFilterDTO);
        List<PropertyEnumValue> stockPropertyVariants = new ArrayList<>();
        for(Modification modification: modificationList) {
            List<ProductProperty> productPropertyList = modification.getProduct().getProductProperties();
            for (ProductProperty productProperty: productPropertyList) {
                if(productProperty.getProperty().getId() == propertyId) {
                    int seasonValue = Integer.parseInt(productProperty.getValue());
                    PropertyEnumValue propertyVariant = productProperty.getProperty().getPropertyEnumValues().stream()
                            .filter(propertyEnumValue -> seasonValue == propertyEnumValue.getId())
                            .findFirst()
                            .orElse(null);
                    if((propertyVariant != null) && (!stockPropertyVariants.contains(propertyVariant))) {
                        stockPropertyVariants.add(propertyVariant);
                    }
                }
            }
        }
        return stockPropertyVariants;
    }

    private String getAllModificationsQueryString(String queryStart, TiresFilterDTO tiresFilterDTO) {
        String q = queryStart + " FROM Modification mod LEFT JOIN Product pr ON mod.product.id = pr.id";

        if(tiresFilterDTO.getSeason() != null) {
            q += " LEFT JOIN ProductProperty prpr ON pr.id = prpr.product.id";
        }

        if(tiresFilterDTO.getSpike() != null) {
            q += " LEFT JOIN ProductProperty prpr2 ON pr.id = prpr2.product.id";
        }

        if(tiresFilterDTO.getBrand() != null) {
            q += " LEFT JOIN ProductProperty prpr3 ON pr.id = prpr3.product.id";
        }

        q += " where mod.stockBalance > 0 and pr.category.id = :code";

        if(tiresFilterDTO.getSeason() != null) {
            q += " and prpr.value = :code2 and prpr.property.id = :seasonPropertyId";
        }
        if(tiresFilterDTO.getSpike() != null) {
            q += " and prpr2.value = :code3 and prpr2.property.id = :spikePropertyId";
        }
        if(tiresFilterDTO.getBrand() != null) {
            q += " and prpr3.value IN :code4 and prpr3.property.id = :brandPropertyId";
        }
        if((tiresFilterDTO.getPriceFrom() != null) && (!tiresFilterDTO.getPriceFrom().equals(""))) {
            q += " and mod.price >= :priceFrom";
        }
        if((tiresFilterDTO.getPriceTo() != null) && (!tiresFilterDTO.getPriceTo().equals(""))) {
            q += " and mod.price <= :priceTo";
        }

        q += " ORDER BY";

        if((tiresFilterDTO.getOrderBy() != null) && (!tiresFilterDTO.getOrderBy().equals(""))) {
            switch (tiresFilterDTO.getOrderBy()) {
                case "price_desc":
                    q += " mod.price DESC";
                    break;
                case "name_asc":
                    q += " pr.name ASC";
                    break;
                case "name_desc":
                    q += " pr.name DESC";
                    break;
                default:
                    q += " mod.price ASC";
                    break;
            }
        } else {
            q += " mod.price ASC";
        }

        return q;
    }

    Query getAllModificationsQuery(Query query, int categoryId, TiresFilterDTO tiresFilterDTO) {
        query.setParameter("code", categoryId);
        if(tiresFilterDTO.getSeason() != null) {
            query.setParameter("code2", tiresFilterDTO.getSeason());
            query.setParameter("seasonPropertyId", Constants.SEASON_PROPERTY_ID);
        }
        if(tiresFilterDTO.getSpike() != null) {
            query.setParameter("code3", tiresFilterDTO.getSpike());
            query.setParameter("spikePropertyId", Constants.SPIKE_PROPERTY_ID);
        }
        if(tiresFilterDTO.getBrand() != null) {
            query.setParameterList("code4", tiresFilterDTO.getBrand());
            query.setParameter("brandPropertyId", Constants.BRAND_PROPERTY_ID);
        }
        if((tiresFilterDTO.getPriceFrom() != null) && (!tiresFilterDTO.getPriceFrom().equals(""))) {
            BigDecimal priceFromBD = new BigDecimal(tiresFilterDTO.getPriceFrom());
            query.setParameter("priceFrom", priceFromBD);
        }
        if((tiresFilterDTO.getPriceTo() != null) && (!tiresFilterDTO.getPriceTo().equals(""))) {
            BigDecimal priceToBD = new BigDecimal(tiresFilterDTO.getPriceTo());
            query.setParameter("priceTo", priceToBD);
        }
        return query;
    }
}
