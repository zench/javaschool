package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.User;

import java.util.List;

public interface OrderDAO {
    List<Order> getAllUserOrders(User user);
    List<Order> getAllOrders(int page);
    Order getLastUserOrder(User user);
    Order add(Order order);
    void edit(Order order);
    Order getById(int id);
    int ordersCount();
}
