package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.dto.TiresFilterDTO;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEnumValue;

import java.math.BigDecimal;
import java.util.List;

public interface ModificationDAO {
    List<Modification> allModifications(int page, int categoryId, TiresFilterDTO tiresFilterDTO);
    List<Modification> productModifications(int productId);
    Modification add(Modification modification);
    void delete(Modification modification);
    void edit(Modification modification);
    int allModificationsCount(int categoryId, TiresFilterDTO tiresFilterDTO);
    Modification getById(int id);
    List<PropertyEnumValue> getStockBrands(int categoryId);
    List<PropertyEnumValue> getStockSeasons(int categoryId);
    BigDecimal getMaxPrice(int categoryId);
    BigDecimal getMinPrice(int categoryId);
    List<Modification> topModifications();
}
