package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.AuthorityDAO;
import ru.t_systems.samushenkov.tires_shop.model.Authority;
import ru.t_systems.samushenkov.tires_shop.model.type.AuthorityType;

@Repository
public class AuthorityDAOImpl extends AbstractBaseDAO implements AuthorityDAO {

    @Override
    public Authority getByName(AuthorityType roleName) {
        return getSession().createQuery("FROM Authority a where a.name=:rolename", Authority.class).setParameter("rolename", roleName).uniqueResult();
    }
}
