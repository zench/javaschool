package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Product;

import java.util.List;

public interface ProductDAO {
    List<Product> allProducts(int page, int categoryId);
    Product add(Product product);
    void delete(Product product);
    void edit(Product product);
    Product getById(int id);
    int productsCount(int categoryId);
}
