package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.OrderStatisticsDAO;
import ru.t_systems.samushenkov.tires_shop.dto.ClientStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.OrderStatistics;
import ru.t_systems.samushenkov.tires_shop.model.User;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Repository
public class OrderStatisticsDAOImpl extends AbstractBaseDAO implements OrderStatisticsDAO {
    private static final String DATE_FROM = "fromDate";
    private static final String DATE_TO = "toDate";

    @Override
    public List<OrderStatistics> getAllUserItems(User user) {
        return Collections.emptyList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<OrderStatistics> getAllOrderItems(Order order) {
        return getSession().
                createQuery("from OrderStatistics where order.id = :code")
                .setParameter("code", order.getId()).list();
    }

    @Override
    public List<OrderStatistics> getAllItems(LocalDateTime from, LocalDateTime to) {
        return Collections.emptyList();
    }

    @Override
    public Object getTotalAmount(LocalDateTime fromDate, LocalDateTime toDate) {
        return getSession().
                createQuery("select sum(total) from OrderStatistics where created >= :fromDate and created <= :toDate")
                .setParameter(DATE_FROM, fromDate)
                .setParameter(DATE_TO, toDate)
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ClientStatisticsDTO> getAggregatedClients(LocalDateTime fromDate, LocalDateTime toDate) {
        return getSession()
                .createQuery("select new ru.t_systems.samushenkov.tires_shop.dto.ClientStatisticsDTO(sum(os.total), sum(os.count), min(os.userId)) from OrderStatistics os where os.created >= :fromDate and os.created <= :toDate group by (os.userId) ORDER BY sum(os.total) desc", ClientStatisticsDTO.class)
                .setParameter(DATE_FROM, fromDate)
                .setParameter(DATE_TO, toDate)
                .setMaxResults(10)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductStatisticsDTO> getAggregatedProducts(LocalDateTime fromDate, LocalDateTime toDate) {
        return getSession()
                .createQuery("select new ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO(sum(os.total), sum(os.count), min(os.modification.id)) from OrderStatistics os where os.created >= :fromDate and os.created <= :toDate group by (os.modification.id) ORDER BY sum(os.total) desc", ProductStatisticsDTO.class)
                .setParameter(DATE_FROM, fromDate)
                .setParameter(DATE_TO, toDate)
                .setMaxResults(10)
                .getResultList();
    }

    @Override
    public OrderStatistics add(OrderStatistics statistics) {
        LocalDateTime dateTime = LocalDateTime.now();
        statistics.setCreated(dateTime);
        getSession().persist(statistics);
        return statistics;
    }
}
