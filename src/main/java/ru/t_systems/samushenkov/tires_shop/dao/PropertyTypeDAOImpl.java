package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.PropertyTypeDAO;
import ru.t_systems.samushenkov.tires_shop.model.PropertyType;

import java.util.List;

@Repository
public class PropertyTypeDAOImpl extends AbstractBaseDAO implements PropertyTypeDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<PropertyType> allTypes() {
        return getSession().createQuery("from PropertyType").list();
    }

    @Override
    public PropertyType getById(int id) {
        return getSession().get(PropertyType.class, id);
    }
}
