package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UserDAO;
import ru.t_systems.samushenkov.tires_shop.model.User;

import java.time.LocalDateTime;

@Repository
public class UserDAOImpl extends AbstractBaseDAO implements UserDAO {

    @Override
    public User getUserByUsername(String username) {
        return getSession().createQuery("FROM User u where u.email=:username", User.class).setParameter("username", username).uniqueResult();
    }

    @Override
    public User getById(int id) {
        return getSession().get(User.class, id);
    }

    @Override
    public User add(User user) {
        LocalDateTime dateTime = LocalDateTime.now();
        user.setCreated(dateTime);
        user.setModified(dateTime);
        getSession().persist(user);
        return user;
    }

    @Override
    public void edit(User user) {
        LocalDateTime dateTime = LocalDateTime.now();
        User dbUser = getById(user.getId());
        dbUser.setModified(dateTime);
        dbUser.setName(user.getName());
        dbUser.setLastName(user.getLastName());
        dbUser.setBirthday(user.getBirthday());
        if(user.getPassword() != null) {
            dbUser.setPassword(user.getPassword());
        }
        dbUser.setEmail(user.getEmail());
        getSession().update(dbUser);
    }

    @Override
    public void delete(User user) {
        getSession().delete(user);
    }
}
