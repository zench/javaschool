package ru.t_systems.samushenkov.tires_shop.dao;

import org.springframework.stereotype.Repository;
import ru.t_systems.samushenkov.tires_shop.dao.interfaces.UserAddressDAO;
import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.model.UserAddress;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class UserAddressDAOImpl extends AbstractBaseDAO implements UserAddressDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<UserAddress> getAllUserAddresses(User user) {
        return getSession().createQuery("from UserAddress where user = :code")
                .setParameter("code", user)
                .getResultList();
    }

    @Override
    public UserAddress add(UserAddress userAddress) {
        LocalDateTime dateTime = LocalDateTime.now();
        userAddress.setCreated(dateTime);
        getSession().persist(userAddress);
        return userAddress;
    }

    @Override
    public void edit(UserAddress userAddress) {
        LocalDateTime dateTime = LocalDateTime.now();
        UserAddress dbUAddr = getById(userAddress.getId());
        dbUAddr.setModified(dateTime);
        dbUAddr.setCountry(userAddress.getCountry());
        dbUAddr.setCity(userAddress.getCity());
        dbUAddr.setStreet(userAddress.getStreet());
        dbUAddr.setHouse(userAddress.getHouse());
        dbUAddr.setFlat(userAddress.getFlat());
        dbUAddr.setZip(userAddress.getZip());
        getSession().update(dbUAddr);
    }

    @Override
    public UserAddress getById(int id) {
        return getSession().get(UserAddress.class, id);
    }
}
