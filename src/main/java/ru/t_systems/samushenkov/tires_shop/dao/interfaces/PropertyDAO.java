package ru.t_systems.samushenkov.tires_shop.dao.interfaces;

import ru.t_systems.samushenkov.tires_shop.model.Property;

import java.util.List;

public interface PropertyDAO {
    List<Property> allProperties(int categoryId, int entityTypeId);
    Property add(Property property);
    void delete(Property property);
    void edit(Property property);
    Property getById(int id);
}
