package ru.t_systems.samushenkov.tires_shop.config;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "ru.t_systems.samushenkov.tires_shop")
@EnableTransactionManagement
@PropertySource(value = "classpath:db.test.properties")
public class AHibernateTestConfig extends AbstractHibConfig {

    @Bean
    @Override
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.testDriverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.testUrl"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.testUsername"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.testPassword"));
        return dataSource;
    }
}
