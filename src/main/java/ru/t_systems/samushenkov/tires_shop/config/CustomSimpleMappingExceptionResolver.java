package ru.t_systems.samushenkov.tires_shop.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.ws.rs.InternalServerErrorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

public class CustomSimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {

    private static final Logger log = LoggerFactory.getLogger(CustomSimpleMappingExceptionResolver.class);
    private static final String NOT_FOUND_TITLE = "404 - Такой страницы не существует";
    private static final String SERVER_ERROR_TITLE = "500 - Что-то пошло не так.... Но мы уже все знаем и исправляем";
    private static final String TITLE = "title";
    private static final String STATUS = "status";
    private static final String PAGE_500 = "/error/500";
    private static final String PAGE_404 = "/error/404";
    public CustomSimpleMappingExceptionResolver() {
        // Turn logging on by default
        setWarnLogCategory(getClass().getName());
    }

    @Override
    public String buildLogMessage(Exception e, HttpServletRequest req) {
        return "MVC exception: " + e.getLocalizedMessage();
    }

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response,
                                              Object handler, Exception ex) {

        // Log exception
        log.error(ex.getMessage());
        String exceptionType = ex.getClass().getCanonicalName();

        // Get the ModelAndView to use
        ModelAndView modelAndView = super.doResolveException(request, response, handler, ex);

        // Make more information available to the view - note that SimpleMappingExceptionResolver adds the exception already
        modelAndView.addObject("url", request.getRequestURL());
        modelAndView.addObject("timestamp", new Date());

        ArrayList<String> exceptions404 = new ArrayList<>(
                Arrays.asList(
                        NoHandlerFoundException.class.getName()
                )
        );
        ArrayList<String> exceptions500 = new ArrayList<>(
                Arrays.asList(
                        InternalServerErrorException.class.getName(),
                        NullPointerException.class.getName()
                )
        );

        String userExceptionDetail = "";
        String errorHuman = "";
        String errorTech = "";

        if (exceptions404.contains(exceptionType)) {
            errorHuman = "We cannot find the page you are looking for";
            errorTech = "Page not found";
            userExceptionDetail = String.format("The page %s cannot be found", request.getRequestURL());
            modelAndView.setViewName(PAGE_404);
            modelAndView.addObject(TITLE, NOT_FOUND_TITLE);
            modelAndView.addObject(STATUS, HttpStatus.NOT_FOUND.value());
        } else if (exceptions500.contains(exceptionType)) {
            errorHuman = "We cannot currently serve the page you request";
            errorTech = "Internal error";
            userExceptionDetail = "The current page refuses to load due to an internal error";
            modelAndView.setViewName(PAGE_500);
            modelAndView.addObject(TITLE, SERVER_ERROR_TITLE);
            modelAndView.addObject(STATUS, HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else {
            errorHuman = "We cannot serve the current page";
            errorTech = "General error";
            userExceptionDetail = "A generic error prevents from serving the page";
            modelAndView.setViewName("/error/generic");
            modelAndView.addObject(TITLE, SERVER_ERROR_TITLE);
            modelAndView.addObject(STATUS, response.getStatus());
        }

        Exception userException = new Exception(userExceptionDetail);
        modelAndView.addObject("error_human", errorHuman);
        modelAndView.addObject("error_tech", errorTech);
        modelAndView.addObject("exception", userException);
        return modelAndView;
    }
}
