package ru.t_systems.samushenkov.tires_shop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import ru.t_systems.samushenkov.tires_shop.model.TireUserDetails;
import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

@Configuration
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    CartService cartService;

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        HttpSession session = httpServletRequest.getSession();
        if (!(authentication.getPrincipal() instanceof TireUserDetails)) {
            throw new  IOException("Principal can not be null!");
        }
        User loggedInUser = ((TireUserDetails) authentication.getPrincipal()).getUserDetails();
        session.setAttribute("userId", loggedInUser.getId());
        session.setAttribute("userName", loggedInUser.getName());
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());

        if (roles.contains("ROLE_ADMIN")) {
            httpServletResponse.sendRedirect("/admin/categories");
        } else if(roles.contains("ROLE_USER")){
            httpServletResponse.sendRedirect("/user/account");
        } else {
            httpServletResponse.sendRedirect(getDefaultTargetUrl());
        }
        cartService.setCartUser(session);
    }

    public CustomAuthenticationSuccessHandler() {
        super();
        setUseReferer(true);
    }

}
