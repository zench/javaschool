package ru.t_systems.samushenkov.tires_shop.dto;

public class BaseDTO {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
