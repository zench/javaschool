package ru.t_systems.samushenkov.tires_shop.dto;

public class CategoryDTO extends BaseDatesDTO {
    private String name;

    private Integer parentCategoryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }
}
