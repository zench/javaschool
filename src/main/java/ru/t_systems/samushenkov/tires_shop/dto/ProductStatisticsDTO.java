package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.model.Modification;

import java.math.BigDecimal;

public class ProductStatisticsDTO implements BaseStatisticsDTO {
    private Modification modification;
    private BigDecimal totalIncome;
    private Long totalCount;

    public ProductStatisticsDTO(BigDecimal totalIncome, Long totalCount, int modificationId) {
        this.totalCount = totalCount;
        this.totalIncome = totalIncome;
        this.modification = new Modification();
        this.modification.setId(modificationId);
    }

    public Modification getModification() {
        return modification;
    }

    public void setModification(Modification modification) {
        this.modification = modification;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
