package ru.t_systems.samushenkov.tires_shop.dto;

import java.time.LocalDateTime;

public class BaseDatesDTO extends BaseDTO {
    private LocalDateTime modified;

    private LocalDateTime created;

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }
}
