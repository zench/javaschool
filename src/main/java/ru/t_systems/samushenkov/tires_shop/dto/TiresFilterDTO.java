package ru.t_systems.samushenkov.tires_shop.dto;

public class TiresFilterDTO {
    String[] brand;
    String season;
    String spike;
    String priceFrom;
    String priceTo;
    String orderBy;

    public String[] getBrand() {
        return brand;
    }

    public void setBrand(String[] brand) {
        this.brand = brand;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getSpike() {
        return spike;
    }

    public void setSpike(String spike) {
        this.spike = ((spike == null) || spike.isEmpty()) ? null : spike;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
    }

    public String getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(String priceTo) {
        this.priceTo = priceTo;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
