package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.model.CartItem;
import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class SavedCartItemDTO {

    private int id;
    private int modificationId;
    private BigDecimal price;
    private String name;
    private String photoUrl;
    private int count;
    private Map<String, String> properties = new HashMap<>();

    public SavedCartItemDTO(CartItem cartItem) {
        this.id = cartItem.getId();
        this.modificationId = cartItem.getModification().getId();
        this.price = cartItem.getModification().getPrice();
        this.name = cartItem.getModification().getProduct().getName();
        this.count = cartItem.getCount();
        this.photoUrl = cartItem.getModification().getProduct().getPhoto();
        for (ModificationProperty modProperty: cartItem.getModification().getModificationProperties()) {

            if((modProperty.getProperty().getId() == Constants.WIDTH_PROPERTY_ID) ||
                    (modProperty.getProperty().getId() == Constants.PROFILE_PROPERTY_ID) ||
                    (modProperty.getProperty().getId() == Constants.DIAMETER_PROPERTY_ID)){
                this.properties.put(modProperty.getProperty().getName(), modProperty.getValue());
            }
        }
    }

    public SavedCartItemDTO(int id, int modificationId, BigDecimal price, String name, String photoUrl, int count, Map<String, String> properties) {
        this.id = id;
        this.modificationId = modificationId;
        this.price = price;
        this.name = name;
        this.photoUrl = photoUrl;
        this.count = count;
        this.properties = properties;
    }

    public SavedCartItemDTO() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModificationId() {
        return modificationId;
    }

    public void setModificationId(int modificationId) {
        this.modificationId = modificationId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
