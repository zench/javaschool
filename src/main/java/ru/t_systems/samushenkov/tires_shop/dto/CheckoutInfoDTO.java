package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.model.DeliveryType;
import ru.t_systems.samushenkov.tires_shop.model.PaymentType;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CheckoutInfoDTO {

    @NotNull
    @Size(min = 2, message = "{Size.userDTO.name}")
    private String name;

    @NotNull
    @Size(min = 2, message = "{Size.userDTO.lastName}")
    private String lastName;

    @Valid
    private UserAddressDTO userAddress;

    @NotNull
    @Size(min = 7, message = "{Size.checkoutInfoDTO.phone}")
    private String phone;

    @NotNull(message = "{NotNull.checkoutInfoDTO.paymentType}")
    private PaymentType paymentType;

    @NotNull(message = "{NotNull.checkoutInfoDTO.deliveryType}")
    private DeliveryType deliveryType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserAddressDTO getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(UserAddressDTO userAddress) {
        this.userAddress = userAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }
}
