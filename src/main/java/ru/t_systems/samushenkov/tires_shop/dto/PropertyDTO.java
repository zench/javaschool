package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEnumValue;
import ru.t_systems.samushenkov.tires_shop.model.PropertyType;
import ru.t_systems.samushenkov.tires_shop.model.Unit;

import java.util.List;

public class PropertyDTO extends BaseDatesDTO {

    private String name;

    private Integer categoryId;

    private PropertyType type;

    private Unit unit;

    private List<PropertyEnumValue> propertyEnumValues;

    private PropertyEntityType entityType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<PropertyEnumValue> getPropertyEnumValues() {
        return propertyEnumValues;
    }

    public void setPropertyEnumValues(List<PropertyEnumValue> propertyEnumValues) {
        this.propertyEnumValues = propertyEnumValues;
    }

    public PropertyEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(PropertyEntityType entityType) {
        this.entityType = entityType;
    }
}
