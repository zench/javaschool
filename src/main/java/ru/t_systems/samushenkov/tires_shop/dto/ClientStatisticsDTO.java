package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.model.User;

import java.math.BigDecimal;

public class ClientStatisticsDTO implements BaseStatisticsDTO {
    private User user;
    private BigDecimal totalIncome;
    private Long totalCount;
    private int userId;

    public ClientStatisticsDTO(BigDecimal totalIncome, Long totalCount, int userId) {
        this.totalCount = totalCount;
        this.totalIncome = totalIncome;
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
