package ru.t_systems.samushenkov.tires_shop.dto;

import java.math.BigDecimal;

public class IncomeStatisticsDTO implements BaseStatisticsDTO {
    private BigDecimal totalIncome;

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }
}
