package ru.t_systems.samushenkov.tires_shop.dto;

import org.springframework.web.multipart.MultipartFile;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;

import java.util.List;

public class ProductDTO extends BaseDatesDTO {

    private String name;

    private String photo;

    private MultipartFile image;

    private Country producerCountry;

    private Category category;

    private List<ProductProperty> productProperties;

    private List<Modification> modificationList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Country getProducerCountry() {
        return producerCountry;
    }

    public void setProducerCountry(Country producerCountry) {
        this.producerCountry = producerCountry;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<ProductProperty> getProductProperties() {
        return productProperties;
    }

    public void setProductProperties(List<ProductProperty> productProperties) {
        this.productProperties = productProperties;
    }

    public List<Modification> getModificationList() {
        return modificationList;
    }

    public void setModificationList(List<Modification> modificationList) {
        this.modificationList = modificationList;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }
}
