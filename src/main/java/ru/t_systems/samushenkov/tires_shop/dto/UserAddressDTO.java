package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.model.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserAddressDTO extends BaseDatesDTO {

    @NotNull(message = "{NotNull.UserAddressDTO.country}")
    private Country country;

    @Size(min=3, message="{Size.UserAddressDTO.city}")
    private String city;

    @Size(min=5, message="{Size.UserAddressDTO.street}")
    private String street;

    @Size(min=1, message="{Size.UserAddressDTO.house}")
    private String house;

    private String flat;

    @Size(min=6, max=6, message="{Size.UserAddressDTO.zip}")
    private String zip;

    private User user;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
