package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;
import ru.t_systems.samushenkov.tires_shop.model.Product;

import java.math.BigDecimal;
import java.util.List;

public class ModificationDTO extends BaseDatesDTO {
    private BigDecimal price;

    private int sort;

    private Product product;

    private int stockBalance;

    private List<ModificationProperty> modificationProperties;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getStockBalance() {
        return stockBalance;
    }

    public void setStockBalance(int stockBalance) {
        this.stockBalance = stockBalance;
    }

    public List<ModificationProperty> getModificationProperties() {
        return modificationProperties;
    }

    public void setModificationProperties(List<ModificationProperty> modificationProperties) {
        this.modificationProperties = modificationProperties;
    }
}
