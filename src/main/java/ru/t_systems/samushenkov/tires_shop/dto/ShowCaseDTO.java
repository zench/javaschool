package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.ModificationProperty;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEnumValue;

import java.math.BigDecimal;
import java.util.List;

public class ShowCaseDTO {
    private String url;
    private String photoUrl;
    private String name;
    private BigDecimal price;

    public ShowCaseDTO(Modification modification) {
        this.url = "/shop/"+modification.getId();
        this.photoUrl = modification.getProduct().getPhoto();
        StringBuilder bld = new StringBuilder();
        if(!modification.getProduct().getProductProperties().isEmpty()) {
            bld.append(getBrand(modification.getProduct().getProductProperties()));
            bld.append(" ");
        }
        bld.append(modification.getProduct().getName());
        bld.append(" ");
        if(!modification.getModificationProperties().isEmpty()) {
            for (ModificationProperty mp: modification.getModificationProperties()) {
                if(mp.getProperty().getId() == Constants.WIDTH_PROPERTY_ID) {
                    bld.append(mp.getValue());
                    bld.append("/");
                }
                if(mp.getProperty().getId() == Constants.PROFILE_PROPERTY_ID) {
                    bld.append(mp.getValue());
                    bld.append("/");
                }
                if(mp.getProperty().getId() == Constants.DIAMETER_PROPERTY_ID) {
                    bld.append("R");
                    bld.append(mp.getValue());
                }
            }
        }
        this.name = bld.toString();
        this.price = modification.getPrice();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    private String getBrand(List<ProductProperty> productProperties) {
        for (ProductProperty pr: productProperties) {
            if(pr.getProperty().getId() == Constants.BRAND_PROPERTY_ID) {
                for (PropertyEnumValue pev: pr.getProperty().getPropertyEnumValues()) {
                    if(pev.getId() == Integer.parseInt(pr.getValue())) {
                        return pev.getValue();
                    }
                }
            }
        }
        return "";
    }
}
