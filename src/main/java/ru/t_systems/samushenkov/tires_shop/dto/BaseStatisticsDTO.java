package ru.t_systems.samushenkov.tires_shop.dto;

import java.math.BigDecimal;

public interface BaseStatisticsDTO {
    public BigDecimal getTotalIncome();
    public void setTotalIncome(BigDecimal totalIncome);
}
