package ru.t_systems.samushenkov.tires_shop.dto;

import ru.t_systems.samushenkov.tires_shop.validation.ValidEmail;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class UserNoPasswordDTO extends BaseDatesDTO {
    @NotNull
    @Size(min = 2, message = "{Size.userDTO.name}")
    private String name;

    @NotNull
    @Size(min = 2, message = "{Size.userDTO.lastName}")
    private String lastName;

    @ValidEmail(message = "{NotValid.userDTO.email}")
    @NotNull
    private String email;

    private LocalDate birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        if((birthday == null) || (birthday.isEmpty())) {
            this.birthday = null;
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            formatter = formatter.withLocale(Locale.US);
            LocalDate bd = LocalDate.parse(birthday, formatter);
            this.birthday = bd;
        }
    }
}
