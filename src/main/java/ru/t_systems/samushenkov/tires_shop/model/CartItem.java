package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;

@Entity
@Table(name = "cart")
public class CartItem extends BaseDatesEntity {

    @Column(name = "sessionId")
    private String sessionId;

    @Column(name = "count")
    private int count;

    @ManyToOne
    @JoinColumn(name = "modificationId", nullable = false)
    private Modification modification;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = true)
    private User user;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Modification getModification() {
        return modification;
    }

    public void setModification(Modification modification) {
        this.modification = modification;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
