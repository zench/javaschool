package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;

@Entity
@Table(name = "categories")
public class Category extends BaseDatesEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "parentCategoryId")
    private Integer parentCategoryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }
}
