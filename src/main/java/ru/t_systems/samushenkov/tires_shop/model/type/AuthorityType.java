package ru.t_systems.samushenkov.tires_shop.model.type;

public enum AuthorityType {
    ROLE_ADMIN,
    ROLE_USER
}
