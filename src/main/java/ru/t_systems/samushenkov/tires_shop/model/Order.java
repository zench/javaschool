package ru.t_systems.samushenkov.tires_shop.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.t_systems.samushenkov.tires_shop.dto.CheckoutInfoDTO;
import ru.t_systems.samushenkov.tires_shop.dto.SavedCartItemDTO;

import javax.persistence.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "user_orders")
public class Order extends BaseDatesEntity {

    @Column(name = "content")
    private String content;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "userAddressId", nullable = false)
    private UserAddress userAddress;

    @ManyToOne
    @JoinColumn(name = "statusId", nullable = false)
    private OrderStatus status;

    @ManyToOne
    @JoinColumn(name = "paymentTypeId", nullable = false)
    private PaymentType paymentType;

    @ManyToOne
    @JoinColumn(name = "deliveryTypeId", nullable = false)
    private DeliveryType deliveryType;

    @ManyToOne
    @JoinColumn(name = "paymentStatusId", nullable = false)
    private PaymentStatus paymentStatus;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private List<OrderStatistics> orderStatistics;

    @Column(name = "total")
    private BigDecimal total;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserAddress getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(UserAddress userAddress) {
        this.userAddress = userAddress;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public List<OrderStatistics> getOrderStatistics() {
        return orderStatistics;
    }

    public void setOrderStatistics(List<OrderStatistics> orderStatistics) {
        this.orderStatistics = orderStatistics;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public List<SavedCartItemDTO> getContentItems() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<List<SavedCartItemDTO>> mapType = new TypeReference<List<SavedCartItemDTO>>() {};
        return objectMapper.readValue(getContent(), mapType);
    }

    public Order() {

    }

    public Order(CheckoutInfoDTO checkoutInfoDTO) {
        setName(checkoutInfoDTO.getName());
        setLastName(checkoutInfoDTO.getLastName());
        setPhone(checkoutInfoDTO.getPhone());
    }
}
