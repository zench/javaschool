package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;

@Entity
@Table(name = "product_property_values")
public class ProductProperty extends BaseDatesEntity {
    @Column(name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name = "propertyId")
    private Property property;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
