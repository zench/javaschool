package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "order_statistics")
public class OrderStatistics extends BaseDatesEntity {

    public OrderStatistics(CartItem cartItem) {
        this.modification = cartItem.getModification();
        this.price = cartItem.getModification().getPrice();
        this.count = cartItem.getCount();
        this.userId = cartItem.getUser().getId();
        BigDecimal tempTotal = new BigDecimal(0);
        BigDecimal tempCount = new BigDecimal(cartItem.getCount());
        this.total = tempTotal.add(tempCount.multiply(this.price));
    }

    @ManyToOne
    @JoinColumn(name = "orderId")
    private Order order;

    @Column(name = "userId")
    private int userId;

    @ManyToOne
    @JoinColumn(name = "modificationId")
    private Modification modification;

    @Column(name = "price")
    BigDecimal price;

    @Column(name = "count")
    int count;

    @Column(name = "total")
    BigDecimal total;

    public OrderStatistics() {

    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Modification getModification() {
        return modification;
    }

    public void setModification(Modification modification) {
        this.modification = modification;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
