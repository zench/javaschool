package ru.t_systems.samushenkov.tires_shop.model;

import ru.t_systems.samushenkov.tires_shop.model.type.AuthorityType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "authority")
public class Authority extends BaseEntity {
    @Enumerated(EnumType.STRING)
    private AuthorityType name;

    public Authority() {}
    public Authority(AuthorityType name) {
        this.name = name;
    }

    public AuthorityType getName() {
        return name;
    }

    public void setName(AuthorityType name) {
        this.name = name;
    }
}
