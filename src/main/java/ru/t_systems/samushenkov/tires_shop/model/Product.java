package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "products")
public class Product extends BaseDatesEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "photo")
    private String photo;

    @ManyToOne
    @JoinColumn(name = "producerCountryId", nullable = true)
    private Country producerCountry;

    @ManyToOne
    @JoinColumn(name = "categoryId", nullable = false)
    private Category category;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private List<ProductProperty> productProperties;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private List<Modification> modificationList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Country getProducerCountry() {
        return producerCountry;
    }

    public void setProducerCountry(Country producerCountry) {
        this.producerCountry = producerCountry;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<ProductProperty> getProductProperties() {
        return productProperties;
    }

    public void setProductProperties(List<ProductProperty> productProperties) {
        this.productProperties = productProperties;
    }

    public List<Modification> getModificationList() {
        return modificationList;
    }

    public void setModificationList(List<Modification> modificationList) {
        this.modificationList = modificationList;
    }
}
