package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "order_statuses")
public class OrderStatus extends BaseEntity {
    public static final int CREATED = 1;

    @Column(name = "value")
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
