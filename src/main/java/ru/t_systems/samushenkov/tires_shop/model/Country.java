package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;

@Entity
@Table(name = "countries")
public class Country extends BaseEntity {

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
