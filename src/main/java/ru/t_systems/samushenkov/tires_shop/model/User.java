package ru.t_systems.samushenkov.tires_shop.model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.t_systems.samushenkov.tires_shop.dto.UserDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserNoPasswordDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
public class User extends BaseDatesEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "birthday")
    private LocalDate birthday;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_authority",
            joinColumns = { @JoinColumn(name = "userId") },
            inverseJoinColumns = { @JoinColumn(name = "authorityId") })
    private Set<Authority> authorities = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<CartItem> cartItems;

    public User() {

    }

    public User(UserDTO userDTO, boolean checkPassword) {
        setName(userDTO.getName());
        setLastName(userDTO.getLastName());
        if(checkPassword) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            if(!userDTO.getPassword().equals(userDTO.getMatchingPassword()) || (userDTO.getPassword().isEmpty())) {
                throw new NullPointerException("Passwords doesn't match");
            }
            setPassword(passwordEncoder.encode(userDTO.getPassword()));
        }
        setEmail(userDTO.getEmail());
        setBirthday(userDTO.getBirthday());
    }

    public User(UserNoPasswordDTO userNoPasswordDTO) {
        setName(userNoPasswordDTO.getName());
        setLastName(userNoPasswordDTO.getLastName());
        setEmail(userNoPasswordDTO.getEmail());
        setBirthday(userNoPasswordDTO.getBirthday());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

}
