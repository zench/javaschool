package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "modifications")
public class Modification extends BaseDatesEntity {

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "sort")
    private int sort;

    @ManyToOne
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(name = "stockBalance")
    private int stockBalance;

    @OneToMany(mappedBy = "modification", fetch = FetchType.EAGER)
    private List<ModificationProperty> modificationProperties;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<ModificationProperty> getModificationProperties() {
        return modificationProperties;
    }

    public void setModificationProperties(List<ModificationProperty> modificationProperties) {
        this.modificationProperties = modificationProperties;
    }

    public int getStockBalance() {
        return stockBalance;
    }

    public void setStockBalance(int stockBalance) {
        this.stockBalance = stockBalance;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
