package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "payment_statuses")
public class PaymentStatus extends BaseEntity {

    public static final int NOT_PAYED = 1;
    @Column(name = "value")
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
