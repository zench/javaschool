package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;

@Entity
@Table(name = "property_enum_values")
public class PropertyEnumValue extends BaseDatesEntity {
    @Column(name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name = "propertyId", nullable = false)
    private Property property;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
