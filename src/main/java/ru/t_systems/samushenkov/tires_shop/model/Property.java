package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "properties")
public class Property extends BaseDatesEntity{

    @Column(name = "name")
    private String name;

    @Column(name = "categoryId")
    private Integer categoryId;

    @ManyToOne
    @JoinColumn(name = "typeId", nullable = false)
    private PropertyType type;

    @ManyToOne
    @JoinColumn(name = "unitId", nullable = true)
    private Unit unit;

    @OneToMany(mappedBy = "property", fetch = FetchType.EAGER)
    private List<PropertyEnumValue> propertyEnumValues;

    @ManyToOne
    @JoinColumn(name = "entityTypeId", nullable = false)
    private PropertyEntityType entityType;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public PropertyEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(PropertyEntityType entityType) {
        this.entityType = entityType;
    }

    public List<PropertyEnumValue> getPropertyEnumValues() {
        return propertyEnumValues;
    }

    public void setPropertyEnumValues(List<PropertyEnumValue> propertyEnumValues) {
        this.propertyEnumValues = propertyEnumValues;
    }
}
