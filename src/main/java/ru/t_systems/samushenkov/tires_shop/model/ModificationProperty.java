package ru.t_systems.samushenkov.tires_shop.model;

import javax.persistence.*;

@Entity
@Table(name = "modification_property_values")
public class ModificationProperty extends BaseDatesEntity {

    @Column(name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name = "propertyId")
    private Property property;

    @ManyToOne
    @JoinColumn(name = "modificationId")
    private Modification modification;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public Modification getModification() {
        return modification;
    }

    public void setModification(Modification modification) {
        this.modification = modification;
    }
}
