package ru.t_systems.samushenkov.tires_shop.model;

import java.util.List;

public class Cart {
    List<CartItem> cartItems;

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }
}
