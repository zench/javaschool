-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.19 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица tires.authority
CREATE TABLE IF NOT EXISTS `authority` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.authority: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `authority` DISABLE KEYS */;
INSERT INTO `authority` (`id`, `name`) VALUES
	(1, 'ROLE_ADMIN'),
	(2, 'ROLE_USER');
/*!40000 ALTER TABLE `authority` ENABLE KEYS */;

-- Дамп структуры для таблица tires.cart
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(32) NOT NULL,
  `userId` int(10) DEFAULT NULL,
  `modificationId` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.cart: ~21 rows (приблизительно)
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` (`id`, `sessionId`, `userId`, `modificationId`, `count`, `modified`, `created`) VALUES
	(55, '5F0BD908F8A9713B46C2FBC6D673D1E8', NULL, 3, 2, '2019-10-27 20:04:56', '2019-10-27 20:04:03'),
	(60, '1AEE881BF04A3A1378441E286CA4A105', NULL, 22, 1, '2019-10-29 11:20:27', '2019-10-29 11:20:27'),
	(61, 'CCF8A49E60902A99FEC97D88F9CEFF5C', NULL, 22, 1, '2019-10-29 11:58:21', '2019-10-29 11:58:21'),
	(63, '08667C4E56C50B81CE180A4AD54F77E6', NULL, 22, 1, '2019-10-29 12:09:30', '2019-10-29 12:09:30'),
	(69, '07AF6EDE996A6C987513463311BDFD58', NULL, 22, 1, '2019-10-29 14:29:39', '2019-10-29 14:29:39'),
	(70, '87A2ED34D5F3FA593679712FE72D8AEA', NULL, 21, 1, '2019-10-30 19:22:49', '2019-10-30 19:22:49'),
	(71, '8DBB7E277A8D5E3BB6EC580D9E502AD5', NULL, 21, 1, '2019-10-30 19:26:56', '2019-10-30 19:26:56'),
	(84, '27E22DD7D5C2A28EE70679F8CE03219D', 2, 22, 1, '2019-10-31 23:01:06', '2019-10-31 23:01:06'),
	(94, '938A9812627ACD1595F807FD23029F7A', NULL, 21, 1, '2019-11-03 15:47:20', '2019-11-03 15:47:20'),
	(95, '938A9812627ACD1595F807FD23029F7A', NULL, 3, 2, '2019-11-03 23:19:58', '2019-11-03 15:48:14'),
	(96, '26D3C7D133677FC40AE5C0DF7A4DCD35', NULL, 2, 9, '2019-11-08 14:08:51', '2019-11-03 15:53:22'),
	(97, '617514F45988F8682717E38E5C381E8E', NULL, 21, 1, '2019-11-03 15:57:58', '2019-11-03 15:57:58'),
	(98, 'D5AF8A4A37A8AFAD3C3E2D159FB0ABA2', NULL, 21, 1, '2019-11-03 16:13:18', '2019-11-03 16:13:18'),
	(100, 'E4CE8CDFB4F64A24E27A601DB4FB3C99', NULL, 22, 1, '2019-11-03 16:34:45', '2019-11-03 16:34:45'),
	(104, '69B9D95BF1AD5911E30DB4F106620FCE', NULL, 21, 1, '2019-11-03 23:20:30', '2019-11-03 23:20:30'),
	(111, 'F3D46EB7821EE6EC810F8E53950E798C', NULL, 30, 4, '2019-11-04 12:39:01', '2019-11-04 12:38:38'),
	(113, 'C893CCAC9456069544CD8B57808E92A3', 27, 19, 1, '2019-11-04 12:47:36', '2019-11-04 12:47:36'),
	(114, '21263A09AE963946361C6B8EC0800FE1', NULL, 19, 4, '2019-11-04 15:52:38', '2019-11-04 15:52:22'),
	(135, 'EB1E6EF53ED46CCA043252FBD7D85BBE', NULL, 31, 1, '2019-11-20 23:03:55', '2019-11-20 23:03:55');
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;

-- Дамп структуры для таблица tires.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parentCategoryId` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.categories: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `parentCategoryId`, `modified`, `created`) VALUES
	(1, 'Шины', NULL, '2019-11-20 14:35:34', '2019-10-14 15:50:37'),
	(16, 'Шиномонтаж', NULL, '2019-10-19 17:03:45', '2019-10-12 15:38:27');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Дамп структуры для таблица tires.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.countries: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `modified`, `created`) VALUES
	(1, 'Россия', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(2, 'Германия', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(3, 'Финляндия', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(4, 'Япония', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(5, 'Италия', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(6, 'Южная Корея', '2019-10-10 00:01:01', '2019-10-10 00:01:01');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Дамп структуры для таблица tires.delivery_types
CREATE TABLE IF NOT EXISTS `delivery_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.delivery_types: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `delivery_types` DISABLE KEYS */;
INSERT INTO `delivery_types` (`id`, `value`) VALUES
	(1, 'Самовывоз'),
	(2, 'Курьер'),
	(3, 'DHL'),
	(4, 'Почта России');
/*!40000 ALTER TABLE `delivery_types` ENABLE KEYS */;

-- Дамп структуры для таблица tires.modifications
CREATE TABLE IF NOT EXISTS `modifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `productId` int(10) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stockBalance` int(11) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_modifications_products` (`productId`),
  CONSTRAINT `FK_modifications_products` FOREIGN KEY (`productId`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.modifications: ~24 rows (приблизительно)
/*!40000 ALTER TABLE `modifications` DISABLE KEYS */;
INSERT INTO `modifications` (`id`, `productId`, `price`, `stockBalance`, `sort`, `modified`, `created`) VALUES
	(1, 1, 4060.00, 8, 100, '2019-11-13 10:52:43', '2019-10-11 11:02:59'),
	(2, 1, 3980.00, 13, 110, '2019-11-20 10:03:22', '2019-10-11 11:02:59'),
	(3, 31, 3560.00, 80, 96, '2019-11-12 18:04:40', '2019-10-11 11:02:59'),
	(4, 31, 3990.45, 80, 1, '2019-11-02 17:14:34', '2019-10-13 17:15:59'),
	(11, 44, 1200.00, 1000, 0, '2019-10-19 17:10:10', '2019-10-19 17:10:10'),
	(12, 45, 1700.00, 1000, 0, '2019-10-19 17:25:15', '2019-10-19 17:25:15'),
	(13, 46, 2200.00, 1000, 0, '2019-10-19 17:25:33', '2019-10-19 17:25:33'),
	(14, 47, 2500.00, 1000, 0, '2019-10-19 17:25:48', '2019-10-19 17:25:48'),
	(15, 48, 3000.00, 1000, 0, '2019-10-19 17:26:04', '2019-10-19 17:26:04'),
	(16, 49, 3500.00, 1000, 0, '2019-10-19 17:26:22', '2019-10-19 17:26:22'),
	(17, 50, 4000.00, 1000, 0, '2019-10-19 17:26:37', '2019-10-19 17:26:37'),
	(19, 53, 4070.00, 55, 1, '2019-11-22 13:56:57', '2019-10-23 14:03:56'),
	(20, 53, 5440.00, 96, 101, '2019-11-21 13:16:39', '2019-10-23 14:09:16'),
	(21, 54, 2100.00, 19, 1, '2019-11-19 19:25:38', '2019-10-24 14:15:41'),
	(22, 54, 2380.00, 80, 1, '2019-11-19 13:41:20', '2019-10-24 14:16:40'),
	(23, 55, 7480.00, 44, 1, '2019-11-15 10:37:46', '2019-10-24 14:28:00'),
	(24, 55, 7100.00, 24, 97, '2019-11-12 18:03:19', '2019-10-24 14:28:31'),
	(25, 55, 6400.00, 36, 1, '2019-11-19 19:26:09', '2019-10-24 14:29:00'),
	(26, 55, 5600.00, 76, 1, '2019-11-18 12:57:37', '2019-10-24 14:29:18'),
	(27, 54, 4100.00, 36, 15, '2019-11-19 22:50:23', '2019-10-24 14:30:04'),
	(28, 56, 9320.00, 14, 102, '2019-11-22 14:02:00', '2019-10-25 10:24:28'),
	(29, 1, 3560.00, 36, 1, '2019-10-28 15:09:06', '2019-10-28 15:09:06'),
	(30, 57, 5500.00, 23, 98, '2019-11-22 14:00:11', '2019-11-04 12:27:41'),
	(31, 58, 4250.00, 15, 99, '2019-11-19 10:05:15', '2019-11-05 13:32:54'),
	(40, 69, 4099.00, 12, 10, '2019-11-24 17:22:41', '2019-11-24 17:22:41');
/*!40000 ALTER TABLE `modifications` ENABLE KEYS */;

-- Дамп структуры для таблица tires.modification_property_values
CREATE TABLE IF NOT EXISTS `modification_property_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) NOT NULL,
  `propertyId` int(10) NOT NULL,
  `modificationId` int(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_modification_property_values_properties` (`propertyId`),
  KEY `FK_modification_property_values_modifications` (`modificationId`),
  CONSTRAINT `FK_modification_property_values_modifications` FOREIGN KEY (`modificationId`) REFERENCES `modifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_modification_property_values_properties` FOREIGN KEY (`propertyId`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.modification_property_values: ~83 rows (приблизительно)
/*!40000 ALTER TABLE `modification_property_values` DISABLE KEYS */;
INSERT INTO `modification_property_values` (`id`, `value`, `propertyId`, `modificationId`, `modified`, `created`) VALUES
	(1, '195', 11, 1, '2019-11-13 10:52:43', '2019-10-11 11:19:29'),
	(2, '65', 12, 1, '2019-11-13 10:52:43', '2019-10-11 11:19:29'),
	(3, '17', 13, 1, '2019-11-13 10:52:43', '2019-10-11 11:20:01'),
	(4, '185', 11, 2, '2019-11-20 10:03:22', '2019-10-11 11:19:29'),
	(5, '65', 12, 2, '2019-11-20 10:03:22', '2019-10-11 11:19:29'),
	(6, '15', 13, 2, '2019-11-20 10:03:22', '2019-10-11 11:20:01'),
	(7, '100', 14, 1, '2019-11-13 10:52:43', '2019-10-18 14:34:14'),
	(8, '0,35', 15, 1, '2019-11-13 10:52:43', '2019-10-18 14:34:39'),
	(23, '195', 11, 19, '2019-10-23 14:03:59', '2019-10-23 14:03:59'),
	(24, '65', 12, 19, '2019-10-23 14:03:59', '2019-10-23 14:03:59'),
	(25, '15', 13, 19, '2019-10-23 14:03:59', '2019-10-23 14:03:59'),
	(26, '6', 14, 19, '2019-10-23 14:03:59', '2019-10-23 14:03:59'),
	(27, '0,35', 15, 19, '2019-10-23 14:03:59', '2019-10-23 14:03:59'),
	(28, '215', 11, 20, '2019-11-21 13:16:39', '2019-10-23 14:09:19'),
	(29, '65', 12, 20, '2019-11-21 13:16:39', '2019-10-23 14:09:19'),
	(30, '16', 13, 20, '2019-11-21 13:16:39', '2019-10-23 14:09:19'),
	(31, '6', 14, 20, '2019-11-21 13:16:39', '2019-10-23 14:09:19'),
	(32, '0,35', 15, 20, '2019-11-21 13:16:39', '2019-10-23 14:09:19'),
	(33, '175', 11, 21, '2019-11-19 19:25:38', '2019-10-24 14:15:41'),
	(34, '65', 12, 21, '2019-11-19 19:25:38', '2019-10-24 14:15:41'),
	(35, '15', 13, 21, '2019-11-19 19:25:38', '2019-10-24 14:15:41'),
	(36, '5', 14, 21, '2019-11-19 19:25:38', '2019-10-24 14:15:41'),
	(37, '0,35', 15, 21, '2019-11-19 19:25:38', '2019-10-24 14:15:41'),
	(38, '185', 11, 22, '2019-11-19 13:41:20', '2019-10-24 14:16:40'),
	(39, '65', 12, 22, '2019-11-19 13:41:20', '2019-10-24 14:16:40'),
	(40, '15', 13, 22, '2019-11-19 13:41:20', '2019-10-24 14:16:40'),
	(41, '5,5', 14, 22, '2019-11-19 13:41:20', '2019-10-24 14:16:40'),
	(42, '0,35', 15, 22, '2019-11-19 13:41:20', '2019-10-24 14:16:40'),
	(43, '225', 11, 23, '2019-11-15 10:37:46', '2019-10-24 14:28:00'),
	(44, '65', 12, 23, '2019-11-15 10:37:46', '2019-10-24 14:28:00'),
	(45, '17', 13, 23, '2019-11-15 10:37:46', '2019-10-24 14:28:00'),
	(46, '8', 14, 23, '2019-11-15 10:37:46', '2019-10-24 14:28:00'),
	(47, '0.4', 15, 23, '2019-11-15 10:37:46', '2019-10-24 14:28:00'),
	(48, '215', 11, 24, '2019-11-12 18:03:19', '2019-10-24 14:28:31'),
	(49, '65', 12, 24, '2019-11-12 18:03:19', '2019-10-24 14:28:31'),
	(50, '17', 13, 24, '2019-11-12 18:03:19', '2019-10-24 14:28:31'),
	(51, '8', 14, 24, '2019-11-12 18:03:19', '2019-10-24 14:28:31'),
	(52, '0.4', 15, 24, '2019-11-12 18:03:19', '2019-10-24 14:28:31'),
	(53, '185', 11, 25, '2019-11-19 19:26:09', '2019-10-24 14:29:00'),
	(54, '65', 12, 25, '2019-11-19 19:26:09', '2019-10-24 14:29:00'),
	(55, '16', 13, 25, '2019-11-19 19:26:09', '2019-10-24 14:29:00'),
	(56, '6', 14, 25, '2019-11-19 19:26:09', '2019-10-24 14:29:00'),
	(57, '0,35', 15, 25, '2019-11-19 19:26:09', '2019-10-24 14:29:00'),
	(58, '175', 11, 26, '2019-11-14 09:08:49', '2019-10-24 14:29:18'),
	(59, '65', 12, 26, '2019-11-14 09:08:49', '2019-10-24 14:29:18'),
	(60, '15', 13, 26, '2019-11-14 09:08:49', '2019-10-24 14:29:18'),
	(61, '5', 14, 26, '2019-11-14 09:08:49', '2019-10-24 14:29:18'),
	(62, '0,35', 15, 26, '2019-11-14 09:08:49', '2019-10-24 14:29:18'),
	(63, '225', 11, 27, '2019-11-19 22:50:23', '2019-10-24 14:30:04'),
	(64, '65', 12, 27, '2019-11-19 22:50:23', '2019-10-24 14:30:04'),
	(65, '17', 13, 27, '2019-11-19 22:50:23', '2019-10-24 14:30:04'),
	(66, '8', 14, 27, '2019-11-19 22:50:23', '2019-10-24 14:30:04'),
	(67, '0.4', 15, 27, '2019-11-19 22:50:23', '2019-10-24 14:30:04'),
	(68, '265', 11, 28, '2019-11-22 14:02:00', '2019-10-25 10:24:29'),
	(69, '70', 12, 28, '2019-11-22 14:02:00', '2019-10-25 10:24:29'),
	(70, '17', 13, 28, '2019-11-22 14:02:00', '2019-10-25 10:24:29'),
	(71, '9', 14, 28, '2019-11-22 14:02:00', '2019-10-25 10:24:29'),
	(72, '0.4', 15, 28, '2019-11-22 14:02:00', '2019-10-25 10:24:29'),
	(73, '195', 11, 3, '2019-11-12 18:04:40', '2019-10-26 23:32:45'),
	(74, '65', 12, 3, '2019-11-12 18:04:40', '2019-10-26 23:32:45'),
	(75, '15', 13, 3, '2019-11-12 18:04:40', '2019-10-26 23:32:45'),
	(76, '5,5', 14, 3, '2019-11-12 18:04:40', '2019-10-26 23:32:45'),
	(77, '0,35', 15, 3, '2019-11-12 18:04:40', '2019-10-26 23:32:45'),
	(78, '215', 11, 4, '2019-10-26 23:33:01', '2019-10-26 23:33:01'),
	(79, '65', 12, 4, '2019-10-26 23:33:01', '2019-10-26 23:33:01'),
	(80, '17', 13, 4, '2019-10-26 23:33:01', '2019-10-26 23:33:01'),
	(81, '6', 14, 4, '2019-10-26 23:33:01', '2019-10-26 23:33:01'),
	(82, '0,35', 15, 4, '2019-10-26 23:33:01', '2019-10-26 23:33:01'),
	(83, '175', 11, 29, '2019-10-28 15:09:06', '2019-10-28 15:09:06'),
	(84, '65', 12, 29, '2019-10-28 15:09:06', '2019-10-28 15:09:06'),
	(85, '17', 13, 29, '2019-10-28 15:09:06', '2019-10-28 15:09:06'),
	(86, '5,5', 14, 29, '2019-10-28 15:09:06', '2019-10-28 15:09:06'),
	(87, '0.4', 15, 29, '2019-10-28 15:09:06', '2019-10-28 15:09:06'),
	(88, '195', 11, 30, '2019-11-20 10:03:01', '2019-11-04 12:27:41'),
	(89, '65', 12, 30, '2019-11-20 10:03:01', '2019-11-04 12:27:41'),
	(90, '16', 13, 30, '2019-11-20 10:03:01', '2019-11-04 12:27:41'),
	(91, '5', 14, 30, '2019-11-20 10:03:01', '2019-11-04 12:27:41'),
	(92, '0,35', 15, 30, '2019-11-20 10:03:01', '2019-11-04 12:27:41'),
	(93, '195', 11, 31, '2019-11-13 15:15:56', '2019-11-05 13:32:54'),
	(94, '65', 12, 31, '2019-11-13 15:15:56', '2019-11-05 13:32:54'),
	(95, '15', 13, 31, '2019-11-13 15:15:56', '2019-11-05 13:32:54'),
	(96, '6', 14, 31, '2019-11-13 15:15:56', '2019-11-05 13:32:54'),
	(97, '0,35', 15, 31, '2019-11-13 15:15:56', '2019-11-05 13:32:54'),
	(108, '195', 11, 40, '2019-11-24 17:22:41', '2019-11-24 17:22:41'),
	(109, '65', 12, 40, '2019-11-24 17:22:41', '2019-11-24 17:22:41'),
	(110, '16', 13, 40, '2019-11-24 17:22:41', '2019-11-24 17:22:41'),
	(111, '5,5', 14, 40, '2019-11-24 17:22:41', '2019-11-24 17:22:41'),
	(112, '0.4', 15, 40, '2019-11-24 17:22:41', '2019-11-24 17:22:41');
/*!40000 ALTER TABLE `modification_property_values` ENABLE KEYS */;

-- Дамп структуры для таблица tires.order_statistics
CREATE TABLE IF NOT EXISTS `order_statistics` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderId` int(10) NOT NULL,
  `modificationId` int(10) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `count` int(10) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `userId` int(10) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_order_statistics_user_order` (`orderId`),
  KEY `FK_order_statistics_modifications` (`modificationId`),
  KEY `FK_order_statistics_users` (`userId`),
  CONSTRAINT `FK_order_statistics_modifications` FOREIGN KEY (`modificationId`) REFERENCES `modifications` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_order_statistics_user_order` FOREIGN KEY (`orderId`) REFERENCES `user_orders` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_order_statistics_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.order_statistics: ~18 rows (приблизительно)
/*!40000 ALTER TABLE `order_statistics` DISABLE KEYS */;
INSERT INTO `order_statistics` (`id`, `orderId`, `modificationId`, `price`, `count`, `total`, `userId`, `created`, `modified`) VALUES
	(1, 14, 2, 3980.00, 4, 15920.00, 2, '2019-10-31 16:39:30', NULL),
	(2, 15, 22, 2380.00, 4, 9520.00, 24, '2019-10-31 19:27:02', NULL),
	(3, 15, 2, 3980.00, 4, 15920.00, 24, '2019-10-31 19:27:02', NULL),
	(4, 16, 20, 5440.00, 8, 43520.00, 25, '2019-11-01 19:06:46', NULL),
	(5, 17, 22, 2380.00, 4, 9520.00, 24, '2019-11-02 11:48:42', NULL),
	(7, 19, 22, 2380.00, 1, 2380.00, 24, '2019-11-02 17:01:03', NULL),
	(9, 24, 3, 3560.00, 4, 14240.00, 26, '2019-11-02 17:12:22', NULL),
	(10, 25, 4, 3990.45, 4, 15961.80, 24, '2019-11-02 17:14:49', NULL),
	(11, 26, 22, 2380.00, 1, 2380.00, 24, '2019-11-02 22:00:57', NULL),
	(12, 27, 2, 3980.00, 1, 3980.00, 24, '2019-11-02 22:24:25', NULL),
	(13, 28, 27, 4100.00, 1, 4100.00, 24, '2019-11-02 22:25:07', NULL),
	(14, 29, 2, 3980.00, 4, 15920.00, 24, '2019-11-03 23:20:58', NULL),
	(15, 29, 21, 2100.00, 1, 2100.00, 24, '2019-11-03 23:20:58', NULL),
	(16, 30, 30, 5500.00, 4, 22000.00, 27, '2019-11-04 12:44:28', NULL),
	(17, 31, 31, 4250.00, 4, 17000.00, 28, '2019-11-05 13:37:12', NULL),
	(18, 32, 31, 4250.00, 4, 17000.00, 29, '2019-11-05 16:41:27', NULL),
	(19, 33, 30, 5500.00, 4, 22000.00, 30, '2019-11-05 16:55:34', NULL),
	(20, 34, 2, 3980.00, 1, 3980.00, 24, '2019-11-07 16:30:24', NULL),
	(26, 40, 2, 3980.00, 4, 15920.00, 24, '2019-11-18 16:31:29', NULL),
	(27, 41, 31, 4250.00, 1, 4250.00, 24, '2019-11-19 10:05:16', NULL),
	(28, 42, 19, 4070.00, 1, 4070.00, 24, '2019-11-20 12:53:37', NULL),
	(29, 43, 20, 5440.00, 1, 5440.00, 24, '2019-11-20 12:56:25', NULL),
	(55, 69, 30, 5500.00, 4, 22000.00, 36, '2019-11-22 14:00:11', NULL);
/*!40000 ALTER TABLE `order_statistics` ENABLE KEYS */;

-- Дамп структуры для таблица tires.order_statuses
CREATE TABLE IF NOT EXISTS `order_statuses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.order_statuses: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `order_statuses` DISABLE KEYS */;
INSERT INTO `order_statuses` (`id`, `value`) VALUES
	(1, 'Создан'),
	(2, 'Подтвержден'),
	(3, 'Отгружен'),
	(4, 'Доставлен'),
	(5, 'Отменен');
/*!40000 ALTER TABLE `order_statuses` ENABLE KEYS */;

-- Дамп структуры для таблица tires.payment_statuses
CREATE TABLE IF NOT EXISTS `payment_statuses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.payment_statuses: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `payment_statuses` DISABLE KEYS */;
INSERT INTO `payment_statuses` (`id`, `value`) VALUES
	(1, 'Ожидает оплаты'),
	(2, 'Оплачен'),
	(3, 'Оплачен частично');
/*!40000 ALTER TABLE `payment_statuses` ENABLE KEYS */;

-- Дамп структуры для таблица tires.payment_types
CREATE TABLE IF NOT EXISTS `payment_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.payment_types: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `payment_types` DISABLE KEYS */;
INSERT INTO `payment_types` (`id`, `value`) VALUES
	(1, 'Наличные'),
	(2, 'По карте');
/*!40000 ALTER TABLE `payment_types` ENABLE KEYS */;

-- Дамп структуры для таблица tires.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `categoryId` int(10) NOT NULL,
  `producerCountryId` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_countries` (`producerCountryId`),
  KEY `FK_products_categories` (`categoryId`),
  CONSTRAINT `FK_products_categories` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_products_countries` FOREIGN KEY (`producerCountryId`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.products: ~14 rows (приблизительно)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `photo`, `categoryId`, `producerCountryId`, `modified`, `created`) VALUES
	(1, 'Hakkapeliitta 8', '/static/images/stock/nokian_hakkapeliitta_8.jpg', 1, 3, '2019-10-22 15:44:46', '2019-10-10 16:40:21'),
	(31, 'Hakkapeliitta 7', '/static/images/stock/hakkapeliitta7.jpg', 1, 3, '2019-11-20 16:11:03', '2019-10-13 16:40:43'),
	(44, 'Шиномонтаж R15', NULL, 16, 1, '2019-10-19 17:08:06', '2019-10-19 17:08:06'),
	(45, 'Шиномонтаж R16', NULL, 16, 1, '2019-10-19 17:08:42', '2019-10-19 17:08:42'),
	(46, 'Шиномонтаж R17', NULL, 16, 1, '2019-10-19 17:08:54', '2019-10-19 17:08:54'),
	(47, 'Шиномонтаж R18', NULL, 16, 1, '2019-10-19 17:09:05', '2019-10-19 17:09:05'),
	(48, 'Шиномонтаж R19', NULL, 16, 1, '2019-10-19 17:09:14', '2019-10-19 17:09:14'),
	(49, 'Шиномонтаж R20', NULL, 16, 1, '2019-10-19 17:09:22', '2019-10-19 17:09:22'),
	(50, 'Шиномонтаж >R20', NULL, 16, 1, '2019-10-19 17:09:33', '2019-10-19 17:09:33'),
	(53, 'Blizzak Spike-02 SUV', '/static/images/stock/blizzak_spike_02.jpg', 1, 4, '2019-10-23 13:52:55', '2019-10-23 13:43:26'),
	(54, 'Formula Energy', '/static/images/stock/formula_energy.jpg', 1, 1, '2019-10-24 14:14:57', '2019-10-24 14:14:57'),
	(55, 'EfficientGrip SUV', '/static/images/stock/efficient_grip.jpg', 1, 2, '2019-10-24 14:27:24', '2019-10-24 14:27:24'),
	(56, 'X- ICE', '/static/images/stock/x_ice.jpg', 1, 5, '2019-10-25 10:23:45', '2019-10-25 10:23:45'),
	(57, 'Proxes ST III', '/static/images/stock/proxes_st_iii.png', 1, 4, '2019-11-04 12:34:32', '2019-11-04 12:26:51'),
	(58, 'Ecopia EP300', '/static/images/stock/ecopia_ep300.jpg', 1, 4, '2019-11-05 13:32:21', '2019-11-05 13:32:21'),
	(69, 'Observe G3 Ice', '/static/images/stock/observe_g3_ice.png', 1, 4, '2019-11-24 17:21:57', '2019-11-24 17:21:34');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Дамп структуры для таблица tires.product_property_values
CREATE TABLE IF NOT EXISTS `product_property_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) NOT NULL,
  `propertyId` int(10) NOT NULL,
  `productId` int(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_property_values_properties` (`propertyId`),
  KEY `FK_product_property_values_products` (`productId`),
  CONSTRAINT `FK_product_property_values_products` FOREIGN KEY (`productId`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_product_property_values_properties` FOREIGN KEY (`propertyId`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.product_property_values: ~24 rows (приблизительно)
/*!40000 ALTER TABLE `product_property_values` DISABLE KEYS */;
INSERT INTO `product_property_values` (`id`, `value`, `propertyId`, `productId`, `modified`, `created`) VALUES
	(1, '9', 1, 1, '2019-10-22 15:44:46', '2019-10-10 16:45:51'),
	(2, '15', 3, 1, '2019-10-22 15:44:46', '2019-10-10 16:50:22'),
	(3, '1', 4, 1, '2019-10-22 15:44:46', '2019-10-19 14:09:07'),
	(13, '9', 1, 31, '2019-11-20 16:11:03', '2019-10-19 17:00:49'),
	(14, '15', 3, 31, '2019-11-20 16:11:03', '2019-10-19 17:00:49'),
	(15, '1', 4, 31, '2019-11-20 16:11:03', '2019-10-19 17:00:49'),
	(24, '2', 1, 53, '2019-10-23 13:52:55', '2019-10-23 13:43:26'),
	(25, '15', 3, 53, '2019-10-23 13:52:55', '2019-10-23 13:43:26'),
	(26, '1', 4, 53, '2019-10-23 13:52:55', '2019-10-23 13:43:26'),
	(27, '12', 1, 54, '2019-10-24 14:14:58', '2019-10-24 14:14:58'),
	(28, '16', 3, 54, '2019-10-24 14:14:58', '2019-10-24 14:14:58'),
	(29, '0', 4, 54, '2019-10-24 14:14:58', '2019-10-24 14:14:58'),
	(30, '6', 1, 55, '2019-10-24 14:27:24', '2019-10-24 14:27:24'),
	(31, '16', 3, 55, '2019-10-24 14:27:24', '2019-10-24 14:27:24'),
	(32, '0', 4, 55, '2019-10-24 14:27:24', '2019-10-24 14:27:24'),
	(33, '8', 1, 56, '2019-10-25 10:23:45', '2019-10-25 10:23:45'),
	(34, '15', 3, 56, '2019-10-25 10:23:45', '2019-10-25 10:23:45'),
	(35, '0', 4, 56, '2019-10-25 10:23:45', '2019-10-25 10:23:45'),
	(36, '10', 1, 57, '2019-11-04 12:34:32', '2019-11-04 12:26:51'),
	(37, '16', 3, 57, '2019-11-04 12:34:32', '2019-11-04 12:26:51'),
	(38, '0', 4, 57, '2019-11-04 12:34:32', '2019-11-04 12:26:51'),
	(39, '2', 1, 58, '2019-11-05 13:32:21', '2019-11-05 13:32:21'),
	(40, '16', 3, 58, '2019-11-05 13:32:21', '2019-11-05 13:32:21'),
	(41, '0', 4, 58, '2019-11-05 13:32:21', '2019-11-05 13:32:21'),
	(72, '10', 1, 69, '2019-11-24 17:21:57', '2019-11-24 17:21:34'),
	(73, '15', 3, 69, '2019-11-24 17:21:57', '2019-11-24 17:21:34'),
	(74, '1', 4, 69, '2019-11-24 17:21:57', '2019-11-24 17:21:34');
/*!40000 ALTER TABLE `product_property_values` ENABLE KEYS */;

-- Дамп структуры для таблица tires.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `categoryId` int(10) NOT NULL,
  `entityTypeId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `unitId` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_properties_categories` (`categoryId`),
  KEY `FK_properties_property_entity_types` (`entityTypeId`),
  KEY `FK_properties_units` (`unitId`),
  CONSTRAINT `FK_properties_categories` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_properties_property_entity_types` FOREIGN KEY (`entityTypeId`) REFERENCES `property_entity_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_properties_units` FOREIGN KEY (`unitId`) REFERENCES `units` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.properties: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `categoryId`, `entityTypeId`, `typeId`, `unitId`, `modified`, `created`) VALUES
	(1, 'Производитель', 1, 1, 3, NULL, '2019-10-16 12:18:29', '2019-10-10 16:29:57'),
	(3, 'Сезон', 1, 1, 3, NULL, '2019-10-10 16:46:58', '2019-10-10 16:47:00'),
	(4, 'Шипы', 1, 1, 4, NULL, '2019-10-10 16:46:58', '2019-10-10 16:47:00'),
	(11, 'Ширина', 1, 2, 2, 1, '2019-11-20 18:26:55', '2019-10-11 11:05:09'),
	(12, 'Профиль', 1, 2, 2, 6, '2019-10-19 12:23:32', '2019-10-11 11:05:09'),
	(13, 'Диаметр', 1, 2, 2, 2, '2019-10-19 12:23:01', '2019-10-11 11:14:35'),
	(14, 'Вес', 1, 2, 1, 4, '2019-10-11 11:15:25', '2019-10-11 11:15:27'),
	(15, 'Объем', 1, 2, 1, 3, '2019-10-17 22:43:49', '2019-10-11 11:16:10');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- Дамп структуры для таблица tires.property_entity_types
CREATE TABLE IF NOT EXISTS `property_entity_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.property_entity_types: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `property_entity_types` DISABLE KEYS */;
INSERT INTO `property_entity_types` (`id`, `value`) VALUES
	(1, 'product'),
	(2, 'modification');
/*!40000 ALTER TABLE `property_entity_types` ENABLE KEYS */;

-- Дамп структуры для таблица tires.property_enum_values
CREATE TABLE IF NOT EXISTS `property_enum_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) NOT NULL,
  `propertyId` int(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_property_enum_values_properties` (`propertyId`),
  CONSTRAINT `FK_property_enum_values_properties` FOREIGN KEY (`propertyId`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.property_enum_values: ~16 rows (приблизительно)
/*!40000 ALTER TABLE `property_enum_values` DISABLE KEYS */;
INSERT INTO `property_enum_values` (`id`, `value`, `propertyId`, `modified`, `created`) VALUES
	(1, 'Amtel', 1, '2019-10-10 16:31:21', '2019-10-10 16:31:22'),
	(2, 'Bridgestone', 1, '2019-10-10 16:31:21', '2019-10-10 16:31:22'),
	(3, 'Continental', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(4, 'Cordiant', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(5, 'Dunlop', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(6, 'GOODYEAR', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(7, 'Gislaved', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(8, 'Michelin', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(9, 'Nokian Tyres', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(10, 'Toyo', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(11, 'Yokohama', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(12, 'Pirelli', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(13, 'Viatti', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(14, 'КАМА', 1, '2019-10-10 16:31:59', '2019-10-10 16:32:01'),
	(15, 'Зима', 3, '2019-10-10 16:47:38', '2019-10-10 16:47:39'),
	(16, 'Лето', 3, '2019-10-10 16:47:48', '2019-10-10 16:47:49');
/*!40000 ALTER TABLE `property_enum_values` ENABLE KEYS */;

-- Дамп структуры для таблица tires.property_types
CREATE TABLE IF NOT EXISTS `property_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.property_types: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `property_types` DISABLE KEYS */;
INSERT INTO `property_types` (`id`, `name`, `modified`, `created`) VALUES
	(1, 'string', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(2, 'int', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(3, 'enum', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(4, 'bool', '2019-10-10 16:46:15', '2019-10-10 16:46:16');
/*!40000 ALTER TABLE `property_types` ENABLE KEYS */;

-- Дамп структуры для таблица tires.units
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.units: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` (`id`, `name`, `modified`, `created`) VALUES
	(1, 'мм', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(2, 'д', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(3, 'м3', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(4, 'кг', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(5, 'м2', '2019-10-10 00:01:01', '2019-10-10 00:01:01'),
	(6, '%', '2019-10-10 00:01:01', '2019-10-10 00:01:01');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;

-- Дамп структуры для таблица tires.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `birthday` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.users: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `lastName`, `email`, `password`, `birthday`, `modified`, `created`) VALUES
	(1, 'ironman', NULL, 'ironman@mail.ru', '$2a$10$hbxecwitQQ.dDT4JOFzQAulNySFwEpaFLw38jda6Td.Y/cOiRzDFu', '2011-10-29', '2019-10-20 16:26:02', '2019-10-20 16:26:03'),
	(2, 'rabi', NULL, 'rabi@inbox.ru', '$2a$10$hbxecwitQQ.dDT4JOFzQAulNySFwEpaFLw38jda6Td.Y/cOiRzDFu', NULL, '2019-10-20 16:26:28', '2019-10-20 16:26:30'),
	(11, 'Сергей', 'Самушенков', 'olesya4511@yandex.ru', '$2a$10$ZVHCey6jNS1S9KKLs49l3.EeuhmN10NwyQ3jnNyX2LseSr6B8LbBS', '1987-04-06', '2019-10-29 21:53:15', '2019-10-29 21:53:15'),
	(24, 'Сергей', 'Самушенков', 'zench@inbox.ru', '$2a$10$ZEExDc0AoeVNvT7QR7EciObJNeGKquk7AVmeAGie2.wdck573zlgK', '1987-04-06', '2019-11-23 17:12:03', '2019-10-31 19:26:09'),
	(25, 'Иван', 'Пупкин', 'pupkin@mail.ru', '$2a$10$sr2UkthRw3mFz9FhFFsuHuJZruY9TnVuwgcQ6lWizsgpadPY1890i', '1977-11-18', '2019-11-01 19:05:18', '2019-11-01 19:05:18'),
	(26, 'Василий ', 'Григорьев', 'vasiliy@mail.ru', '$2a$10$TUtbNUK/Z5mOKIJ.i6lJtubjNK.xWmTBDpwXvBG37j2qe1JzazORe', '1998-02-18', '2019-11-02 15:13:44', '2019-11-02 15:13:44'),
	(27, 'Иван', 'Иванов', 'ivan@mail.ru', '$2a$10$xv02JXo4bcZhJMzNrwgJ7ODLbDPyw8RQtBNhwRdRFbH3kMrU6b64u', '2000-12-09', '2019-11-04 12:43:24', '2019-11-04 12:42:10'),
	(28, 'Сидор', 'Сидоров', 'sidor@mail.ru', '$2a$10$LlyshXPiBgiyBx4s0Nl5jepMqR6y3Y3VAI6qeLYymUW34ftXJNE5.', '1994-08-10', '2019-11-05 13:36:09', '2019-11-05 13:35:38'),
	(29, 'Петр', 'Петров', 'petr@mail.ru', '$2a$10$grwh3N9f1/4asq7PWZ0ZTOUI8VB6mUpDk8Jzj/zFkrgiZWclE3OyG', '1980-11-07', '2019-11-05 16:40:14', '2019-11-05 16:40:14'),
	(30, 'Сергей', 'Сергеев', 'sergei@mail.ru', '$2a$10$jm7LB7hFrrgs5IUcAaWacOOMU7uGgEgJ.iL9n7JWmXpNVojtM.dJq', '1980-11-08', '2019-11-05 16:54:36', '2019-11-05 16:54:36'),
	(36, 'Zinedin', 'Zidane', 'zidan@mail.ru', '$2a$10$BkuyYilV3hP/nWelLcT..OmOANUBsN5GrdkrwZqbBDXPKOIzTdgs6', '1970-11-22', '2019-11-22 13:58:47', '2019-11-22 13:58:47'),
	(37, 'Пупкин', 'Иван', 'pupkin2@mail.ru', '$2a$10$uM69DseuabBuc10kl6XAfuMyj/RSEr8YMzmFLgKoYVXfj.Fo30xRu', '1979-12-18', '2019-11-23 15:41:16', '2019-11-23 15:30:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп структуры для таблица tires.user_address
CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userId` int(10) DEFAULT NULL,
  `countryId` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `house` varchar(20) NOT NULL,
  `flat` varchar(20) DEFAULT NULL,
  `zip` int(10) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_address_users` (`userId`),
  KEY `FK_user_address_countries` (`countryId`),
  CONSTRAINT `FK_user_address_countries` FOREIGN KEY (`countryId`) REFERENCES `countries` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_user_address_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.user_address: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
INSERT INTO `user_address` (`id`, `userId`, `countryId`, `city`, `street`, `house`, `flat`, `zip`, `modified`, `created`) VALUES
	(33, 2, 1, 'Пушкин', 'Anciferovskaya', '9', '12', 196632, NULL, '2019-10-31 16:39:13'),
	(34, 24, 1, 'Пушкин', 'Анциферовская(Гуммолосары) улица (г. Пушкин)', '9', '14', 196632, '2019-11-23 16:46:46', '2019-10-31 19:26:56'),
	(35, 25, 1, 'Санкт-Петербург', 'Невский пр.', '18', '202', 190000, NULL, '2019-11-01 19:06:42'),
	(36, 24, 1, 'Санкт-Петербург', '13 линия Васильевского острова', '13', '', 190000, '2019-11-23 16:47:00', '2019-11-02 11:48:41'),
	(42, 26, 1, 'Санкт-Петербург', 'Невский пр.', '22', '1', 190000, NULL, '2019-11-02 17:12:14'),
	(43, 27, 1, 'Санкт-Петербург', 'Большой пр. ВО', '12', '2', 190000, NULL, '2019-11-04 12:44:27'),
	(44, 28, 1, 'Санкт-Петербург', 'Дыбенко ул.', '167', '65', 190000, NULL, '2019-11-05 13:37:12'),
	(45, 29, 1, 'Санкт-Петербург', 'Санкт-Петербург, 13 линия, 14', '9', '1', 190000, NULL, '2019-11-05 16:41:27'),
	(46, 30, 1, 'Санкт-Петербург', 'Санкт-Петербург, 13 линия, 14', '89', '23', 190000, NULL, '2019-11-05 16:55:34'),
	(54, 36, 1, 'Madrid', 'La Rambla', '5', '', 456789, NULL, '2019-11-22 14:00:11');
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;

-- Дамп структуры для таблица tires.user_authority
CREATE TABLE IF NOT EXISTS `user_authority` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `authorityId` int(10) NOT NULL,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_user_authority_users` (`userId`),
  KEY `FK_user_authority_authority` (`authorityId`),
  CONSTRAINT `FK_user_authority_authority` FOREIGN KEY (`authorityId`) REFERENCES `authority` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_authority_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.user_authority: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `user_authority` DISABLE KEYS */;
INSERT INTO `user_authority` (`id`, `userId`, `authorityId`, `modified`, `created`) VALUES
	(1, 1, 1, '2019-10-20 16:26:48', '2019-10-20 16:26:49'),
	(2, 2, 2, '2019-10-20 16:26:58', '2019-10-20 16:27:00'),
	(5, 11, 2, NULL, '2019-10-29 21:53:15'),
	(18, 24, 2, NULL, '2019-10-31 19:26:08'),
	(19, 25, 2, NULL, '2019-11-01 19:05:18'),
	(20, 26, 2, NULL, '2019-11-02 15:13:43'),
	(21, 27, 2, NULL, '2019-11-04 12:42:09'),
	(22, 28, 2, NULL, '2019-11-05 13:35:38'),
	(23, 29, 2, NULL, '2019-11-05 16:40:14'),
	(24, 30, 2, NULL, '2019-11-05 16:54:35'),
	(30, 36, 2, NULL, '2019-11-22 13:58:46'),
	(31, 37, 2, NULL, '2019-11-23 15:30:21');
/*!40000 ALTER TABLE `user_authority` ENABLE KEYS */;

-- Дамп структуры для таблица tires.user_orders
CREATE TABLE IF NOT EXISTS `user_orders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `userAddressId` int(10) DEFAULT NULL,
  `content` varchar(1000) NOT NULL,
  `statusId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `paymentTypeId` int(10) NOT NULL,
  `deliveryTypeId` int(10) NOT NULL,
  `paymentStatusId` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_order_users` (`userId`),
  KEY `FK_order_user_address` (`userAddressId`),
  KEY `FK_order_order_status` (`statusId`),
  KEY `FK_user_order_delivery_type` (`deliveryTypeId`),
  KEY `FK_user_order_payment_type` (`paymentTypeId`),
  KEY `FK_user_orders_payment_statuses` (`paymentStatusId`),
  CONSTRAINT `FK_order_order_status` FOREIGN KEY (`statusId`) REFERENCES `order_statuses` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_order_user_address` FOREIGN KEY (`userAddressId`) REFERENCES `user_address` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `FK_order_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_order_delivery_type` FOREIGN KEY (`deliveryTypeId`) REFERENCES `delivery_types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_user_order_payment_type` FOREIGN KEY (`paymentTypeId`) REFERENCES `payment_types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_user_orders_payment_statuses` FOREIGN KEY (`paymentStatusId`) REFERENCES `payment_statuses` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tires.user_orders: ~16 rows (приблизительно)
/*!40000 ALTER TABLE `user_orders` DISABLE KEYS */;
INSERT INTO `user_orders` (`id`, `userId`, `userAddressId`, `content`, `statusId`, `name`, `lastName`, `phone`, `total`, `paymentTypeId`, `deliveryTypeId`, `paymentStatusId`, `modified`, `created`) VALUES
	(14, 2, 33, '[{"id":81,"modificationId":2,"price":3980.00,"name":"Hakkapeliitta 8","photoUrl":"/static/images/stock/nokian_hakkapeliitta_8.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 3, 'Сергей', 'Самушенков', '89046154073', 15920.00, 1, 1, 1, '2019-10-31 19:00:23', '2019-10-31 16:39:24'),
	(15, 24, 34, '[{"id":82,"modificationId":22,"price":2380.00,"name":"Formula Energy","photoUrl":"/static/images/stock/formula_energy.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}},{"id":83,"modificationId":2,"price":3980.00,"name":"Hakkapeliitta 8","photoUrl":"/static/images/stock/nokian_hakkapeliitta_8.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 2, 'Сергей', 'Самушенков', '89046154073', 25440.00, 2, 3, 2, '2019-11-02 18:25:19', '2019-10-31 19:26:58'),
	(16, 25, 35, '[{"id":85,"modificationId":20,"price":5440.00,"name":"Blizzak Spike-02 SUV","photoUrl":"/static/images/stock/blizzak_spike_02.jpg","count":8,"properties":{"Диаметр":"16","Ширина":"215","Профиль":"65"}}]', 2, 'Иван', 'Пупкин', '89219544545', 43520.00, 1, 1, 1, '2019-11-01 19:07:42', '2019-11-01 19:06:46'),
	(17, 24, 36, '[{"id":86,"modificationId":22,"price":2380.00,"name":"Formula Energy","photoUrl":"/static/images/stock/formula_energy.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 9520.00, 1, 1, 1, NULL, '2019-11-02 11:48:42'),
	(19, 24, 36, '[{"id":88,"modificationId":22,"price":2380.00,"name":"Formula Energy","photoUrl":"/static/images/stock/formula_energy.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 2380.00, 1, 1, 1, NULL, '2019-11-02 17:01:03'),
	(24, 26, 42, '[{"id":89,"modificationId":3,"price":3560.00,"name":"Hakkapeliitta 7","photoUrl":"/static/images/stock/hakkapeliitta7.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"195","Профиль":"65"}}]', 1, 'Василий', 'Григорьев', '89219696666', 14240.00, 1, 1, 1, NULL, '2019-11-02 17:12:14'),
	(25, 24, 36, '[{"id":90,"modificationId":4,"price":3990.45,"name":"Hakkapeliitta 7","photoUrl":"/static/images/stock/hakkapeliitta7.jpg","count":4,"properties":{"Диаметр":"17","Ширина":"215","Профиль":"65"}}]', 2, 'Сергей', 'Самушенков', '89046154073', 15961.80, 2, 4, 1, '2019-11-02 18:08:29', '2019-11-02 17:14:34'),
	(26, 24, 36, '[{"id":91,"modificationId":22,"price":2380.00,"name":"Formula Energy","photoUrl":"/static/images/stock/formula_energy.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 2380.00, 2, 2, 2, '2019-11-02 22:01:58', '2019-11-02 22:00:57'),
	(27, 24, 36, '[{"id":92,"modificationId":2,"price":3980.00,"name":"Hakkapeliitta 8","photoUrl":"/static/images/stock/nokian_hakkapeliitta_8.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 3980.00, 1, 1, 1, NULL, '2019-11-02 22:24:25'),
	(28, 24, 36, '[{"id":93,"modificationId":27,"price":4100.00,"name":"Formula Energy","photoUrl":"/static/images/stock/formula_energy.jpg","count":1,"properties":{"Диаметр":"17","Ширина":"225","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 4100.00, 1, 3, 1, NULL, '2019-11-02 22:25:07'),
	(29, 24, 36, '[{"id":99,"modificationId":2,"price":3980.00,"name":"Hakkapeliitta 8","photoUrl":"/static/images/stock/nokian_hakkapeliitta_8.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}},{"id":103,"modificationId":21,"price":2100.00,"name":"Formula Energy","photoUrl":"/static/images/stock/formula_energy.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"175","Профиль":"65"}}]', 2, 'Сергей', 'Самушенков', '89046154073', 18020.00, 1, 1, 1, '2019-11-18 10:30:06', '2019-11-03 23:20:58'),
	(30, 27, 43, '[{"id":112,"modificationId":30,"price":5500.00,"name":"Proxes ST III","photoUrl":"/static/images/stock/proxes_st_iii.png","count":4,"properties":{"Диаметр":"16","Ширина":"195","Профиль":"65"}}]', 2, 'Иван', 'Иванов', '89219454545', 22000.00, 2, 1, 1, '2019-11-04 12:45:10', '2019-11-04 12:44:28'),
	(31, 28, 44, '[{"id":115,"modificationId":31,"price":4250.00,"name":"Ecopia EP300","photoUrl":"/static/images/stock/ecopia_ep300.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"195","Профиль":"65"}}]', 2, 'Сидор', 'Сидоров', '89219664545', 17000.00, 2, 2, 1, '2019-11-05 13:38:28', '2019-11-05 13:37:12'),
	(32, 29, 45, '[{"id":116,"modificationId":31,"price":4250.00,"name":"Ecopia EP300","photoUrl":"/static/images/stock/ecopia_ep300.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"195","Профиль":"65"}}]', 2, 'Петр', 'Петров', '89046154073', 17000.00, 2, 2, 1, '2019-11-05 16:42:10', '2019-11-05 16:41:27'),
	(33, 30, 46, '[{"id":117,"modificationId":30,"price":5500.00,"name":"Proxes ST III","photoUrl":"/static/images/stock/proxes_st_iii.png","count":4,"properties":{"Диаметр":"16","Ширина":"195","Профиль":"65"}}]', 2, 'Сергей', 'Сергеев', '89046154073', 22000.00, 2, 3, 1, '2019-11-05 16:56:14', '2019-11-05 16:55:34'),
	(34, 24, 36, '[{"id":118,"modificationId":2,"price":3980.00,"name":"Hakkapeliitta 8","photoUrl":"/static/images/stock/nokian_hakkapeliitta_8.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 3980.00, 1, 2, 1, NULL, '2019-11-07 16:30:24'),
	(40, 24, 36, '[{"id":131,"modificationId":2,"price":3980.00,"name":"Hakkapeliitta 8","photoUrl":"/static/images/stock/nokian_hakkapeliitta_8.jpg","count":4,"properties":{"Диаметр":"15","Ширина":"185","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 15920.00, 1, 1, 1, NULL, '2019-11-18 16:31:28'),
	(41, 24, 36, '[{"id":132,"modificationId":31,"price":4250.00,"name":"Ecopia EP300","photoUrl":"/static/images/stock/ecopia_ep300.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"195","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 4250.00, 1, 1, 1, NULL, '2019-11-19 10:05:15'),
	(42, 24, 36, '[{"id":133,"modificationId":19,"price":4070.00,"name":"Blizzak Spike-02 SUV","photoUrl":"/static/images/stock/blizzak_spike_02.jpg","count":1,"properties":{"Диаметр":"15","Ширина":"195","Профиль":"65"}}]', 1, 'Сергей', 'Самушенков', '89046154073', 4070.00, 1, 2, 1, NULL, '2019-11-20 12:53:37'),
	(43, 24, 36, '[{"id":134,"modificationId":20,"price":5440.00,"name":"Blizzak Spike-02 SUV","photoUrl":"/static/images/stock/blizzak_spike_02.jpg","count":1,"properties":{"Диаметр":"16","Ширина":"215","Профиль":"65"}}]', 5, 'Сергей', 'Самушенков', '89046154073', 5440.00, 1, 2, 1, '2019-11-20 22:56:10', '2019-11-20 12:56:25'),
	(69, 36, 54, '[{"id":160,"modificationId":30,"price":5500.00,"name":"Proxes ST III","photoUrl":"/static/images/stock/proxes_st_iii.png","count":4,"properties":{"Диаметр":"16","Ширина":"195","Профиль":"65"}}]', 1, 'Zinedin', 'Zidane', '+49046879785666', 22000.00, 2, 2, 1, NULL, '2019-11-22 14:00:11');
/*!40000 ALTER TABLE `user_orders` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
