package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.OrderDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.OrderMapper;
import ru.t_systems.samushenkov.tires_shop.model.*;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.OrderService;

import java.util.List;
import java.util.Map;

public class OrderServiceTest extends AbstractServiceTest {
    @Autowired
    OrderService orderService;

    @Autowired
    OrderMapper orderMapper;

    @Test
    public void testAllOrders() {
        ModelAndView mv = orderService.allOrders(1);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("ordersList"));
        Assert.assertTrue(modelMap.containsKey("page"));
        Assert.assertTrue(modelMap.containsKey("itemsCount"));
        Assert.assertTrue(modelMap.containsKey("pagesCount"));
        Assert.assertTrue(modelMap.containsKey("statusesList"));
        Assert.assertTrue(modelMap.containsKey("deliveryTypesList"));
        Assert.assertTrue(modelMap.containsKey("paymentTypesList"));
        Assert.assertTrue(modelMap.containsKey("paymentStatusesList"));
        List<Order> orders = (List<Order>) modelMap.get("ordersList");
        Assert.assertTrue(orders.size() >= 1);
    }

    @Test
    public void testEditOrder() {
        DeliveryType tempDT = new DeliveryType();
        tempDT.setId(2);
        OrderStatus tempStatus = new OrderStatus();
        tempStatus.setId(2);
        PaymentStatus tempPS = new PaymentStatus();
        tempPS.setId(2);
        PaymentType tempPT = new PaymentType();
        tempPT.setId(2);

        OrderDTO orderDTO = orderMapper.convertToDTO(orderService.getById(1));
        Assert.assertNotNull(orderDTO);
        Assert.assertFalse(orderDTO.getContent().isEmpty());

        DeliveryType dbDeliveryType = orderDTO.getDeliveryType();
        OrderStatus dbOS = orderDTO.getStatus();
        PaymentType dbPT = orderDTO.getPaymentType();
        PaymentStatus dbPS = orderDTO.getPaymentStatus();
        orderDTO.setDeliveryType(tempDT);
        orderDTO.setStatus(tempStatus);
        orderDTO.setPaymentStatus(tempPS);
        orderDTO.setPaymentType(tempPT);
        orderService.edit(orderDTO);
        orderDTO = orderMapper.convertToDTO(orderService.getById(orderDTO.getId()));
        Assert.assertEquals(2, orderDTO.getDeliveryType().getId());
        Assert.assertEquals(2, orderDTO.getStatus().getId());
        orderDTO.setStatus(dbOS);
        orderDTO.setDeliveryType(dbDeliveryType);
        orderDTO.setPaymentType(dbPT);
        orderDTO.setPaymentStatus(dbPS);
        orderService.edit(orderDTO);
    }
}
