package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.PropertyDTO;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.model.Property;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;
import ru.t_systems.samushenkov.tires_shop.model.PropertyType;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyService;

import java.util.List;
import java.util.Map;

public class PropertyServiceTest extends AbstractServiceTest {

    private Property property;
    @Autowired
    private PropertyService propertyService;



    @Before
    public void setUp(){
        this.property = new Property();
        this.property.setCategoryId(1);
        this.property.setId(1);
        this.property.setName("Производитель");
    }

    @Test
    public void testGetById() {
        Property property = propertyService.getById(1);
        Assert.assertNotNull(property);
        Assert.assertEquals(this.property.getName(), property.getName());
    }

    @Test
    public void testAllProperties() {
        List<Property> properties = propertyService.allProperties(1, 1);
        Assert.assertFalse(properties.isEmpty());
        Assert.assertTrue(properties.size()>1);
        Assert.assertTrue(properties.get(0).getName().length() > 2);
    }

    @Test
    public void testAllMVProperties() {
        ModelAndView mv = propertyService.allMVProperties(1,1);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("propertiesList"));
        Assert.assertTrue(modelMap.containsKey("category"));
        List<Property> properties = (List<Property>) modelMap.get("propertiesList");
        Assert.assertTrue(properties.size() >= 1);
        Assert.assertTrue(properties.get(0).getName().length() > 2);
        Category category = (Category) modelMap.get("category");
        Assert.assertTrue(category.getName().length() > 2);
    }

    @Test
    public void testGetProperty() {
        ModelAndView mv = propertyService.getProperty(1);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("property"));
        Assert.assertTrue(modelMap.containsKey("category"));
        Assert.assertTrue(modelMap.containsKey("units"));
        Assert.assertTrue(modelMap.containsKey("types"));
        Assert.assertTrue(modelMap.containsKey("entityTypes"));
        Property property = (Property) modelMap.get("property");
        Assert.assertEquals(1, property.getId());
        Assert.assertTrue(property.getName().length() > 2);
    }

    @Test
    public void testAdd() {
        PropertyDTO newPropertyDTO = new PropertyDTO();
        PropertyEntityType pet = new PropertyEntityType();
        pet.setId(1);
        PropertyType pt = new PropertyType();
        pt.setId(1);
        newPropertyDTO.setName("Test");
        newPropertyDTO.setCategoryId(1);
        newPropertyDTO.setEntityType(pet);
        newPropertyDTO.setType(pt);
        newPropertyDTO = propertyService.add(newPropertyDTO);
        Assert.assertEquals("Test", newPropertyDTO.getName());
        newPropertyDTO.setName("TestUpdated");
        propertyService.edit(newPropertyDTO);
        Assert.assertEquals("TestUpdated", propertyService.getById(newPropertyDTO.getId()).getName());
        propertyService.delete(newPropertyDTO.getId());
    }
}
