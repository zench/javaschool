package ru.t_systems.samushenkov.tires_shop.service;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t_systems.samushenkov.tires_shop.config.AHibernateTestConfig;
import ru.t_systems.samushenkov.tires_shop.config.WebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebConfig.class, AHibernateTestConfig.class })
@WebAppConfiguration
public class AbstractServiceTest {
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Test
    public void abstractTest(){
        Assert.assertTrue(true);
    }
}
