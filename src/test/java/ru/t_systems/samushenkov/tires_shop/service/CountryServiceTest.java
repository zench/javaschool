package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CountryService;

import java.util.List;

public class CountryServiceTest  extends AbstractServiceTest {

    private Country country;

    @Autowired
    CountryService countryService;

    @Before
    public void setUp(){
        this.country = new Country();
        this.country.setName("Россия");
        this.country.setId(1);
    }

    @Test
    public void testGetById(){
        Country dbCountry = countryService.getById(1);
        Assert.assertNotNull(dbCountry);
        Assert.assertEquals(this.country.getName(), dbCountry.getName());
    }

    @Test
    public void testAllCountries() {
        List<Country> countries = countryService.allCountries();
        Assert.assertFalse(countries.isEmpty());
        Assert.assertTrue(countries.size()>5);
    }
}
