package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.naming.NamingException;

public class SenderServiceTest extends AbstractServiceTest {

    @Autowired
    private SenderService senderService;

    @Test
    public void testSend() {
        String message = "test";
        try{
            senderService.send(message);
            Assert.assertTrue(true);
        } catch (NamingException | JMSException e) {
            Assert.fail();
        }
    }
}
