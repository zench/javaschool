package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.model.PropertyEntityType;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyEntityTypeService;

import java.util.List;

public class PropertyEntityTypeServiceTest  extends AbstractServiceTest {
    private PropertyEntityType propertyEntityType;

    @Autowired
    PropertyEntityTypeService propertyEntityTypeService;

    @Before
    public void setUp(){
        this.propertyEntityType = new PropertyEntityType();
        this.propertyEntityType.setValue("product");
        this.propertyEntityType.setId(1);
    }

    @Test
    public void testGetById(){
        PropertyEntityType dbpropertyEntityType = propertyEntityTypeService.getById(1);
        Assert.assertNotNull(dbpropertyEntityType);
        Assert.assertEquals(this.propertyEntityType.getValue(), dbpropertyEntityType.getValue());
    }

    @Test
    public void testAllEntityTypes() {
        List<PropertyEntityType> propertyEntityTypes = propertyEntityTypeService.allEntityTypes();
        Assert.assertFalse(propertyEntityTypes.isEmpty());
        Assert.assertTrue(propertyEntityTypes.size()>1);
    }
}
