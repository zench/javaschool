package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.CategoryDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.CategoryMapper;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CategoryService;

import java.util.List;
import java.util.Map;

public class CategoryServiceTest extends AbstractServiceTest {
    private CategoryDTO categoryDTO;
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    CategoryService categoryService;

    @Before
    public void setUp(){
        this.categoryDTO = new CategoryDTO();
        this.categoryDTO.setName("Шины");
        this.categoryDTO.setId(1);
    }

    @Test
    public void testGetById(){
        Category dbCategory = categoryService.getById(1);
        Assert.assertNotNull(dbCategory);
        Assert.assertEquals(this.categoryDTO.getName(), dbCategory.getName());
    }

    @Test
    public void testAllCategories() {
        ModelAndView modelAndView = categoryService.allCategories();
        Map<String, Object> modelMap = modelAndView.getModel();
        Assert.assertNotNull(modelAndView);
        Assert.assertTrue(modelMap.containsKey("categoriesList"));
        List<CategoryDTO> categories = (List<CategoryDTO>) modelMap.get("categoriesList");
        Assert.assertTrue(categories.size() >= 1);
    }

    @Test
    public void testAddCategory() {
        CategoryDTO newCatDTO = new CategoryDTO();
        newCatDTO.setName("Test");
        newCatDTO = categoryService.add(newCatDTO);
        Assert.assertEquals("Test", newCatDTO.getName());
        newCatDTO.setName("TestUpdated");
        categoryService.edit(newCatDTO);
        Assert.assertEquals("TestUpdated", categoryService.getById(categoryMapper.convertToEntity(newCatDTO).getId()).getName());
        categoryService.delete(newCatDTO.getId());
    }
}
