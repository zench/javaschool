package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.context.WebApplicationContext;
import ru.t_systems.samushenkov.tires_shop.model.CartItem;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.CartItemService;

public class CartItemServiceTest extends AbstractServiceTest {
    private CartItem cartItem;
    private MockHttpSession mockSession;
    private String sessionId = "C9EC5F7F3A8BC5DD7495FE37AEE87D4B";
    private int dbUserId = 24;

    @Autowired
    private CartItemService cartItemService;
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp(){
        this.mockSession = new MockHttpSession(wac.getServletContext(), this.sessionId);
        Modification modification = new Modification();
        modification.setId(1);
        this.cartItem = new CartItem();
        this.cartItem.setModification(modification);
        this.cartItem.setCount(1);
        this.cartItem.setSessionId(this.sessionId);
    }

    @Test
    public void testAdd() {
        //test new cartItem
        CartItem savedCartItem = cartItemService.add(this.sessionId, this.cartItem.getModification().getId(), this.cartItem.getCount(), null);
        Assert.assertNotNull(savedCartItem);
        Assert.assertTrue(savedCartItem.getId() > 0);
        Assert.assertEquals(savedCartItem.getModification().getId(), this.cartItem.getModification().getId());
        Assert.assertEquals(savedCartItem.getCount(), this.cartItem.getCount());

        //test add cartItem if there is in the cart the same
        this.cartItem.setCount(this.cartItem.getCount()+1);
        CartItem savedCartItem2 = cartItemService.add(this.sessionId, this.cartItem.getModification().getId(), this.cartItem.getCount(), this.dbUserId);
        Assert.assertNotNull(savedCartItem2);
        Assert.assertEquals(savedCartItem2.getId(), savedCartItem.getId());
        Assert.assertEquals(savedCartItem2.getModification().getId(), this.cartItem.getModification().getId());
        Assert.assertEquals(savedCartItem2.getCount(), this.cartItem.getCount()+1);

        //edit cartItem
        savedCartItem2.setCount(savedCartItem2.getCount()+1);
        cartItemService.edit(savedCartItem2);
        CartItem savedCartItem3 = cartItemService.getById(savedCartItem2.getId());
        Assert.assertNotNull(savedCartItem3);
        Assert.assertEquals(savedCartItem3.getCount(), savedCartItem2.getCount());

        //delete cartItem
        this.cartItemService.delete(savedCartItem.getId());
    }
}
