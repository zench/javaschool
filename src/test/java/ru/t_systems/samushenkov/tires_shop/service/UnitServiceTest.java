package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.model.Unit;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UnitService;

import java.util.List;

public class UnitServiceTest  extends AbstractServiceTest {
    private Unit unit;

    @Autowired
    UnitService unitService;

    @Before
    public void setUp(){
        this.unit = new Unit();
        this.unit.setName("мм");
        this.unit.setId(1);
    }

    @Test
    public void testGetById(){
        Unit dbUnit = unitService.getById(1);
        Assert.assertNotNull(dbUnit);
        Assert.assertEquals(this.unit.getName(), dbUnit.getName());
    }

    @Test
    public void testAllUnits() {
        List<Unit> units = unitService.allUnits();
        Assert.assertFalse(units.isEmpty());
        Assert.assertTrue(units.size()>5);
    }
}
