package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ProductDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.ProductMapper;
import ru.t_systems.samushenkov.tires_shop.model.*;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductServiceTest extends AbstractServiceTest {
    private Product product;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    ProductService productService;

    @Before
    public void setUp(){
        Category category = new Category();
        category.setId(1);
        Country country = new Country();
        country.setId(1);
        this.product = new Product();
        this.product.setName("Hakkapeliitta 8");
        this.product.setId(1);
        this.product.setCategory(category);
        this.product.setProducerCountry(country);
        ProductProperty pp = new ProductProperty();
        Property property = new Property();
        property.setId(1);
        pp.setProperty(property);
        pp.setValue("9");
        List<ProductProperty> productProperties = new ArrayList<>();
        productProperties.add(pp);
        this.product.setProductProperties(productProperties);
    }

    @Test
    public void testGetById(){
        Product dbProduct = productService.getById(1);
        Assert.assertNotNull(dbProduct);
        Assert.assertEquals(this.product.getName(), dbProduct.getName());
    }

    @Test
    public void testAllProducts() {
        ModelAndView mv = productService.allProducts(1, this.product.getCategory().getId());
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("productsList"));
        Assert.assertTrue(modelMap.containsKey("category"));
        Assert.assertTrue(modelMap.containsKey("page"));
        Assert.assertTrue(modelMap.containsKey("itemsCount"));
        Assert.assertTrue(modelMap.containsKey("pagesCount"));
        List<Product> products = (List<Product>) modelMap.get("productsList");
        Assert.assertTrue(products.size() >= 1);
    }

    @Test
    public void testAddProduct() {
        this.product.setId(0);
        ProductDTO dbProductDTO = productService.add(productMapper.convertToDTO(this.product), null);
        Assert.assertEquals(this.product.getName(), dbProductDTO.getName());
        dbProductDTO.setName("TestUpdated");
        productService.edit(dbProductDTO, null);
        Assert.assertEquals("TestUpdated", productService.getById(dbProductDTO.getId()).getName());
        productService.delete(dbProductDTO.getId());
    }

    @Test
    public void testGetProduct() {
        ModelAndView mv = productService.getProduct(this.product.getId());
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("product"));
        Assert.assertTrue(modelMap.containsKey("categoriesList"));
        Assert.assertTrue(modelMap.containsKey("countriesList"));
        Product dbProduct = (Product)modelMap.get("product");
        Assert.assertEquals(this.product.getName(), dbProduct.getName());
    }

    @Test
    public void testProductsCount() {
        int productsCount = productService.productsCount(this.product.getCategory().getId());
        Assert.assertTrue(productsCount > 2);
    }
}
