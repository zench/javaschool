package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.model.Category;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.model.ProductProperty;
import ru.t_systems.samushenkov.tires_shop.model.Property;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ProductPropertyService;

import java.util.ArrayList;
import java.util.List;

public class ProductPropertyServiceTest extends AbstractServiceTest {

    private ProductProperty productProperty;
    private Product product;

    @Autowired
    ProductPropertyService productPropertyService;

    @Before
    public void setUp(){
        this.productProperty = new ProductProperty();
        this.product = new Product();
        this.product.setId(1);
        Category category = new Category();
        category.setId(1);
        this.product.setCategory(category);
        Property property = new Property();
        property.setId(1);
        this.productProperty.setProduct(this.product);
        this.productProperty.setId(1);
        this.productProperty.setValue("9");
        this.productProperty.setProperty(property);
    }

    @Test
    public void testGetById(){
        ProductProperty productProperty = productPropertyService.getById(1);
        Assert.assertNotNull(productProperty);
        Assert.assertEquals(this.productProperty.getValue(), productProperty.getValue());
    }

    @Test
    public void testGetNewProductProperties() {
        this.product.setId(0);
        List<ProductProperty> productProperties = productPropertyService.getProductProperties(this.product);
        Assert.assertFalse(productProperties.isEmpty());
        Assert.assertTrue(productProperties.size()>1);
        Assert.assertTrue(productProperties.get(0).getProperty() instanceof Property);
    }

    @Test
    public void testGetProductProperties() {
        List<ProductProperty> tempProductProperties = new ArrayList<>();
        this.product.setProductProperties(tempProductProperties);
        List<ProductProperty> productProperties = productPropertyService.getProductProperties(this.product);
        Assert.assertFalse(productProperties.isEmpty());
        Assert.assertTrue(productProperties.size()>1);
        Assert.assertTrue(productProperties.get(0).getProperty() instanceof Property);
    }

    @Test
    public void testAddProductProperty() {
        this.productProperty.setId(0);
        this.productProperty = productPropertyService.add(this.productProperty);
        Assert.assertEquals("9", this.productProperty.getValue());
        this.productProperty.setValue("19");
        productPropertyService.edit(this.productProperty);
        Assert.assertEquals("19", productPropertyService.getById(this.productProperty.getId()).getValue());
        productPropertyService.delete(this.productProperty);
    }
}
