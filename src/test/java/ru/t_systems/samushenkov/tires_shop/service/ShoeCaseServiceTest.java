package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.dto.ShowCaseDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ShowCaseService;

import java.util.List;

public class ShoeCaseServiceTest extends AbstractServiceTest {
    @Autowired
    ShowCaseService showCaseService;

    @Test
    public void testGetTopElements() {
        List<ShowCaseDTO> showCaseDTOList = showCaseService.getTopSortElements();
        Assert.assertNotNull(showCaseDTOList);
        Assert.assertTrue(showCaseDTOList.get(0).getName().length() > 1);
        Assert.assertTrue(showCaseDTOList.get(0).getPhotoUrl().length() > 1);
        Assert.assertTrue(showCaseDTOList.get(0).getUrl().length() > 1);
        Assert.assertTrue(showCaseDTOList.get(0).getPrice().intValue() > 100);
    }
}
