package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class UserDetailsServiceTest extends AbstractServiceTest {

    @Autowired
    UserDetailsService userDetailsService;

    @Test
    public void testLoadUserByUsername() {
        UserDetails userDetails = userDetailsService.loadUserByUsername("ironman@mail.ru");
        Assert.assertNotNull(userDetails);
        Assert.assertTrue(userDetails.isAccountNonExpired());
    }
}
