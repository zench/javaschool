package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.ModificationDTO;
import ru.t_systems.samushenkov.tires_shop.dto.TiresFilterDTO;
import ru.t_systems.samushenkov.tires_shop.mapper.ModificationMapper;
import ru.t_systems.samushenkov.tires_shop.model.Modification;
import ru.t_systems.samushenkov.tires_shop.model.Product;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ModificationServiceTest  extends AbstractServiceTest {
    private static final int DEFAULT_MODIFICATION_ID = 1;
    private static final int DEFAULT_PRODUCT_ID = 1;

    @Autowired
    ModificationMapper modificationMapper;

    @Autowired
    ModificationService modificationService;

    @Test
    public void testGetById(){
        Modification dbModification = modificationService.getById(DEFAULT_MODIFICATION_ID);
        Assert.assertNotNull(dbModification);
        Assert.assertTrue(dbModification.getPrice().intValue() > 100);
    }

    @Test
    public void testAllModifications() {
        TiresFilterDTO tfDTO = new TiresFilterDTO();
        ModelAndView modelAndView = modificationService.allModifications(1, 1, tfDTO);
        Map<String, Object> modelMap = modelAndView.getModel();
        Assert.assertNotNull(modelAndView);
        Assert.assertTrue(modelMap.containsKey("modificationsList"));
        Assert.assertTrue(modelMap.containsKey("brands"));
        Assert.assertTrue(modelMap.containsKey("seasons"));
        Assert.assertTrue(modelMap.containsKey("itemsCount"));
        Assert.assertTrue(modelMap.containsKey("pagesCount"));
        Assert.assertTrue(modelMap.containsKey("maxPrice"));
        Assert.assertTrue(modelMap.containsKey("minPrice"));
        List<Modification> modifications = (List<Modification>) modelMap.get("modificationsList");
        Assert.assertTrue(modifications.size() >= 1);
    }

    @Test
    public void testGetModification() {
        ModelAndView modelAndView = modificationService.getModification(DEFAULT_MODIFICATION_ID);
        Map<String, Object> modelMap = modelAndView.getModel();
        Assert.assertNotNull(modelAndView);
        Assert.assertTrue(modelMap.containsKey("modification"));
        Modification modification = (Modification)modelMap.get("modification");
        Assert.assertTrue(modification.getPrice().intValue() > 100);
    }

    @Test
    public void testGetMVById() {
        ModelAndView modelAndView = modificationService.getMVById(DEFAULT_MODIFICATION_ID);
        Map<String, Object> modelMap = modelAndView.getModel();
        Assert.assertNotNull(modelAndView);
        Assert.assertTrue(modelMap.containsKey("title"));
        Assert.assertTrue(modelMap.containsKey("modification"));
        Assert.assertTrue(modelMap.containsKey("productProperties"));
        Modification modification = (Modification)modelMap.get("modification");
        Assert.assertTrue(modification.getPrice().intValue() > 100);
        Assert.assertFalse(((String)modelMap.get("title")).isEmpty());
    }

    @Test
    public void testProductModification() {
        ModelAndView modelAndView = modificationService.productModifications(DEFAULT_PRODUCT_ID);
        Map<String, Object> modelMap = modelAndView.getModel();
        Assert.assertNotNull(modelAndView);
        Assert.assertTrue(modelMap.containsKey("modificationsList"));
        Assert.assertTrue(modelMap.containsKey("product"));
        List<Modification> modifications = (List<Modification>) modelMap.get("modificationsList");
        Assert.assertTrue(modifications.size() >= 1);
        Product product = (Product) modelMap.get("product");
        Assert.assertTrue(product.getName().length() > 1);
    }

    @Test
    public void testAddModification() {
        ModificationDTO newModDTO = new ModificationDTO();
        Product product = new Product();
        product.setId(1);
        newModDTO.setSort(1);
        newModDTO.setPrice(new BigDecimal(100));
        newModDTO.setProduct(product);
        newModDTO.setStockBalance(1);
        newModDTO = modificationService.add(newModDTO);
        Assert.assertTrue(newModDTO.getId() > 0);
        newModDTO.setStockBalance(2);
        modificationService.edit(newModDTO);
        Modification dbMod = modificationService.getById(newModDTO.getId());
        Assert.assertEquals(dbMod.getStockBalance(), newModDTO.getStockBalance());
        modificationService.delete(newModDTO.getId());
        Modification deletedMod = modificationService.getById(newModDTO.getId());
        Assert.assertNull(deletedMod);
    }
}
