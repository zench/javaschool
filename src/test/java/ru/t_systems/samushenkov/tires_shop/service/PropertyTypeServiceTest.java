package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.model.PropertyType;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.PropertyTypeService;

import java.util.List;

public class PropertyTypeServiceTest  extends AbstractServiceTest {
    private PropertyType propertyType;

    @Autowired
    PropertyTypeService propertyTypeService;

    @Before
    public void setUp(){
        this.propertyType = new PropertyType();
        this.propertyType.setName("string");
        this.propertyType.setId(1);
    }

    @Test
    public void testGetById(){
        PropertyType dbPropertyType = propertyTypeService.getById(1);
        Assert.assertNotNull(dbPropertyType);
        Assert.assertEquals(this.propertyType.getName(), dbPropertyType.getName());
    }

    @Test
    public void testAllUnits() {
        List<PropertyType> propertyTypes = propertyTypeService.allTypes();
        Assert.assertFalse(propertyTypes.isEmpty());
        Assert.assertTrue(propertyTypes.size()>2);
    }
}
