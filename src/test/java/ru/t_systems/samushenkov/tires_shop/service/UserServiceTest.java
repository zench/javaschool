package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.dto.UserDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserNoPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.dto.UserPasswordDTO;
import ru.t_systems.samushenkov.tires_shop.model.Country;
import ru.t_systems.samushenkov.tires_shop.model.Order;
import ru.t_systems.samushenkov.tires_shop.model.User;
import ru.t_systems.samushenkov.tires_shop.model.UserAddress;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.UserService;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.Map;

public class UserServiceTest extends AbstractServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext wac;

    private UserDTO userDTO;
    private UserNoPasswordDTO userNoPasswordDTO;
    private UserPasswordDTO userPasswordDTO;
    private MockHttpSession mockSession;
    private int dbUserId = 24;

    @Before
    public void setUp(){
        this.userDTO = new UserDTO();
        this.userDTO.setName("Test0101");
        this.userDTO.setEmail("test0101@mail.ru");
        this.userDTO.setLastName("Test0101LastName");
        this.userDTO.setBirthday("1980-01-01");
        this.userDTO.setPassword("123123");
        this.userDTO.setMatchingPassword("123123");

        this.userNoPasswordDTO = new UserNoPasswordDTO();
        this.userNoPasswordDTO.setName("Test0101");
        this.userNoPasswordDTO.setEmail("test0101@mail.ru");
        this.userNoPasswordDTO.setLastName("Test0101LastName");
        this.userNoPasswordDTO.setBirthday("1980-01-01");
        this.mockSession = new MockHttpSession(wac.getServletContext(), "C9EC5F7F3A8BC5DD7495FE37AEE87D4B");
        this.mockSession.setAttribute("userId", this.dbUserId);

        this.userPasswordDTO = new UserPasswordDTO();
        this.userPasswordDTO.setOldPassword(this.userDTO.getPassword());
        this.userPasswordDTO.setPassword("12345");
        this.userPasswordDTO.setMatchingPassword("12345");
    }

    @Test
    public void testGetById() {
        User user = userService.getById(1);
        Assert.assertNotNull(user);
        Assert.assertEquals("ironman", user.getName());
        Assert.assertEquals("ironman@mail.ru", user.getEmail());
        Assert.assertTrue(user.getPassword().length() > 30);
    }

    @Test
    public void testRegisterNewUserAccount() {
        BindingResult bindingResult = mock(BindingResult.class);
        User newUser = userService.registerNewUserAccount(this.userDTO, bindingResult);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(newUser.getEmail(), this.userDTO.getEmail());
        this.userNoPasswordDTO.setLastName("Test0102LastName");
        this.mockSession.setAttribute("userId", newUser.getId());
        userService.editUser(this.userNoPasswordDTO, this.mockSession, bindingResult);
        User editedUser = userService.getById(newUser.getId());
        Assert.assertEquals(this.userNoPasswordDTO.getLastName(), editedUser.getLastName());
        userService.deleteUser(newUser);
    }

    @Test
    public void testGetUserIdFromSession() {
        int userId = userService.getUserIdFromSession(this.mockSession);
        Assert.assertTrue(userId > 0);
    }

    @Test
    public void testGetUserInformation() {
        ModelAndView mv = userService.getUserInformation(this.mockSession);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("user"));
        Assert.assertTrue(modelMap.containsKey("countriesList"));
        Assert.assertTrue(modelMap.containsKey("userAddresses"));
        User user = (User)modelMap.get("user");
        Assert.assertEquals(user.getId(), this.dbUserId);
        Assert.assertTrue(user.getPassword().length() > 30);
        List<UserAddress> userAddresses = (List<UserAddress>)modelMap.get("userAddresses");
        Assert.assertTrue(userAddresses.size() > 0);
        List<Country> countries = (List<Country>)modelMap.get("countriesList");
        Assert.assertTrue(countries.size() > 2);
    }

    @Test
    public void testGetUserOrders() {
        ModelAndView mv = userService.getUserOrders(this.mockSession);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertTrue(modelMap.containsKey("ordersList"));
        List<Order> orders = (List<Order>)modelMap.get("ordersList");
        Assert.assertTrue(orders.size() > 1);
        Assert.assertTrue(orders.get(0).getContent().length()>10);
    }

    @Test
    public void testEditPassword() {
        boolean result = userService.editPassword(this.userPasswordDTO, this.mockSession);
        Assert.assertTrue(result);
        boolean result2 = userService.editPassword(this.userPasswordDTO, this.mockSession);
        Assert.assertFalse(result2);
        this.userPasswordDTO.setOldPassword(this.userPasswordDTO.getPassword());
        this.userPasswordDTO.setPassword(this.userDTO.getPassword());
        this.userPasswordDTO.setMatchingPassword(this.userDTO.getMatchingPassword());
        boolean result3 = userService.editPassword(this.userPasswordDTO, this.mockSession);
        Assert.assertTrue(result3);
    }
}
