package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ru.t_systems.samushenkov.tires_shop.constants.Constants;
import ru.t_systems.samushenkov.tires_shop.dto.ClientStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.dto.ProductStatisticsDTO;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.StatisticsService;

import java.util.List;
import java.util.Map;

public class StatisticsServiceTest  extends AbstractServiceTest {

    @Autowired
    StatisticsService statisticsService;

    @Test
    public void testGetAllStatisticsItemsAllNulls() {
        ModelAndView mv = statisticsService.getAllStatisticsItems(1, null, null, null);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertEquals(Constants.STATISTICS_MAIN_PAGE, mv.getViewName());
    }

    @Test
    public void testGetAllStatisticsItemsClientType() {
        ModelAndView mv = statisticsService.getAllStatisticsItems(1, "2019-11-01", "2019-11-30", Constants.STATISTICS_CLIENT_TYPE);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertEquals(Constants.STATISTICS_CLIENT_PAGE, mv.getViewName());
        Assert.assertTrue(modelMap.containsKey("clients"));
        List<ClientStatisticsDTO> clientsList = (List<ClientStatisticsDTO>) modelMap.get("clients");
        Assert.assertTrue(clientsList.size() >= 1);
    }

    @Test
    public void testGetAllStatisticsItemsProductType() {
        ModelAndView mv = statisticsService.getAllStatisticsItems(1, "2019-11-01", "2019-11-30", Constants.STATISTICS_PRODUCT_TYPE);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertEquals(Constants.STATISTICS_PRODUCT_PAGE, mv.getViewName());
        Assert.assertTrue(modelMap.containsKey("products"));
        List<ProductStatisticsDTO> productsList = (List<ProductStatisticsDTO>) modelMap.get("products");
        Assert.assertTrue(productsList.size() >= 1);
    }

    @Test
    public void testGetAllStatisticsItemsIncomeType() {
        ModelAndView mv = statisticsService.getAllStatisticsItems(1, "2019-11-01", "2019-11-30", Constants.STATISTICS_INCOME_TYPE);
        Map<String, Object> modelMap = mv.getModel();
        Assert.assertNotNull(mv);
        Assert.assertEquals(Constants.STATISTICS_INCOME_PAGE, mv.getViewName());
        Assert.assertTrue(modelMap.containsKey("income"));
    }
}
