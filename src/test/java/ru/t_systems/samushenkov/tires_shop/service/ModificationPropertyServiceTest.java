package ru.t_systems.samushenkov.tires_shop.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t_systems.samushenkov.tires_shop.model.*;
import ru.t_systems.samushenkov.tires_shop.service.interfaces.ModificationPropertyService;

import java.util.ArrayList;
import java.util.List;

public class ModificationPropertyServiceTest extends AbstractServiceTest {
    private ModificationProperty modificationProperty;
    private Modification modification;
    private int dbModificationPropertyId = 11;

    @Autowired
    ModificationPropertyService modificationPropertyService;

    @Before
    public void setUp(){
        this.modificationProperty = new ModificationProperty();
        this.modification = new Modification();
        this.modification.setId(1);
        Category category = new Category();
        category.setId(1);
        Product product = new Product();
        product.setId(1);
        product.setCategory(category);
        this.modification.setProduct(product);
        Property property = new Property();
        property.setId(this.dbModificationPropertyId);
        this.modificationProperty.setModification(this.modification);
        this.modificationProperty.setId(1);
        this.modificationProperty.setValue("195");
        this.modificationProperty.setProperty(property);
    }

    @Test
    public void testGetById(){
        ModificationProperty modificationProperty = modificationPropertyService.getById(1);
        Assert.assertNotNull(modificationProperty);
        Assert.assertEquals(this.modificationProperty.getValue(), modificationProperty.getValue());
    }

    @Test
    public void testGetModificationProperties() {
        List<ModificationProperty> tempModificationProperties = new ArrayList<>();
        this.modification.setModificationProperties(tempModificationProperties);
        List<ModificationProperty> modificationProperties = modificationPropertyService.getModificationProperties(this.modification, this.modification.getProduct());
        Assert.assertFalse(modificationProperties.isEmpty());
        Assert.assertTrue(modificationProperties.size()>1);
    }

    @Test
    public void testAddProductProperty() {
        this.modificationProperty.setId(0);
        ModificationProperty savedMP = modificationPropertyService.add(this.modificationProperty);
        Assert.assertEquals(savedMP.getValue(), this.modificationProperty.getValue());
        savedMP.setValue("205");
        modificationPropertyService.edit(savedMP);
        Assert.assertEquals("205", modificationPropertyService.getById(savedMP.getId()).getValue());
        modificationPropertyService.delete(savedMP);
    }
}
